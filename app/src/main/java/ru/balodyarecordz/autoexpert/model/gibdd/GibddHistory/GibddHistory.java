
package ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class GibddHistory implements Serializable{

    @SerializedName("RequestResult")
    @Expose
    private RequestResult requestResult;
    @SerializedName("vin")
    @Expose
    private String vin;
    @SerializedName("regnum")
    @Expose
    private Object regnum;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private int status;

    /**
     * 
     * @return
     *     The requestResult
     */
    public RequestResult getRequestResult() {
        return requestResult;
    }

    /**
     * 
     * @param requestResult
     *     The RequestResult
     */
    public void setRequestResult(RequestResult requestResult) {
        this.requestResult = requestResult;
    }

    /**
     * 
     * @return
     *     The vin
     */
    public String getVin() {
        return vin;
    }

    /**
     * 
     * @param vin
     *     The vin
     */
    public void setVin(String vin) {
        this.vin = vin;
    }

    /**
     * 
     * @return
     *     The regnum
     */
    public Object getRegnum() {
        return regnum;
    }

    /**
     * 
     * @param regnum
     *     The regnum
     */
    public void setRegnum(Object regnum) {
        this.regnum = regnum;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The status
     */
    public int getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(int status) {
        this.status = status;
    }

}
