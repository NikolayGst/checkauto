
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GTO implements Parcelable {

    @SerializedName("GTODate")
    @Expose
    private String GTODate;
    @SerializedName("GTORegionKod")
    @Expose
    private String GTORegionKod;
    @SerializedName("GTORes")
    @Expose
    private String GTORes;
    @SerializedName("GTORowNum")
    @Expose
    private String GTORowNum;

    /**
     * 
     * @return
     *     The GTODate
     */
    public String getGTODate() {
        return GTODate;
    }

    /**
     * 
     * @param GTODate
     *     The GTODate
     */
    public void setGTODate(String GTODate) {
        this.GTODate = GTODate;
    }

    /**
     * 
     * @return
     *     The GTORegionKod
     */
    public String getGTORegionKod() {
        return GTORegionKod;
    }

    /**
     * 
     * @param GTORegionKod
     *     The GTORegionKod
     */
    public void setGTORegionKod(String GTORegionKod) {
        this.GTORegionKod = GTORegionKod;
    }

    /**
     * 
     * @return
     *     The GTORes
     */
    public String getGTORes() {
        return GTORes;
    }

    /**
     * 
     * @param GTORes
     *     The GTORes
     */
    public void setGTORes(String GTORes) {
        this.GTORes = GTORes;
    }

    /**
     * 
     * @return
     *     The GTORowNum
     */
    public String getGTORowNum() {
        return GTORowNum;
    }

    /**
     * 
     * @param GTORowNum
     *     The GTORowNum
     */
    public void setGTORowNum(String GTORowNum) {
        this.GTORowNum = GTORowNum;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.GTODate);
        dest.writeString(this.GTORegionKod);
        dest.writeString(this.GTORes);
        dest.writeString(this.GTORowNum);
    }

    public GTO() {
    }

    protected GTO(Parcel in) {
        this.GTODate = in.readString();
        this.GTORegionKod = in.readString();
        this.GTORes = in.readString();
        this.GTORowNum = in.readString();
    }

    public static final Parcelable.Creator<GTO> CREATOR = new Parcelable.Creator<GTO>() {
        @Override
        public GTO createFromParcel(Parcel source) {
            return new GTO(source);
        }

        @Override
        public GTO[] newArray(int size) {
            return new GTO[size];
        }
    };
}
