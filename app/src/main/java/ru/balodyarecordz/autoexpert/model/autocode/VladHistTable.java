
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VladHistTable implements Parcelable {

    @SerializedName("VladPeriod")
    @Expose
    private String VladPeriod;
    @SerializedName("VladPeriodEnd")
    @Expose
    private String VladPeriodEnd;
    @SerializedName("VladPeriodStart")
    @Expose
    private String VladPeriodStart;
    @SerializedName("VladRowNum")
    @Expose
    private String VladRowNum;
    @SerializedName("VladType")
    @Expose
    private String VladType;

    /**
     * 
     * @return
     *     The VladPeriod
     */
    public String getVladPeriod() {
        return VladPeriod;
    }

    /**
     * 
     * @param VladPeriod
     *     The VladPeriod
     */
    public void setVladPeriod(String VladPeriod) {
        this.VladPeriod = VladPeriod;
    }

    /**
     * 
     * @return
     *     The VladPeriodEnd
     */
    public String getVladPeriodEnd() {
        return VladPeriodEnd;
    }

    /**
     * 
     * @param VladPeriodEnd
     *     The VladPeriodEnd
     */
    public void setVladPeriodEnd(String VladPeriodEnd) {
        this.VladPeriodEnd = VladPeriodEnd;
    }

    /**
     * 
     * @return
     *     The VladPeriodStart
     */
    public String getVladPeriodStart() {
        return VladPeriodStart;
    }

    /**
     * 
     * @param VladPeriodStart
     *     The VladPeriodStart
     */
    public void setVladPeriodStart(String VladPeriodStart) {
        this.VladPeriodStart = VladPeriodStart;
    }

    /**
     * 
     * @return
     *     The VladRowNum
     */
    public String getVladRowNum() {
        return VladRowNum;
    }

    /**
     * 
     * @param VladRowNum
     *     The VladRowNum
     */
    public void setVladRowNum(String VladRowNum) {
        this.VladRowNum = VladRowNum;
    }

    /**
     * 
     * @return
     *     The VladType
     */
    public String getVladType() {
        return VladType;
    }

    /**
     * 
     * @param VladType
     *     The VladType
     */
    public void setVladType(String VladType) {
        this.VladType = VladType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.VladPeriod);
        dest.writeString(this.VladPeriodEnd);
        dest.writeString(this.VladPeriodStart);
        dest.writeString(this.VladRowNum);
        dest.writeString(this.VladType);
    }

    public VladHistTable() {
    }

    protected VladHistTable(Parcel in) {
        this.VladPeriod = in.readString();
        this.VladPeriodEnd = in.readString();
        this.VladPeriodStart = in.readString();
        this.VladRowNum = in.readString();
        this.VladType = in.readString();
    }

    public static final Parcelable.Creator<VladHistTable> CREATOR = new Parcelable.Creator<VladHistTable>() {
        @Override
        public VladHistTable createFromParcel(Parcel source) {
            return new VladHistTable(source);
        }

        @Override
        public VladHistTable[] newArray(int size) {
            return new VladHistTable[size];
        }
    };
}
