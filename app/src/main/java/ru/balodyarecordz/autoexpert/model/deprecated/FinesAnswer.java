package ru.balodyarecordz.autoexpert.model.deprecated;

public class FinesAnswer {
    private String status;
    private String regnum;
    private String regreg;
    private String stsnum;
    private String rnum;
    private Request request;

    public String getRegnum() {
        return regnum;
    }

    public void setRegnum(String regnum) {
        this.regnum = regnum;
    }

    public String getRegreg() {
        return regreg;
    }

    public void setRegreg(String regreg) {
        this.regreg = regreg;
    }

    public String getStsnum() {
        return stsnum;
    }

    public void setStsnum(String stsnum) {
        this.stsnum = stsnum;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
