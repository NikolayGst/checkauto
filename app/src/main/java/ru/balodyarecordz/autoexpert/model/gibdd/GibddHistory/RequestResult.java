
package ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class RequestResult implements Serializable {

    @SerializedName("ownershipPeriods")
    @Expose
    private OwnershipPeriods ownershipPeriods;
/*    @SerializedName("vehiclePassport")
    @Expose
    private VehiclePassport vehiclePassport;*/
    @SerializedName("vehicle")
    @Expose
    private Vehicle vehicle;

    /**
     * 
     * @return
     *     The ownershipPeriods
     */
    public OwnershipPeriods getOwnershipPeriods() {
        return ownershipPeriods;
    }

    /**
     * 
     * @param ownershipPeriods
     *     The ownershipPeriods
     */
    public void setOwnershipPeriods(OwnershipPeriods ownershipPeriods) {
        this.ownershipPeriods = ownershipPeriods;
    }

  /*  *//**
     *
     * @return
     *     The vehiclePassport
     *//*
    public VehiclePassport getVehiclePassport() {
        return vehiclePassport;
    }

    *//**
     *
     * @param vehiclePassport
     *     The vehiclePassport
     *//*
    public void setVehiclePassport(VehiclePassport vehiclePassport) {
        this.vehiclePassport = vehiclePassport;
    }*/

    /**
     * 
     * @return
     *     The vehicle
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * 
     * @param vehicle
     *     The vehicle
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

}
