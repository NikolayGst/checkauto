package ru.balodyarecordz.autoexpert.model.autocode;

import android.graphics.Bitmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.balodyarecordz.autoexpert.utils.Utils;

public class AutocodeCaptcha {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("ses")
    @Expose
    private String ses;
    @SerializedName("captcha")
    @Expose
    private String captcha;
    @SerializedName("rkey")
    @Expose
    private String rkey;
    @SerializedName("vin")
    @Expose
    private String vin;
    @SerializedName("sts")
    @Expose
    private String sts;
    @SerializedName("values")
    @Expose
    private String values;

    public Bitmap getCaptchaImage() {
        return Utils.base64ToBitmap(captcha);
    }

    /**
     *
     * @return
     * The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The ses
     */
    public String getSes() {
        return ses;
    }

    /**
     *
     * @param ses
     * The ses
     */
    public void setSes(String ses) {
        this.ses = ses;
    }

    /**
     *
     * @return
     * The captcha
     */
    public String getCaptcha() {
        return captcha;
    }

    /**
     *
     * @param captcha
     * The captcha
     */
    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    /**
     *
     * @return
     * The rkey
     */
    public String getRkey() {
        return rkey;
    }

    /**
     *
     * @param rkey
     * The rkey
     */
    public void setRkey(String rkey) {
        this.rkey = rkey;
    }

    /**
     *
     * @return
     * The vin
     */
    public String getVin() {
        return vin;
    }

    /**
     *
     * @param vin
     * The vin
     */
    public void setVin(String vin) {
        this.vin = vin;
    }

    /**
     *
     * @return
     * The sts
     */
    public String getSts() {
        return sts;
    }

    /**
     *
     * @param sts
     * The sts
     */
    public void setSts(String sts) {
        this.sts = sts;
    }

    /**
     *
     * @return
     * The values
     */
    public String getValues() {
        return values;
    }

    /**
     *
     * @param values
     * The values
     */
    public void setValues(String values) {
        this.values = values;
    }

}
