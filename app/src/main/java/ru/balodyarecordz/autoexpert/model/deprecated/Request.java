package ru.balodyarecordz.autoexpert.model.deprecated;

import java.util.ArrayList;

public class Request {
    private int error;
    private int count;
    private ArrayList<Fine> data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Fine> getData() {
        return data;
    }

    public void setData(ArrayList<Fine> data) {
        this.data = data;
    }
}
