
package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("car_id")
    @Expose
    private Integer carId;
    @SerializedName("city_id")
    @Expose
    private Integer cityId;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("price_history")
    @Expose
    private Object priceHistory;
    @SerializedName("transmission")
    @Expose
    private String transmission;
    @SerializedName("drive")
    @Expose
    private String drive;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("source_url")
    @Expose
    private String sourceUrl;
    @SerializedName("parser")
    @Expose
    private String parser;
    @SerializedName("engine")
    @Expose
    private String engine;
    @SerializedName("mileage")
    @Expose
    private String mileage;
    @SerializedName("phones")
    @Expose
    private List<Phone> phones = new ArrayList<Phone>();
    @SerializedName("car_name")
    @Expose
    private String carName;
    @SerializedName("comments_count")
    @Expose
    private Integer commentsCount;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("person")
    @Expose
    private String person;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("phone_offers_count")
    @Expose
    private Integer phoneOffersCount;
    @SerializedName("price_diff")
    @Expose
    private Object priceDiff;
    @SerializedName("price_color")
    @Expose
    private Object priceColor;
    @SerializedName("source_host")
    @Expose
    private String sourceHost;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The carId
     */
    public Integer getCarId() {
        return carId;
    }

    /**
     * 
     * @param carId
     *     The car_id
     */
    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    /**
     * 
     * @return
     *     The cityId
     */
    public Integer getCityId() {
        return cityId;
    }

    /**
     * 
     * @param cityId
     *     The city_id
     */
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    /**
     * 
     * @return
     *     The cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * 
     * @param cityName
     *     The city_name
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * 
     * @return
     *     The year
     */
    public Integer getYear() {
        return year;
    }

    /**
     * 
     * @param year
     *     The year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /**
     * 
     * @return
     *     The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The priceHistory
     */
    public Object getPriceHistory() {
        return priceHistory;
    }

    /**
     * 
     * @param priceHistory
     *     The price_history
     */
    public void setPriceHistory(Object priceHistory) {
        this.priceHistory = priceHistory;
    }

    /**
     * 
     * @return
     *     The transmission
     */
    public String getTransmission() {
        return transmission;
    }

    /**
     * 
     * @param transmission
     *     The transmission
     */
    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    /**
     * 
     * @return
     *     The drive
     */
    public String getDrive() {
        return drive;
    }

    /**
     * 
     * @param drive
     *     The drive
     */
    public void setDrive(String drive) {
        this.drive = drive;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The sourceUrl
     */
    public String getSourceUrl() {
        return sourceUrl;
    }

    /**
     * 
     * @param sourceUrl
     *     The source_url
     */
    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    /**
     * 
     * @return
     *     The parser
     */
    public String getParser() {
        return parser;
    }

    /**
     * 
     * @param parser
     *     The parser
     */
    public void setParser(String parser) {
        this.parser = parser;
    }

    /**
     * 
     * @return
     *     The engine
     */
    public String getEngine() {
        return engine;
    }

    /**
     * 
     * @param engine
     *     The engine
     */
    public void setEngine(String engine) {
        this.engine = engine;
    }

    /**
     * 
     * @return
     *     The mileage
     */
    public String getMileage() {
        return mileage;
    }

    /**
     * 
     * @param mileage
     *     The mileage
     */
    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    /**
     * 
     * @return
     *     The phones
     */
    public List<Phone> getPhones() {
        return phones;
    }

    /**
     * 
     * @param phones
     *     The phones
     */
    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    /**
     * 
     * @return
     *     The carName
     */
    public String getCarName() {
        return carName;
    }

    /**
     * 
     * @param carName
     *     The car_name
     */
    public void setCarName(String carName) {
        this.carName = carName;
    }

    /**
     * 
     * @return
     *     The commentsCount
     */
    public Integer getCommentsCount() {
        return commentsCount;
    }

    /**
     * 
     * @param commentsCount
     *     The comments_count
     */
    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    /**
     * 
     * @return
     *     The state
     */
    public String getState() {
        return state;
    }

    /**
     * 
     * @param state
     *     The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 
     * @return
     *     The person
     */
    public String getPerson() {
        return person;
    }

    /**
     * 
     * @param person
     *     The person
     */
    public void setPerson(String person) {
        this.person = person;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The phoneOffersCount
     */
    public Integer getPhoneOffersCount() {
        return phoneOffersCount;
    }

    /**
     * 
     * @param phoneOffersCount
     *     The phone_offers_count
     */
    public void setPhoneOffersCount(Integer phoneOffersCount) {
        this.phoneOffersCount = phoneOffersCount;
    }

    /**
     * 
     * @return
     *     The priceDiff
     */
    public Object getPriceDiff() {
        return priceDiff;
    }

    /**
     * 
     * @param priceDiff
     *     The price_diff
     */
    public void setPriceDiff(Object priceDiff) {
        this.priceDiff = priceDiff;
    }

    /**
     * 
     * @return
     *     The priceColor
     */
    public Object getPriceColor() {
        return priceColor;
    }

    /**
     * 
     * @param priceColor
     *     The price_color
     */
    public void setPriceColor(Object priceColor) {
        this.priceColor = priceColor;
    }

    /**
     * 
     * @return
     *     The sourceHost
     */
    public String getSourceHost() {
        return sourceHost;
    }

    /**
     * 
     * @param sourceHost
     *     The source_host
     */
    public void setSourceHost(String sourceHost) {
        this.sourceHost = sourceHost;
    }

}
