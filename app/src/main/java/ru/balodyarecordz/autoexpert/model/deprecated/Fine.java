package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.SerializedName;

public class Fine {
    private String id;
    @SerializedName("DateDecis")
    private String date;
    @SerializedName("Summa")
    private int summa;
    @SerializedName("KoAPtext")
    private String fine;
    @SerializedName("KoAPcode")
    private String code;
    @SerializedName("Fam")
    private String fam;
    @SerializedName("Name")
    private String name;
    @SerializedName("Sname")
    private String sname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSumma() {
        return summa;
    }

    public void setSumma(int summa) {
        this.summa = summa;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFam() {
        return fam;
    }

    public void setFam(String fam) {
        this.fam = fam;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }
}
