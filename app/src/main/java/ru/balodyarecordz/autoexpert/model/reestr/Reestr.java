
package ru.balodyarecordz.autoexpert.model.reestr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Reestr {

    @SerializedName("CurrentFilterAsText")
    @Expose
    private List<String> CurrentFilterAsText = new ArrayList<String>();
    @SerializedName("TotalCount")
    @Expose
    private Integer TotalCount;
    @SerializedName("Items")
    @Expose
    private List<Item> Items = new ArrayList<Item>();
    @SerializedName("NeedCapcha")
    @Expose
    private Boolean NeedCapcha;
    @SerializedName("HasError")
    @Expose
    private Boolean HasError;
    @SerializedName("HasInfo")
    @Expose
    private Boolean HasInfo;
    @SerializedName("ErrorMessage")
    @Expose
    private Object ErrorMessage;
    @SerializedName("InfoMessage")
    @Expose
    private Object InfoMessage;
    @SerializedName("Error")
    @Expose
    private Integer Error;
    @SerializedName("Reasone")
    @Expose
    private String Reasone;

    /**
     *
     * @return
     * The Error
     */
    public Integer getError() {
        return Error;
    }

    /**
     *
     * @param Error
     * The Error
     */
    public void setError(Integer Error) {
        this.Error = Error;
    }

    /**
     *
     * @return
     * The Reasone
     */
    public String getReasone() {
        return Reasone;
    }

    /**
     *
     * @param Reasone
     * The Reasone
     */
    public void setReasone(String Reasone) {
        this.Reasone = Reasone;
    }

    /**
     * 
     * @return
     *     The CurrentFilterAsText
     */
    public List<String> getCurrentFilterAsText() {
        return CurrentFilterAsText;
    }

    /**
     * 
     * @param CurrentFilterAsText
     *     The CurrentFilterAsText
     */
    public void setCurrentFilterAsText(List<String> CurrentFilterAsText) {
        this.CurrentFilterAsText = CurrentFilterAsText;
    }

    /**
     * 
     * @return
     *     The TotalCount
     */
    public Integer getTotalCount() {
        return TotalCount;
    }

    /**
     * 
     * @param TotalCount
     *     The TotalCount
     */
    public void setTotalCount(Integer TotalCount) {
        this.TotalCount = TotalCount;
    }

    /**
     * 
     * @return
     *     The Items
     */
    public List<Item> getItems() {
        return Items;
    }

    /**
     * 
     * @param Items
     *     The Items
     */
    public void setItems(List<Item> Items) {
        this.Items = Items;
    }

    /**
     * 
     * @return
     *     The NeedCapcha
     */
    public Boolean getNeedCapcha() {
        return NeedCapcha;
    }

    /**
     * 
     * @param NeedCapcha
     *     The NeedCapcha
     */
    public void setNeedCapcha(Boolean NeedCapcha) {
        this.NeedCapcha = NeedCapcha;
    }

    /**
     * 
     * @return
     *     The HasError
     */
    public Boolean getHasError() {
        return HasError;
    }

    /**
     * 
     * @param HasError
     *     The HasError
     */
    public void setHasError(Boolean HasError) {
        this.HasError = HasError;
    }

    /**
     * 
     * @return
     *     The HasInfo
     */
    public Boolean getHasInfo() {
        return HasInfo;
    }

    /**
     * 
     * @param HasInfo
     *     The HasInfo
     */
    public void setHasInfo(Boolean HasInfo) {
        this.HasInfo = HasInfo;
    }

    /**
     * 
     * @return
     *     The ErrorMessage
     */
    public Object getErrorMessage() {
        return ErrorMessage;
    }

    /**
     * 
     * @param ErrorMessage
     *     The ErrorMessage
     */
    public void setErrorMessage(Object ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

    /**
     * 
     * @return
     *     The InfoMessage
     */
    public Object getInfoMessage() {
        return InfoMessage;
    }

    /**
     * 
     * @param InfoMessage
     *     The InfoMessage
     */
    public void setInfoMessage(Object InfoMessage) {
        this.InfoMessage = InfoMessage;
    }

}
