
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DTP implements Parcelable {

    @SerializedName("DTPDamage")
    @Expose
    private String DTPDamage;
    @SerializedName("DTPDate")
    @Expose
    private String DTPDate;
    @SerializedName("DTPRegionKod")
    @Expose
    private String DTPRegionKod;
    @SerializedName("DTPRowNum")
    @Expose
    private String DTPRowNum;
    @SerializedName("DTPType")
    @Expose
    private String DTPType;

    /**
     * 
     * @return
     *     The DTPDamage
     */
    public String getDTPDamage() {
        return DTPDamage;
    }

    /**
     * 
     * @param DTPDamage
     *     The DTPDamage
     */
    public void setDTPDamage(String DTPDamage) {
        this.DTPDamage = DTPDamage;
    }

    /**
     * 
     * @return
     *     The DTPDate
     */
    public String getDTPDate() {
        return DTPDate;
    }

    /**
     * 
     * @param DTPDate
     *     The DTPDate
     */
    public void setDTPDate(String DTPDate) {
        this.DTPDate = DTPDate;
    }

    /**
     * 
     * @return
     *     The DTPRegionKod
     */
    public String getDTPRegionKod() {
        return DTPRegionKod;
    }

    /**
     * 
     * @param DTPRegionKod
     *     The DTPRegionKod
     */
    public void setDTPRegionKod(String DTPRegionKod) {
        this.DTPRegionKod = DTPRegionKod;
    }

    /**
     * 
     * @return
     *     The DTPRowNum
     */
    public String getDTPRowNum() {
        return DTPRowNum;
    }

    /**
     * 
     * @param DTPRowNum
     *     The DTPRowNum
     */
    public void setDTPRowNum(String DTPRowNum) {
        this.DTPRowNum = DTPRowNum;
    }

    /**
     * 
     * @return
     *     The DTPType
     */
    public String getDTPType() {
        return DTPType;
    }

    /**
     * 
     * @param DTPType
     *     The DTPType
     */
    public void setDTPType(String DTPType) {
        this.DTPType = DTPType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.DTPDamage);
        dest.writeString(this.DTPDate);
        dest.writeString(this.DTPRegionKod);
        dest.writeString(this.DTPRowNum);
        dest.writeString(this.DTPType);
    }

    public DTP() {
    }

    protected DTP(Parcel in) {
        this.DTPDamage = in.readString();
        this.DTPDate = in.readString();
        this.DTPRegionKod = in.readString();
        this.DTPRowNum = in.readString();
        this.DTPType = in.readString();
    }

    public static final Parcelable.Creator<DTP> CREATOR = new Parcelable.Creator<DTP>() {
        @Override
        public DTP createFromParcel(Parcel source) {
            return new DTP(source);
        }

        @Override
        public DTP[] newArray(int size) {
            return new DTP[size];
        }
    };
}
