
package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Phone {

    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("offers_count")
    @Expose
    private Integer offersCount;
    @SerializedName("is_black")
    @Expose
    private Boolean isBlack;

    /**
     * 
     * @return
     *     The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 
     * @param phone
     *     The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 
     * @return
     *     The region
     */
    public String getRegion() {
        return region;
    }

    /**
     * 
     * @param region
     *     The region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * 
     * @return
     *     The offersCount
     */
    public Integer getOffersCount() {
        return offersCount;
    }

    /**
     * 
     * @param offersCount
     *     The offers_count
     */
    public void setOffersCount(Integer offersCount) {
        this.offersCount = offersCount;
    }

    /**
     * 
     * @return
     *     The isBlack
     */
    public Boolean getIsBlack() {
        return isBlack;
    }

    /**
     * 
     * @param isBlack
     *     The is_black
     */
    public void setIsBlack(Boolean isBlack) {
        this.isBlack = isBlack;
    }

}
