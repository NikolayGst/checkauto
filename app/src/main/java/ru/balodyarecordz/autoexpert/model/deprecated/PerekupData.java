
package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PerekupData {

    @SerializedName("body")
    @Expose
    private Body body;
    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * 
     * @return
     *     The body
     */
    public Body getBody() {
        return body;
    }

    /**
     * 
     * @param body
     *     The body
     */
    public void setBody(Body body) {
        this.body = body;
    }

    /**
     * 
     * @return
     *     The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

}
