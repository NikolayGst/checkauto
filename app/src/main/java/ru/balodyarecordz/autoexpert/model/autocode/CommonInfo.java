
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonInfo implements Parcelable {


    @SerializedName("CategoryTC")
    @Expose
    private String CategoryTC;
    @SerializedName("Color")
    @Expose
    private String Color;
    @SerializedName("CorpusTip")
    @Expose
    private String CorpusTip;
    @SerializedName("EcologClass")
    @Expose
    private String EcologClass;
    @SerializedName("KWT")
    @Expose
    private String KWT;
    @SerializedName("KolVl")
    @Expose
    private String KolVl;
    @SerializedName("LS")
    @Expose
    private String LS;
    @SerializedName("Model")
    @Expose
    private String Model;
    @SerializedName("PermissWeight")
    @Expose
    private String PermissWeight;
    @SerializedName("StatevyvozName")
    @Expose
    private String StatevyvozName;
    @SerializedName("V3")
    @Expose
    private String V3;
    @SerializedName("Year")
    @Expose
    private String Year;

    /**
     * 
     * @return
     *     The CategoryTC
     */
    public String getCategoryTC() {
        return CategoryTC;
    }

    /**
     * 
     * @param CategoryTC
     *     The CategoryTC
     */
    public void setCategoryTC(String CategoryTC) {
        this.CategoryTC = CategoryTC;
    }

    /**
     * 
     * @return
     *     The Color
     */
    public String getColor() {
        return Color;
    }

    /**
     * 
     * @param Color
     *     The Color
     */
    public void setColor(String Color) {
        this.Color = Color;
    }

    /**
     * 
     * @return
     *     The CorpusTip
     */
    public String getCorpusTip() {
        return CorpusTip;
    }

    /**
     * 
     * @param CorpusTip
     *     The CorpusTip
     */
    public void setCorpusTip(String CorpusTip) {
        this.CorpusTip = CorpusTip;
    }

    /**
     * 
     * @return
     *     The EcologClass
     */
    public String getEcologClass() {
        return EcologClass;
    }

    /**
     * 
     * @param EcologClass
     *     The EcologClass
     */
    public void setEcologClass(String EcologClass) {
        this.EcologClass = EcologClass;
    }

    /**
     * 
     * @return
     *     The KWT
     */
    public String getKWT() {
        return KWT;
    }

    /**
     * 
     * @param KWT
     *     The KWT
     */
    public void setKWT(String KWT) {
        this.KWT = KWT;
    }

    /**
     * 
     * @return
     *     The KolVl
     */
    public String getKolVl() {
        return KolVl;
    }

    /**
     * 
     * @param KolVl
     *     The KolVl
     */
    public void setKolVl(String KolVl) {
        this.KolVl = KolVl;
    }

    /**
     * 
     * @return
     *     The LS
     */
    public String getLS() {
        return LS;
    }

    /**
     * 
     * @param LS
     *     The LS
     */
    public void setLS(String LS) {
        this.LS = LS;
    }

    /**
     * 
     * @return
     *     The Model
     */
    public String getModel() {
        return Model;
    }

    /**
     * 
     * @param Model
     *     The Model
     */
    public void setModel(String Model) {
        this.Model = Model;
    }

    /**
     * 
     * @return
     *     The PermissWeight
     */
    public String getPermissWeight() {
        return PermissWeight;
    }

    /**
     * 
     * @param PermissWeight
     *     The PermissWeight
     */
    public void setPermissWeight(String PermissWeight) {
        this.PermissWeight = PermissWeight;
    }

    /**
     * 
     * @return
     *     The StatevyvozName
     */
    public String getStatevyvozName() {
        return StatevyvozName;
    }

    /**
     * 
     * @param StatevyvozName
     *     The StatevyvozName
     */
    public void setStatevyvozName(String StatevyvozName) {
        this.StatevyvozName = StatevyvozName;
    }

    /**
     * 
     * @return
     *     The V3
     */
    public String getV3() {
        return V3;
    }

    /**
     * 
     * @param V3
     *     The V3
     */
    public void setV3(String V3) {
        this.V3 = V3;
    }

    /**
     * 
     * @return
     *     The Year
     */
    public String getYear() {
        return Year;
    }

    /**
     * 
     * @param Year
     *     The Year
     */
    public void setYear(String Year) {
        this.Year = Year;
    }

    @Override
    public String toString() {
        return Model + " " + Color + " " + LS + " лс" + " " + Year  + " г." ;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.CategoryTC);
        dest.writeString(this.Color);
        dest.writeString(this.CorpusTip);
        dest.writeString(this.EcologClass);
        dest.writeString(this.KWT);
        dest.writeString(this.KolVl);
        dest.writeString(this.LS);
        dest.writeString(this.Model);
        dest.writeString(this.PermissWeight);
        dest.writeString(this.StatevyvozName);
        dest.writeString(this.V3);
        dest.writeString(this.Year);
    }

    public CommonInfo() {
    }

    protected CommonInfo(Parcel in) {
        this.CategoryTC = in.readString();
        this.Color = in.readString();
        this.CorpusTip = in.readString();
        this.EcologClass = in.readString();
        this.KWT = in.readString();
        this.KolVl = in.readString();
        this.LS = in.readString();
        this.Model = in.readString();
        this.PermissWeight = in.readString();
        this.StatevyvozName = in.readString();
        this.V3 = in.readString();
        this.Year = in.readString();
    }

    public static final Parcelable.Creator<CommonInfo> CREATOR = new Parcelable.Creator<CommonInfo>() {
        @Override
        public CommonInfo createFromParcel(Parcel source) {
            return new CommonInfo(source);
        }

        @Override
        public CommonInfo[] newArray(int size) {
            return new CommonInfo[size];
        }
    };
}
