
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class InsurancePart implements Parcelable {

    @SerializedName("Insurance")
    @Expose
    private List<Object> Insurance = new ArrayList<Object>();
    @SerializedName("InsuranceComment")
    @Expose
    private String InsuranceComment;

    /**
     * 
     * @return
     *     The Insurance
     */
    public List<Object> getInsurance() {
        return Insurance;
    }

    /**
     * 
     * @param Insurance
     *     The Insurance
     */
    public void setInsurance(List<Object> Insurance) {
        this.Insurance = Insurance;
    }

    /**
     * 
     * @return
     *     The InsuranceComment
     */
    public String getInsuranceComment() {
        return InsuranceComment;
    }

    /**
     * 
     * @param InsuranceComment
     *     The InsuranceComment
     */
    public void setInsuranceComment(String InsuranceComment) {
        this.InsuranceComment = InsuranceComment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.Insurance);
        dest.writeString(this.InsuranceComment);
    }

    public InsurancePart() {
    }

    protected InsurancePart(Parcel in) {
        this.Insurance = new ArrayList<Object>();
        in.readList(this.Insurance, Object.class.getClassLoader());
        this.InsuranceComment = in.readString();
    }

    public static final Parcelable.Creator<InsurancePart> CREATOR = new Parcelable.Creator<InsurancePart>() {
        @Override
        public InsurancePart createFromParcel(Parcel source) {
            return new InsurancePart(source);
        }

        @Override
        public InsurancePart[] newArray(int size) {
            return new InsurancePart[size];
        }
    };
}
