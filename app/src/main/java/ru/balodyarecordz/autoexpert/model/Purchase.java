package ru.balodyarecordz.autoexpert.model;

import android.content.Context;
import android.content.SharedPreferences;

public class Purchase {

    public static final String KEY_PRO = "ru.balodyarecordz.autoexpert.purchase.PRO";
    public static final String KEY_COUNT = "ru.balodyarecordz.autoexpert.purchase.COUNT";
    public static final String KEY_KEY = "ru.balodyarecordz.autoexpert.purchase.KEY";

    private static Purchase instance;
    private SharedPreferences preferences;

    private Purchase() {

    }

    public static Purchase getInstance() {
        if (instance == null) {
            instance = new Purchase();
        }
        return instance;
    }

    public void init(Context context) {
        preferences = context.getSharedPreferences("pref_purchase", Context.MODE_PRIVATE);
    }

    public void setPro(boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_PRO, value);
        editor.apply();
    }

    public Boolean isPro() {
        return preferences.getBoolean(KEY_PRO, false);
    }

    public void counter(int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_COUNT, value - 1);
        editor.apply();
    }

    public int getCount() {
        return preferences.getInt(KEY_COUNT, 10);
    }
}
