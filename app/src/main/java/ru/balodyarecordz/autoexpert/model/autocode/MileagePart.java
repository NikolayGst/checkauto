
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MileagePart implements Parcelable {

    @SerializedName("Mileage")
    @Expose
    private List<Object> Mileage = new ArrayList<Object>();
    @SerializedName("MileageComment")
    @Expose
    private String MileageComment;

    /**
     * 
     * @return
     *     The Mileage
     */
    public List<Object> getMileage() {
        return Mileage;
    }

    /**
     * 
     * @param Mileage
     *     The Mileage
     */
    public void setMileage(List<Object> Mileage) {
        this.Mileage = Mileage;
    }

    /**
     * 
     * @return
     *     The MileageComment
     */
    public String getMileageComment() {
        return MileageComment;
    }

    /**
     * 
     * @param MileageComment
     *     The MileageComment
     */
    public void setMileageComment(String MileageComment) {
        this.MileageComment = MileageComment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.Mileage);
        dest.writeString(this.MileageComment);
    }

    public MileagePart() {
    }

    protected MileagePart(Parcel in) {
        this.Mileage = new ArrayList<Object>();
        in.readList(this.Mileage, Object.class.getClassLoader());
        this.MileageComment = in.readString();
    }

    public static final Parcelable.Creator<MileagePart> CREATOR = new Parcelable.Creator<MileagePart>() {
        @Override
        public MileagePart createFromParcel(Parcel source) {
            return new MileagePart(source);
        }

        @Override
        public MileagePart[] newArray(int size) {
            return new MileagePart[size];
        }
    };
}
