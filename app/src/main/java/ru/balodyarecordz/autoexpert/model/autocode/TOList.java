
package ru.balodyarecordz.autoexpert.model.autocode;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class TOList {

    @SerializedName("TOCompany")
    @Expose
    private String tOCompany;
    @SerializedName("TODate")
    @Expose
    private String tODate;
    @SerializedName("TORes")
    @Expose
    private String tORes;

    /**
     * 
     * @return
     *     The tOCompany
     */
    public String getTOCompany() {
        return tOCompany;
    }

    /**
     * 
     * @param tOCompany
     *     The TOCompany
     */
    public void setTOCompany(String tOCompany) {
        this.tOCompany = tOCompany;
    }

    /**
     * 
     * @return
     *     The tODate
     */
    public String getTODate() {
        return tODate;
    }

    /**
     * 
     * @param tODate
     *     The TODate
     */
    public void setTODate(String tODate) {
        this.tODate = tODate;
    }

    /**
     * 
     * @return
     *     The tORes
     */
    public String getTORes() {
        return tORes;
    }

    /**
     * 
     * @param tORes
     *     The TORes
     */
    public void setTORes(String tORes) {
        this.tORes = tORes;
    }

}
