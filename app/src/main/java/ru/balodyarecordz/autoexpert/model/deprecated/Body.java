
package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Body {

    @SerializedName("total_pages")
    @Expose
    private Integer totalPages;
    @SerializedName("total_count")
    @Expose
    private String totalCount;
    @SerializedName("curr_page")
    @Expose
    private Integer currPage;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();

    /**
     * 
     * @return
     *     The totalPages
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * 
     * @param totalPages
     *     The total_pages
     */
    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * 
     * @return
     *     The totalCount
     */
    public String getTotalCount() {
        return totalCount;
    }

    /**
     * 
     * @param totalCount
     *     The total_count
     */
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * 
     * @return
     *     The currPage
     */
    public Integer getCurrPage() {
        return currPage;
    }

    /**
     * 
     * @param currPage
     *     The curr_page
     */
    public void setCurrPage(Integer currPage) {
        this.currPage = currPage;
    }

    /**
     * 
     * @return
     *     The data
     */
    public List<Datum> getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(List<Datum> data) {
        this.data = data;
    }

}
