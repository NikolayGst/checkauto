
package ru.balodyarecordz.autoexpert.model.gibdd.GibddWanted;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WantedRecord {

    @SerializedName("w_rec")
    @Expose
    private Integer wRec;
    @SerializedName("w_reg_inic")
    @Expose
    private String wRegInic;
    @SerializedName("w_user")
    @Expose
    private String wUser;
    @SerializedName("w_model")
    @Expose
    private String wModel;
    @SerializedName("w_data_pu")
    @Expose
    private String wDataPu;
    @SerializedName("w_god_vyp")
    @Expose
    private String wGodVyp;
    @SerializedName("w_vid_uch")
    @Expose
    private String wVidUch;
    @SerializedName("w_un_gic")
    @Expose
    private String wUnGic;

    /**
     * 
     * @return
     *     The wRec
     */
    public Integer getWRec() {
        return wRec;
    }

    /**
     * 
     * @param wRec
     *     The w_rec
     */
    public void setWRec(Integer wRec) {
        this.wRec = wRec;
    }

    /**
     * 
     * @return
     *     The wRegInic
     */
    public String getWRegInic() {
        return wRegInic;
    }

    /**
     * 
     * @param wRegInic
     *     The w_reg_inic
     */
    public void setWRegInic(String wRegInic) {
        this.wRegInic = wRegInic;
    }

    /**
     * 
     * @return
     *     The wUser
     */
    public String getWUser() {
        return wUser;
    }

    /**
     * 
     * @param wUser
     *     The w_user
     */
    public void setWUser(String wUser) {
        this.wUser = wUser;
    }

    /**
     * 
     * @return
     *     The wModel
     */
    public String getWModel() {
        return wModel;
    }

    /**
     * 
     * @param wModel
     *     The w_model
     */
    public void setWModel(String wModel) {
        this.wModel = wModel;
    }

    /**
     * 
     * @return
     *     The wDataPu
     */
    public String getWDataPu() {
        return wDataPu;
    }

    /**
     * 
     * @param wDataPu
     *     The w_data_pu
     */
    public void setWDataPu(String wDataPu) {
        this.wDataPu = wDataPu;
    }

    /**
     * 
     * @return
     *     The wGodVyp
     */
    public String getWGodVyp() {
        return wGodVyp;
    }

    /**
     * 
     * @param wGodVyp
     *     The w_god_vyp
     */
    public void setWGodVyp(String wGodVyp) {
        this.wGodVyp = wGodVyp;
    }

    /**
     * 
     * @return
     *     The wVidUch
     */
    public String getWVidUch() {
        return wVidUch;
    }

    /**
     * 
     * @param wVidUch
     *     The w_vid_uch
     */
    public void setWVidUch(String wVidUch) {
        this.wVidUch = wVidUch;
    }

    /**
     * 
     * @return
     *     The wUnGic
     */
    public String getWUnGic() {
        return wUnGic;
    }

    /**
     * 
     * @param wUnGic
     *     The w_un_gic
     */
    public void setWUnGic(String wUnGic) {
        this.wUnGic = wUnGic;
    }

}
