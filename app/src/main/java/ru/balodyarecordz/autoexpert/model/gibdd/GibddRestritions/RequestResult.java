
package ru.balodyarecordz.autoexpert.model.gibdd.GibddRestritions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RequestResult {

    @SerializedName("records")
    @Expose
    private List<RestritionsRecord> mRestritionsRecords = new ArrayList<RestritionsRecord>();
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("error")
    @Expose
    private Integer error;

    /**
     * 
     * @return
     *     The mRestritionsRecords
     */
    public List<RestritionsRecord> getRestritionsRecords() {
        return mRestritionsRecords;
    }

    /**
     * 
     * @param restritionsRecords
     *     The mRestritionsRecords
     */
    public void setRestritionsRecords(List<RestritionsRecord> restritionsRecords) {
        this.mRestritionsRecords = restritionsRecords;
    }

    /**
     * 
     * @return
     *     The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 
     * @return
     *     The error
     */
    public Integer getError() {
        return error;
    }

    /**
     * 
     * @param error
     *     The error
     */
    public void setError(Integer error) {
        this.error = error;
    }

}
