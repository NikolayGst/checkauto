package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Filter {
    @SerializedName("VIN")
    private String vin;
    @SerializedName("FormFields")
    private ArrayList<String> formFields;
    private ArrayList<String> errors;

    public Filter(String vin) {
        this.vin = vin;
        this.formFields = new ArrayList<>();
        this.formFields.add(vin);
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public ArrayList<String> getFormFields() {
        return formFields;
    }

    public void setFormFields(ArrayList<String> formFields) {
        this.formFields = formFields;
    }

    public ArrayList<String> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<String> errors) {
        this.errors = errors;
    }
}
