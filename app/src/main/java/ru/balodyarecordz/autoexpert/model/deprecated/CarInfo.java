package ru.balodyarecordz.autoexpert.model.deprecated;

public class CarInfo {
    private String model;
    private String vin;
    private String year;
    private String engineNumber;
    private String frameNumber;
    private String bodyNumber;
    private String color;
    private String govNumber;
    private String sts;
    private CarDoc carDoc;
    private String whoSts;
//    private String pts;
//    private String ptsWho;
//    private String ptsDate;

    public CarInfo() {
        carDoc = new CarDoc();
    }

    public CarInfo(String model, String vin, String year, String engineNumber, String frameNumber, String bodyNumber, String color, String govNumber, String sts, CarDoc carDoc) {
        this.model = model;
        this.vin = vin;
        this.year = year;
        this.engineNumber = engineNumber;
        this.frameNumber = frameNumber;
        this.bodyNumber = bodyNumber;
        this.color = color;
        this.govNumber = govNumber;
        this.sts = sts;
        this.carDoc = carDoc;
    }

    public String getWhoSts() {
        return whoSts;
    }

    public void setWhoSts(String whoSts) {
        this.whoSts = whoSts;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(String frameNumber) {
        this.frameNumber = frameNumber;
    }

    public String getBodyNumber() {
        return bodyNumber;
    }

    public void setBodyNumber(String bodyNumber) {
        this.bodyNumber = bodyNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getGovNumber() {
        return govNumber;
    }

    public void setGovNumber(String govNumber) {
        this.govNumber = govNumber;
    }

    public String getSts() {
        return sts;
    }

    public void setSts(String sts) {
        this.sts = sts;
    }

    public CarDoc getCarDoc() {
        return carDoc;
    }

    public void setCarDoc(CarDoc carDoc) {
        this.carDoc = carDoc;
    }

}
