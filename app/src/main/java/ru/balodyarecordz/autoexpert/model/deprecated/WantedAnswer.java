package ru.balodyarecordz.autoexpert.model.deprecated;

public class WantedAnswer {
    private String vin;
    private int status;
    private Restricted restricted;
    private Restricted wanted;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Restricted getRestricted() {
        return restricted;
    }

    public void setRestricted(Restricted restricted) {
        this.restricted = restricted;
    }

    public Restricted getWanted() {
        return wanted;
    }

    public void setWanted(Restricted wanted) {
        this.wanted = wanted;
    }
}
