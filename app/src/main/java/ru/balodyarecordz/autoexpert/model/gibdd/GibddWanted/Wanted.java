
package ru.balodyarecordz.autoexpert.model.gibdd.GibddWanted;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Wanted {

    @SerializedName("RequestResult")
    @Expose
    private RequestResult requestResult;
    @SerializedName("vin")
    @Expose
    private String vin;
    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * 
     * @return
     *     The requestResult
     */
    public RequestResult getRequestResult() {
        return requestResult;
    }

    /**
     * 
     * @param requestResult
     *     The RequestResult
     */
    public void setRequestResult(RequestResult requestResult) {
        this.requestResult = requestResult;
    }

    /**
     * 
     * @return
     *     The vin
     */
    public String getVin() {
        return vin;
    }

    /**
     * 
     * @param vin
     *     The vin
     */
    public void setVin(String vin) {
        this.vin = vin;
    }

    /**
     * 
     * @return
     *     The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

}
