
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Values implements Parcelable {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("actual_time")
    @Expose
    private String actualTime;

    /**
     * 
     * @return
     *     The data
     */
    public Data getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    /**
     * 
     * @return
     *     The actualTime
     */
    public String getActualTime() {
        return actualTime;
    }

    /**
     * 
     * @param actualTime
     *     The actual_time
     */
    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.data, flags);
        dest.writeString(this.actualTime);
    }

    public Values() {
    }

    protected Values(Parcel in) {
        this.data = in.readParcelable(Data.class.getClassLoader());
        this.actualTime = in.readString();
    }

    public static final Parcelable.Creator<Values> CREATOR = new Parcelable.Creator<Values>() {
        @Override
        public Values createFromParcel(Parcel source) {
            return new Values(source);
        }

        @Override
        public Values[] newArray(int size) {
            return new Values[size];
        }
    };
}
