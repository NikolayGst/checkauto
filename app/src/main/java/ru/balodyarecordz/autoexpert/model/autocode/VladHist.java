
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class VladHist implements Parcelable {

    @SerializedName("VladHistComment")
    @Expose
    private Object VladHistComment;
    @SerializedName("VladHistTable")
    @Expose
    private List<ru.balodyarecordz.autoexpert.model.autocode.VladHistTable> VladHistTable = new ArrayList<ru.balodyarecordz.autoexpert.model.autocode.VladHistTable>();

    /**
     * 
     * @return
     *     The VladHistComment
     */
    public Object getVladHistComment() {
        return VladHistComment;
    }

    /**
     * 
     * @param VladHistComment
     *     The VladHistComment
     */
    public void setVladHistComment(Object VladHistComment) {
        this.VladHistComment = VladHistComment;
    }

    /**
     * 
     * @return
     *     The VladHistTable
     */
    public List<ru.balodyarecordz.autoexpert.model.autocode.VladHistTable> getVladHistTable() {
        return VladHistTable;
    }

    /**
     * 
     * @param VladHistTable
     *     The VladHistTable
     */
    public void setVladHistTable(List<ru.balodyarecordz.autoexpert.model.autocode.VladHistTable> VladHistTable) {
        this.VladHistTable = VladHistTable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.VladHistTable);
    }

    public VladHist() {
    }

    protected VladHist(Parcel in) {
        this.VladHistTable = new ArrayList<ru.balodyarecordz.autoexpert.model.autocode.VladHistTable>();
        in.readList(this.VladHistTable, ru.balodyarecordz.autoexpert.model.autocode.VladHistTable.class.getClassLoader());
    }

    public static final Parcelable.Creator<VladHist> CREATOR = new Parcelable.Creator<VladHist>() {
        @Override
        public VladHist createFromParcel(Parcel source) {
            return new VladHist(source);
        }

        @Override
        public VladHist[] newArray(int size) {
            return new VladHist[size];
        }
    };
}
