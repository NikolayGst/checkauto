package ru.balodyarecordz.autoexpert.model;

import android.graphics.Bitmap;

import ru.balodyarecordz.autoexpert.utils.Utils;

public class CaptchaResponse {
    private String code;
    private String ses;
    private String captcha;
    private String capcha;
    private String token;

    public String getCapcha() {
        return capcha;
    }

    public void setCapcha(String capcha) {
        this.capcha = capcha;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSes() {
        return ses;
    }

    public void setSes(String ses) {
        this.ses = ses;
    }

    public String getCaptcha() {
        return captcha;
    }

    public Bitmap getCaptchaImage() {
        return Utils.base64ToBitmap(captcha);
    }

    public Bitmap getCapchaImage() {
        return Utils.base64ToBitmap(capcha);
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}
