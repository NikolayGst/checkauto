package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.SerializedName;

public class VinData {
    @SerializedName("DocId")
    private String id;
    @SerializedName("RegDate")
    private String date;
    @SerializedName("RegInfoText")
    private String vinCode;
    @SerializedName("Pledgor")
    private String name;
    @SerializedName("Mortgagees")
    private String owner;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVinCode() {
        return vinCode;
    }

    public void setVinCode(String vinCode) {
        this.vinCode = vinCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
