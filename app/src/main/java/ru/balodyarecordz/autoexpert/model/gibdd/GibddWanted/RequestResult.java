
package ru.balodyarecordz.autoexpert.model.gibdd.GibddWanted;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RequestResult {

    @SerializedName("records")
    @Expose
    private List<WantedRecord> mWantedRecords = new ArrayList<WantedRecord>();
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("error")
    @Expose
    private Integer error;

    /**
     * 
     * @return
     *     The mWantedRecords
     */
    public List<WantedRecord> getWantedRecords() {
        return mWantedRecords;
    }

    /**
     * 
     * @param wantedRecords
     *     The mWantedRecords
     */
    public void setWantedRecords(List<WantedRecord> wantedRecords) {
        this.mWantedRecords = wantedRecords;
    }

    /**
     * 
     * @return
     *     The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 
     * @return
     *     The error
     */
    public Integer getError() {
        return error;
    }

    /**
     * 
     * @param error
     *     The error
     */
    public void setError(Integer error) {
        this.error = error;
    }

}
