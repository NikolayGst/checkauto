
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data implements Parcelable {

    @SerializedName("__type")
    @Expose
    private String Type;
    @SerializedName("EventID")
    @Expose
    private Integer EventID;
    @SerializedName("IsMain")
    @Expose
    private Boolean IsMain;
    @SerializedName("Message")
    @Expose
    private Object Message;
    @SerializedName("OutServiceID")
    @Expose
    private Integer OutServiceID;
    @SerializedName("State")
    @Expose
    private Integer State;
    @SerializedName("CommonInfo")
    @Expose
    private ru.balodyarecordz.autoexpert.model.autocode.CommonInfo CommonInfo;
    @SerializedName("DTP_Part")
    @Expose
    private ru.balodyarecordz.autoexpert.model.autocode.DTPPart DTPPart;
    @SerializedName("GTO_Part")
    @Expose
    private ru.balodyarecordz.autoexpert.model.autocode.GTOPart GTOPart;
    @SerializedName("Insurance_Part")
    @Expose
    private ru.balodyarecordz.autoexpert.model.autocode.InsurancePart InsurancePart;
    @SerializedName("Kommercial_Part")
    @Expose
    private ru.balodyarecordz.autoexpert.model.autocode.KommercialPart KommercialPart;
    @SerializedName("Mileage_Part")
    @Expose
    private ru.balodyarecordz.autoexpert.model.autocode.MileagePart MileagePart;
    @SerializedName("Mortgage")
    @Expose
    private List<Object> Mortgage = new ArrayList<Object>();
    @SerializedName("Options")
    @Expose
    private List<Object> Options = new ArrayList<Object>();
    @SerializedName("Restrictions_Part")
    @Expose
    private ru.balodyarecordz.autoexpert.model.autocode.RestrictionsPart RestrictionsPart;
    @SerializedName("Services")
    @Expose
    private List<Object> Services = new ArrayList<Object>();
    @SerializedName("TrucksRestrictions_Part")
    @Expose
    private Object TrucksRestrictionsPart;
    @SerializedName("VladHist")
    @Expose
    private ru.balodyarecordz.autoexpert.model.autocode.VladHist VladHist;
    @SerializedName("TOList")
    @Expose
    private List<TOList> tOList = new ArrayList<TOList>();

    public List<TOList> gettOList() {
        return tOList;
    }

    public void settOList(List<TOList> tOList) {
        this.tOList = tOList;
    }

    /**
     * 
     * @return
     *     The Type
     */
    public String getType() {
        return Type;
    }

    /**
     * 
     * @param Type
     *     The __type
     */
    public void setType(String Type) {
        this.Type = Type;
    }

    /**
     * 
     * @return
     *     The EventID
     */
    public Integer getEventID() {
        return EventID;
    }

    /**
     * 
     * @param EventID
     *     The EventID
     */
    public void setEventID(Integer EventID) {
        this.EventID = EventID;
    }

    /**
     * 
     * @return
     *     The IsMain
     */
    public Boolean getIsMain() {
        return IsMain;
    }

    /**
     * 
     * @param IsMain
     *     The IsMain
     */
    public void setIsMain(Boolean IsMain) {
        this.IsMain = IsMain;
    }

    /**
     * 
     * @return
     *     The Message
     */
    public Object getMessage() {
        return Message;
    }

    /**
     * 
     * @param Message
     *     The Message
     */
    public void setMessage(Object Message) {
        this.Message = Message;
    }

    /**
     * 
     * @return
     *     The OutServiceID
     */
    public Integer getOutServiceID() {
        return OutServiceID;
    }

    /**
     * 
     * @param OutServiceID
     *     The OutServiceID
     */
    public void setOutServiceID(Integer OutServiceID) {
        this.OutServiceID = OutServiceID;
    }

    /**
     * 
     * @return
     *     The State
     */
    public Integer getState() {
        return State;
    }

    /**
     * 
     * @param State
     *     The State
     */
    public void setState(Integer State) {
        this.State = State;
    }

    /**
     * 
     * @return
     *     The CommonInfo
     */
    public ru.balodyarecordz.autoexpert.model.autocode.CommonInfo getCommonInfo() {
        return CommonInfo;
    }

    /**
     * 
     * @param CommonInfo
     *     The CommonInfo
     */
    public void setCommonInfo(ru.balodyarecordz.autoexpert.model.autocode.CommonInfo CommonInfo) {
        this.CommonInfo = CommonInfo;
    }

    /**
     * 
     * @return
     *     The DTPPart
     */
    public ru.balodyarecordz.autoexpert.model.autocode.DTPPart getDTPPart() {
        return DTPPart;
    }

    /**
     * 
     * @param DTPPart
     *     The DTP_Part
     */
    public void setDTPPart(ru.balodyarecordz.autoexpert.model.autocode.DTPPart DTPPart) {
        this.DTPPart = DTPPart;
    }

    /**
     * 
     * @return
     *     The GTOPart
     */
    public ru.balodyarecordz.autoexpert.model.autocode.GTOPart getGTOPart() {
        return GTOPart;
    }

    /**
     * 
     * @param GTOPart
     *     The GTO_Part
     */
    public void setGTOPart(ru.balodyarecordz.autoexpert.model.autocode.GTOPart GTOPart) {
        this.GTOPart = GTOPart;
    }

    /**
     * 
     * @return
     *     The InsurancePart
     */
    public ru.balodyarecordz.autoexpert.model.autocode.InsurancePart getInsurancePart() {
        return InsurancePart;
    }

    /**
     * 
     * @param InsurancePart
     *     The Insurance_Part
     */
    public void setInsurancePart(ru.balodyarecordz.autoexpert.model.autocode.InsurancePart InsurancePart) {
        this.InsurancePart = InsurancePart;
    }

    /**
     * 
     * @return
     *     The KommercialPart
     */
    public ru.balodyarecordz.autoexpert.model.autocode.KommercialPart getKommercialPart() {
        return KommercialPart;
    }

    /**
     * 
     * @param KommercialPart
     *     The Kommercial_Part
     */
    public void setKommercialPart(ru.balodyarecordz.autoexpert.model.autocode.KommercialPart KommercialPart) {
        this.KommercialPart = KommercialPart;
    }

    /**
     * 
     * @return
     *     The MileagePart
     */
    public ru.balodyarecordz.autoexpert.model.autocode.MileagePart getMileagePart() {
        return MileagePart;
    }

    /**
     * 
     * @param MileagePart
     *     The Mileage_Part
     */
    public void setMileagePart(ru.balodyarecordz.autoexpert.model.autocode.MileagePart MileagePart) {
        this.MileagePart = MileagePart;
    }

    /**
     * 
     * @return
     *     The Mortgage
     */
    public List<Object> getMortgage() {
        return Mortgage;
    }

    /**
     * 
     * @param Mortgage
     *     The Mortgage
     */
    public void setMortgage(List<Object> Mortgage) {
        this.Mortgage = Mortgage;
    }

    /**
     * 
     * @return
     *     The Options
     */
    public List<Object> getOptions() {
        return Options;
    }

    /**
     * 
     * @param Options
     *     The Options
     */
    public void setOptions(List<Object> Options) {
        this.Options = Options;
    }

    /**
     * 
     * @return
     *     The RestrictionsPart
     */
    public ru.balodyarecordz.autoexpert.model.autocode.RestrictionsPart getRestrictionsPart() {
        return RestrictionsPart;
    }

    /**
     * 
     * @param RestrictionsPart
     *     The Restrictions_Part
     */
    public void setRestrictionsPart(ru.balodyarecordz.autoexpert.model.autocode.RestrictionsPart RestrictionsPart) {
        this.RestrictionsPart = RestrictionsPart;
    }

    /**
     * 
     * @return
     *     The Services
     */
    public List<Object> getServices() {
        return Services;
    }

    /**
     * 
     * @param Services
     *     The Services
     */
    public void setServices(List<Object> Services) {
        this.Services = Services;
    }

    /**
     * 
     * @return
     *     The TrucksRestrictionsPart
     */
    public Object getTrucksRestrictionsPart() {
        return TrucksRestrictionsPart;
    }

    /**
     * 
     * @param TrucksRestrictionsPart
     *     The TrucksRestrictions_Part
     */
    public void setTrucksRestrictionsPart(Object TrucksRestrictionsPart) {
        this.TrucksRestrictionsPart = TrucksRestrictionsPart;
    }

    /**
     * 
     * @return
     *     The VladHist
     */
    public ru.balodyarecordz.autoexpert.model.autocode.VladHist getVladHist() {
        return VladHist;
    }

    /**
     * 
     * @param VladHist
     *     The VladHist
     */
    public void setVladHist(ru.balodyarecordz.autoexpert.model.autocode.VladHist VladHist) {
        this.VladHist = VladHist;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Type);
        dest.writeValue(this.EventID);
        dest.writeValue(this.IsMain);
        dest.writeValue(this.OutServiceID);
        dest.writeValue(this.State);
        dest.writeParcelable(this.CommonInfo, flags);
        dest.writeParcelable(this.DTPPart, flags);
        dest.writeParcelable(this.GTOPart, flags);
        dest.writeParcelable(this.InsurancePart, flags);
        dest.writeParcelable(this.KommercialPart, flags);
        dest.writeParcelable(this.MileagePart, flags);
        dest.writeList(this.Mortgage);
        dest.writeList(this.Options);
        dest.writeParcelable(this.RestrictionsPart, flags);
        dest.writeList(this.Services);
        dest.writeParcelable(this.VladHist, flags);
    }

    public Data() {
    }

    protected Data(Parcel in) {
        this.Type = in.readString();
        this.EventID = (Integer) in.readValue(Integer.class.getClassLoader());
        this.IsMain = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.OutServiceID = (Integer) in.readValue(Integer.class.getClassLoader());
        this.State = (Integer) in.readValue(Integer.class.getClassLoader());
        this.CommonInfo = in.readParcelable(ru.balodyarecordz.autoexpert.model.autocode.CommonInfo.class.getClassLoader());
        this.DTPPart = in.readParcelable(ru.balodyarecordz.autoexpert.model.autocode.DTPPart.class.getClassLoader());
        this.GTOPart = in.readParcelable(ru.balodyarecordz.autoexpert.model.autocode.GTOPart.class.getClassLoader());
        this.InsurancePart = in.readParcelable(ru.balodyarecordz.autoexpert.model.autocode.InsurancePart.class.getClassLoader());
        this.KommercialPart = in.readParcelable(ru.balodyarecordz.autoexpert.model.autocode.KommercialPart.class.getClassLoader());
        this.MileagePart = in.readParcelable(ru.balodyarecordz.autoexpert.model.autocode.MileagePart.class.getClassLoader());
        this.Mortgage = new ArrayList<Object>();
        in.readList(this.Mortgage, Object.class.getClassLoader());
        this.Options = new ArrayList<Object>();
        in.readList(this.Options, Object.class.getClassLoader());
        this.RestrictionsPart = in.readParcelable(ru.balodyarecordz.autoexpert.model.autocode.RestrictionsPart.class.getClassLoader());
        this.Services = new ArrayList<Object>();
        in.readList(this.Services, Object.class.getClassLoader());
        this.VladHist = in.readParcelable(ru.balodyarecordz.autoexpert.model.autocode.VladHist.class.getClassLoader());
    }

    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
