
package ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Vehicle implements Serializable {

    @SerializedName("engineVolume")
    @Expose
    private String engineVolume;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("bodyNumber")
    @Expose
    private String bodyNumber;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("engineNumber")
    @Expose
    private String engineNumber;
    @SerializedName("vin")
    @Expose
    private String vin;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("powerHp")
    @Expose
    private String powerHp;
    @SerializedName("powerKwt")
    @Expose
    private String powerKwt;

    /**
     * 
     * @return
     *     The engineVolume
     */
    public String getEngineVolume() {
        return engineVolume;
    }

    /**
     * 
     * @param engineVolume
     *     The engineVolume
     */
    public void setEngineVolume(String engineVolume) {
        this.engineVolume = engineVolume;
    }

    /**
     * 
     * @return
     *     The color
     */
    public String getColor() {
        return color;
    }

    /**
     * 
     * @param color
     *     The color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 
     * @return
     *     The bodyNumber
     */
    public String getBodyNumber() {
        return bodyNumber;
    }

    /**
     * 
     * @param bodyNumber
     *     The bodyNumber
     */
    public void setBodyNumber(String bodyNumber) {
        this.bodyNumber = bodyNumber;
    }

    /**
     * 
     * @return
     *     The year
     */
    public String getYear() {
        return year;
    }

    /**
     * 
     * @param year
     *     The year
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 
     * @return
     *     The engineNumber
     */
    public String getEngineNumber() {
        return engineNumber;
    }

    /**
     * 
     * @param engineNumber
     *     The engineNumber
     */
    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    /**
     * 
     * @return
     *     The vin
     */
    public String getVin() {
        return vin;
    }

    /**
     * 
     * @param vin
     *     The vin
     */
    public void setVin(String vin) {
        this.vin = vin;
    }

    /**
     * 
     * @return
     *     The model
     */
    public String getModel() {
        return model;
    }

    /**
     * 
     * @param model
     *     The model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * 
     * @return
     *     The category
     */
    public String getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The powerHp
     */
    public String getPowerHp() {
        return powerHp;
    }

    /**
     * 
     * @param powerHp
     *     The powerHp
     */
    public void setPowerHp(String powerHp) {
        this.powerHp = powerHp;
    }

    /**
     * 
     * @return
     *     The powerKwt
     */
    public String getPowerKwt() {
        return powerKwt;
    }

    /**
     * 
     * @param powerKwt
     *     The powerKwt
     */
    public void setPowerKwt(String powerKwt) {
        this.powerKwt = powerKwt;
    }

}
