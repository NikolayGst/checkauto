
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class KommercialPart implements Parcelable {

    @SerializedName("Kommercial")
    @Expose
    private List<ru.balodyarecordz.autoexpert.model.autocode.Kommercial> Kommercial = new ArrayList<ru.balodyarecordz.autoexpert.model.autocode.Kommercial>();

    /**
     * 
     * @return
     *     The Kommercial
     */
    public List<ru.balodyarecordz.autoexpert.model.autocode.Kommercial> getKommercial() {
        return Kommercial;
    }

    /**
     * 
     * @param Kommercial
     *     The Kommercial
     */
    public void setKommercial(List<ru.balodyarecordz.autoexpert.model.autocode.Kommercial> Kommercial) {
        this.Kommercial = Kommercial;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.Kommercial);
    }

    public KommercialPart() {
    }

    protected KommercialPart(Parcel in) {
        this.Kommercial = in.createTypedArrayList(ru.balodyarecordz.autoexpert.model.autocode.Kommercial.CREATOR);
    }

    public static final Parcelable.Creator<KommercialPart> CREATOR = new Parcelable.Creator<KommercialPart>() {
        @Override
        public KommercialPart createFromParcel(Parcel source) {
            return new KommercialPart(source);
        }

        @Override
        public KommercialPart[] newArray(int size) {
            return new KommercialPart[size];
        }
    };
}
