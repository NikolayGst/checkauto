package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.SerializedName;

public class CaptchaInit {
    @SerializedName("HasError")
    private boolean hasError;
    @SerializedName("ImageUrl")
    private String imageUrl;
    @SerializedName("Token")
    private String token;

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
