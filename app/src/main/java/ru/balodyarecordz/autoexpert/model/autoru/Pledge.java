package ru.balodyarecordz.autoexpert.model.autoru;

public class Pledge {
    private String zalog;
    private Values values;

    public String getZalog() {
        return zalog;
    }

    public void setZalog(String zalog) {
        this.zalog = zalog;
    }

    public Values getValues() {
        return values;
    }

    public void setValues(Values values) {
        this.values = values;
    }
}
