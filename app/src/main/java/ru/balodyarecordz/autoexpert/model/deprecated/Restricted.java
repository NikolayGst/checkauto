package ru.balodyarecordz.autoexpert.model.deprecated;

import java.util.ArrayList;

public class Restricted {
    private String error;
    private String count;
    private String status;
    private ArrayList<String> records;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public ArrayList<String> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<String> records) {
        this.records = records;
    }
}
