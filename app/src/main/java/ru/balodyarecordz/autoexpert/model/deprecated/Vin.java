package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.SerializedName;

public class Vin {
    @SerializedName("Filter")
    private Filter filter;
    @SerializedName("PageSize")
    private String pageSize = "10";
    @SerializedName("PageIndex")
    private String pageIndex = "0";

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }
}
