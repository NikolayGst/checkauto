package ru.balodyarecordz.autoexpert.model.deprecated;

public class CarDoc {
    private String number;
    private String issued;
    private String date;

    public CarDoc() {
    }

    public CarDoc( String number, String issued, String date) {
        this.number = number;
        this.issued = issued;
        this.date = date;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
