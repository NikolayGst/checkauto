package ru.balodyarecordz.autoexpert.model.deprecated;

public class ContractModel {
    private DocHeader docHeader;
    private Price price;
    private Person buyer;
    private Person seller;
    private CarInfo carInfo;

    public DocHeader getDocHeader() {
        return docHeader;
    }

    public void setDocHeader(DocHeader docHeader) {
        this.docHeader = docHeader;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Person getBuyer() {
        return buyer;
    }

    public void setBuyer(Person buyer) {
        this.buyer = buyer;
    }

    public Person getSeller() {
        return seller;
    }

    public void setSeller(Person seller) {
        this.seller = seller;
    }

    public CarInfo getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(CarInfo carInfo) {
        this.carInfo = carInfo;
    }
}

