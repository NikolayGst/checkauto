
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DTPPart implements Parcelable {

    @SerializedName("DTP")
    @Expose
    private List<DTP> DTP = new ArrayList<DTP>();
    @SerializedName("DTPComment")
    @Expose
    private String DTPComment;

    /**
     * 
     * @return
     *     The DTP
     */
    public List<DTP> getDTP() {
        return DTP;
    }

    /**
     * 
     * @param DTP
     *     The DTP
     */
    public void setDTP(List<DTP> DTP) {
        this.DTP = DTP;
    }

    /**
     * 
     * @return
     *     The DTPComment
     */
    public String getDTPComment() {
        return DTPComment;
    }

    /**
     * 
     * @param DTPComment
     *     The DTPComment
     */
    public void setDTPComment(String DTPComment) {
        this.DTPComment = DTPComment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.DTP);
        dest.writeString(this.DTPComment);
    }

    public DTPPart() {
    }

    protected DTPPart(Parcel in) {
        this.DTP = new ArrayList<DTP>();
        in.readList(this.DTP, Object.class.getClassLoader());
        this.DTPComment = in.readString();
    }

    public static final Parcelable.Creator<DTPPart> CREATOR = new Parcelable.Creator<DTPPart>() {
        @Override
        public DTPPart createFromParcel(Parcel source) {
            return new DTPPart(source);
        }

        @Override
        public DTPPart[] newArray(int size) {
            return new DTPPart[size];
        }
    };
}
