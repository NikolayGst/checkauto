package ru.balodyarecordz.autoexpert.model;

public class ErrorResponse {
    private String code;
    private String values;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }
}
