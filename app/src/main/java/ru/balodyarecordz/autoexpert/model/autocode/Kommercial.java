
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kommercial implements Parcelable {

    @SerializedName("KommercialLicenseStop")
    @Expose
    private String KommercialLicenseStop;
    @SerializedName("KommercialStartDate")
    @Expose
    private String KommercialStartDate;

    /**
     * 
     * @return
     *     The KommercialLicenseStop
     */
    public String getKommercialLicenseStop() {
        return KommercialLicenseStop;
    }

    /**
     * 
     * @param KommercialLicenseStop
     *     The KommercialLicenseStop
     */
    public void setKommercialLicenseStop(String KommercialLicenseStop) {
        this.KommercialLicenseStop = KommercialLicenseStop;
    }

    /**
     * 
     * @return
     *     The KommercialStartDate
     */
    public String getKommercialStartDate() {
        return KommercialStartDate;
    }

    /**
     * 
     * @param KommercialStartDate
     *     The KommercialStartDate
     */
    public void setKommercialStartDate(String KommercialStartDate) {
        this.KommercialStartDate = KommercialStartDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.KommercialLicenseStop);
        dest.writeString(this.KommercialStartDate);
    }

    public Kommercial() {
    }

    protected Kommercial(Parcel in) {
        this.KommercialLicenseStop = in.readString();
        this.KommercialStartDate = in.readString();
    }

    public static final Parcelable.Creator<Kommercial> CREATOR = new Parcelable.Creator<Kommercial>() {
        @Override
        public Kommercial createFromParcel(Parcel source) {
            return new Kommercial(source);
        }

        @Override
        public Kommercial[] newArray(int size) {
            return new Kommercial[size];
        }
    };
}
