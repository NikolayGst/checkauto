package ru.balodyarecordz.autoexpert.model.deprecated;

public class Person {
    private String name;
    private String livingAddress;
    private String regAddress;
    private String passSeries;
    private String passNumber;
    private String passDate;
    private String passWho;

    public Person() {
    }

    public Person(String name, String livingAddress, String regAddress, String passSeries, String passNumber, String passDate, String passWho) {
        this.name = name;
        this.livingAddress = livingAddress;
        this.regAddress = regAddress;
        this.passSeries = passSeries;
        this.passNumber = passNumber;
        this.passDate = passDate;
        this.passWho = passWho;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLivingAddress() {
        return livingAddress;
    }

    public void setLivingAddress(String livingAddress) {
        this.livingAddress = livingAddress;
    }

    public String getRegAddress() {
        return regAddress;
    }

    public void setRegAddress(String regAddress) {
        this.regAddress = regAddress;
    }

    public String getPassSeries() {
        return passSeries;
    }

    public void setPassSeries(String passSeries) {
        this.passSeries = passSeries;
    }

    public String getPassNumber() {
        return passNumber;
    }

    public void setPassNumber(String passNumber) {
        this.passNumber = passNumber;
    }

    public String getPassDate() {
        return passDate;
    }

    public void setPassDate(String passDate) {
        this.passDate = passDate;
    }

    public String getPassWho() {
        return passWho;
    }

    public void setPassWho(String passWho) {
        this.passWho = passWho;
    }
}
