
package ru.balodyarecordz.autoexpert.model.fssp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Fssp {

    @SerializedName("fssp_name")
    @Expose
    private FsspName fsspName;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("values")
    @Expose
    private Object values;// = new ArrayList<List<String>>();

    /**
     * 
     * @return
     *     The fsspName
     */
    public FsspName getFsspName() {
        return fsspName;
    }

    /**
     * 
     * @param fsspName
     *     The fssp_name
     */
    public void setFsspName(FsspName fsspName) {
        this.fsspName = fsspName;
    }

    /**
     * 
     * @return
     *     The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The values
     */
    public Object getValues() {
        return values;
    }

    /**
     * 
     * @param values
     *     The values
     */
    public void setValues(List<List<String>> values) {
        this.values = values;
    }

}
