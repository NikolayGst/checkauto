package ru.balodyarecordz.autoexpert.model.deprecated;

public class DocHeader {
    private String date;
    private String city;

    public DocHeader(String city, String date) {
        this.city = city;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
