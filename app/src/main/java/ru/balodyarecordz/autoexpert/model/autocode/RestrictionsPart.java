
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RestrictionsPart implements Parcelable {

    @SerializedName("Restrictions")
    @Expose
    private List<Restriction> Restrictions = new ArrayList<Restriction>();
    @SerializedName("RestrictionsComment")
    @Expose
    private String RestrictionsComment;

    /**
     * 
     * @return
     *     The Restrictions
     */
    public List<Restriction> getRestrictions() {
        return Restrictions;
    }

    /**
     * 
     * @param Restrictions
     *     The Restrictions
     */
    public void setRestrictions(List<Restriction> Restrictions) {
        this.Restrictions = Restrictions;
    }

    /**
     * 
     * @return
     *     The RestrictionsComment
     */
    public String getRestrictionsComment() {
        return RestrictionsComment;
    }

    /**
     * 
     * @param RestrictionsComment
     *     The RestrictionsComment
     */
    public void setRestrictionsComment(String RestrictionsComment) {
        this.RestrictionsComment = RestrictionsComment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.Restrictions);
        dest.writeString(this.RestrictionsComment);
    }

    public RestrictionsPart() {
    }

    protected RestrictionsPart(Parcel in) {
        this.Restrictions = in.createTypedArrayList(Restriction.CREATOR);
        this.RestrictionsComment = in.readString();
    }

    public static final Parcelable.Creator<RestrictionsPart> CREATOR = new Parcelable.Creator<RestrictionsPart>() {
        @Override
        public RestrictionsPart createFromParcel(Parcel source) {
            return new RestrictionsPart(source);
        }

        @Override
        public RestrictionsPart[] newArray(int size) {
            return new RestrictionsPart[size];
        }
    };
}
