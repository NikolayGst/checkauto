package ru.balodyarecordz.autoexpert.model.autoru;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Values {
    @SerializedName("Идентификационный номер")
    @NonNull
    private String vin;
    @SerializedName("Марка")
    @NonNull
    private String mark;
    @SerializedName("Модель")
    @NonNull
    private String model;
    @SerializedName("Модификация")
    @NonNull
    private String modify;
    @SerializedName("Тип кузова")
    @NonNull
    private String kuzov;
    @SerializedName("Количество дверей")
    @NonNull
    private String doorsCount;
    @SerializedName("Тип транспортного средства")
    @NonNull
    private String type;
    @SerializedName("Модельный год")
    @NonNull
    private String year;
    @SerializedName("Сборочный завод")
    @NonNull
    private String factory;
    @SerializedName("Страна сборки")
    @NonNull
    private String country;
    @SerializedName("Страна происхождения")
    @NonNull
    private String countryMain;
    @SerializedName("Серийный номер")
    @NonNull
    private String serialNumber;
    @SerializedName("VIN-код")
    @NonNull
    private String vinCode;
    @SerializedName("Банк")
    @NonNull
    private String bank;

    @NonNull
    public String getVinCode() {
        return vinCode;
    }

    public void setVinCode(@NonNull String vinCode) {
        this.vinCode = vinCode;
    }

    @NonNull
    public String getBank() {
        return bank;
    }

    public void setBank(@NonNull String bank) {
        this.bank = bank;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModify() {
        return modify;
    }

    public void setModify(String modify) {
        this.modify = modify;
    }

    public String getKuzov() {
        return kuzov;
    }

    public void setKuzov(String kuzov) {
        this.kuzov = kuzov;
    }

    public String getDoorsCount() {
        return doorsCount;
    }

    public void setDoorsCount(String doorsCount) {
        this.doorsCount = doorsCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryMain() {
        return countryMain;
    }

    public void setCountryMain(String countryMain) {
        this.countryMain = countryMain;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
