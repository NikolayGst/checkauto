
package ru.balodyarecordz.autoexpert.model.gibdd.GibddDtp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RequestResult {

    @SerializedName("errorDescription")
    @Expose
    private String errorDescription;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("Accidents")
    @Expose
    private List<Object> accidents = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The errorDescription
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * 
     * @param errorDescription
     *     The errorDescription
     */
    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    /**
     * 
     * @return
     *     The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * 
     * @param statusCode
     *     The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * 
     * @return
     *     The accidents
     */
    public List<Object> getAccidents() {
        return accidents;
    }

    /**
     * 
     * @param accidents
     *     The Accidents
     */
    public void setAccidents(List<Object> accidents) {
        this.accidents = accidents;
    }

}
