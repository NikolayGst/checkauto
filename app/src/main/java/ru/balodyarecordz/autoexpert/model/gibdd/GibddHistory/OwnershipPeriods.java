
package ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OwnershipPeriods implements Serializable  {

    @SerializedName("ownershipPeriod")
    @Expose
    private List<OwnershipPeriod> ownershipPeriod = new ArrayList<OwnershipPeriod>();

    /**
     * 
     * @return
     *     The ownershipPeriod
     */
    public List<OwnershipPeriod> getOwnershipPeriod() {
        return ownershipPeriod;
    }

    /**
     * 
     * @param ownershipPeriod
     *     The ownershipPeriod
     */
    public void setOwnershipPeriod(List<OwnershipPeriod> ownershipPeriod) {
        this.ownershipPeriod = ownershipPeriod;
    }

}
