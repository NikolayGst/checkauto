
package ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OwnershipPeriod implements Serializable {

    @SerializedName("simplePersonType")
    @Expose
    private String simplePersonType;
    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("to")
    @Expose
    private String to;

    /**
     * 
     * @return
     *     The simplePersonType
     */
    public String getSimplePersonType() {
        return simplePersonType;
    }

    /**
     * 
     * @param simplePersonType
     *     The simplePersonType
     */
    public void setSimplePersonType(String simplePersonType) {
        this.simplePersonType = simplePersonType;
    }

    /**
     * 
     * @return
     *     The from
     */
    public String getFrom() {
        return from;
    }

    /**
     * 
     * @param from
     *     The from
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * 
     * @return
     *     The to
     */
    public String getTo() {
        return to;
    }

    /**
     * 
     * @param to
     *     The to
     */
    public void setTo(String to) {
        this.to = to;
    }

}
