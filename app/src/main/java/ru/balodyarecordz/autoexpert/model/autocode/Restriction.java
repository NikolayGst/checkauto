
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Restriction implements Parcelable {

    @SerializedName("OgrKod")
    @Expose
    private String OgrKod;
    @SerializedName("OgrName")
    @Expose
    private String OgrName;
    @SerializedName("OgrRes")
    @Expose
    private String OgrRes;

    /**
     * 
     * @return
     *     The OgrKod
     */
    public String getOgrKod() {
        return OgrKod;
    }

    /**
     * 
     * @param OgrKod
     *     The OgrKod
     */
    public void setOgrKod(String OgrKod) {
        this.OgrKod = OgrKod;
    }

    /**
     * 
     * @return
     *     The OgrName
     */
    public String getOgrName() {
        return OgrName;
    }

    /**
     * 
     * @param OgrName
     *     The OgrName
     */
    public void setOgrName(String OgrName) {
        this.OgrName = OgrName;
    }

    /**
     * 
     * @return
     *     The OgrRes
     */
    public String getOgrRes() {
        return OgrRes;
    }

    /**
     * 
     * @param OgrRes
     *     The OgrRes
     */
    public void setOgrRes(String OgrRes) {
        this.OgrRes = OgrRes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.OgrKod);
        dest.writeString(this.OgrName);
        dest.writeString(this.OgrRes);
    }

    public Restriction() {
    }

    protected Restriction(Parcel in) {
        this.OgrKod = in.readString();
        this.OgrName = in.readString();
        this.OgrRes = in.readString();
    }

    public static final Parcelable.Creator<Restriction> CREATOR = new Parcelable.Creator<Restriction>() {
        @Override
        public Restriction createFromParcel(Parcel source) {
            return new Restriction(source);
        }

        @Override
        public Restriction[] newArray(int size) {
            return new Restriction[size];
        }
    };
}
