
package ru.balodyarecordz.autoexpert.model.gibdd.GibddRestritions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RestritionsRecord {

    @SerializedName("regname")
    @Expose
    private String regname;
    @SerializedName("gid")
    @Expose
    private String gid;
    @SerializedName("tsyear")
    @Expose
    private Object tsyear;
    @SerializedName("dateadd")
    @Expose
    private String dateadd;
    @SerializedName("regid")
    @Expose
    private Integer regid;
    @SerializedName("divtype")
    @Expose
    private Integer divtype;
    @SerializedName("dateogr")
    @Expose
    private String dateogr;
    @SerializedName("ogrkod")
    @Expose
    private Integer ogrkod;
    @SerializedName("divid")
    @Expose
    private Integer divid;
    @SerializedName("tsmodel")
    @Expose
    private String tsmodel;

    /**
     * 
     * @return
     *     The regname
     */
    public String getRegname() {
        return regname;
    }

    /**
     * 
     * @param regname
     *     The regname
     */
    public void setRegname(String regname) {
        this.regname = regname;
    }

    /**
     * 
     * @return
     *     The gid
     */
    public String getGid() {
        return gid;
    }

    /**
     * 
     * @param gid
     *     The gid
     */
    public void setGid(String gid) {
        this.gid = gid;
    }

    /**
     * 
     * @return
     *     The tsyear
     */
    public Object getTsyear() {
        return tsyear;
    }

    /**
     * 
     * @param tsyear
     *     The tsyear
     */
    public void setTsyear(Object tsyear) {
        this.tsyear = tsyear;
    }

    /**
     * 
     * @return
     *     The dateadd
     */
    public String getDateadd() {
        return dateadd;
    }

    /**
     * 
     * @param dateadd
     *     The dateadd
     */
    public void setDateadd(String dateadd) {
        this.dateadd = dateadd;
    }

    /**
     * 
     * @return
     *     The regid
     */
    public Integer getRegid() {
        return regid;
    }

    /**
     * 
     * @param regid
     *     The regid
     */
    public void setRegid(Integer regid) {
        this.regid = regid;
    }

    /**
     * 
     * @return
     *     The divtype
     */
    public Integer getDivtype() {
        return divtype;
    }

    /**
     * 
     * @param divtype
     *     The divtype
     */
    public void setDivtype(Integer divtype) {
        this.divtype = divtype;
    }

    /**
     * 
     * @return
     *     The dateogr
     */
    public String getDateogr() {
        return dateogr;
    }

    /**
     * 
     * @param dateogr
     *     The dateogr
     */
    public void setDateogr(String dateogr) {
        this.dateogr = dateogr;
    }

    /**
     * 
     * @return
     *     The ogrkod
     */
    public Integer getOgrkod() {
        return ogrkod;
    }

    /**
     * 
     * @param ogrkod
     *     The ogrkod
     */
    public void setOgrkod(Integer ogrkod) {
        this.ogrkod = ogrkod;
    }

    /**
     * 
     * @return
     *     The divid
     */
    public Integer getDivid() {
        return divid;
    }

    /**
     * 
     * @param divid
     *     The divid
     */
    public void setDivid(Integer divid) {
        this.divid = divid;
    }

    /**
     * 
     * @return
     *     The tsmodel
     */
    public String getTsmodel() {
        return tsmodel;
    }

    /**
     * 
     * @param tsmodel
     *     The tsmodel
     */
    public void setTsmodel(String tsmodel) {
        this.tsmodel = tsmodel;
    }

}
