
package ru.balodyarecordz.autoexpert.model.reestr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("DocId")
    @Expose
    private String DocId;
    @SerializedName("RegDate")
    @Expose
    private String RegDate;
    @SerializedName("RegNumber")
    @Expose
    private String RegNumber;
    @SerializedName("RegInfoText")
    @Expose
    private String RegInfoText;
    @SerializedName("Pledgor")
    @Expose
    private String Pledgor;
    @SerializedName("Mortgagees")
    @Expose
    private String Mortgagees;

    /**
     * 
     * @return
     *     The DocId
     */
    public String getDocId() {
        return DocId;
    }

    /**
     * 
     * @param DocId
     *     The DocId
     */
    public void setDocId(String DocId) {
        this.DocId = DocId;
    }

    /**
     * 
     * @return
     *     The RegDate
     */
    public String getRegDate() {
        return RegDate;
    }

    /**
     * 
     * @param RegDate
     *     The RegDate
     */
    public void setRegDate(String RegDate) {
        this.RegDate = RegDate;
    }

    /**
     * 
     * @return
     *     The RegNumber
     */
    public String getRegNumber() {
        return RegNumber;
    }

    /**
     * 
     * @param RegNumber
     *     The RegNumber
     */
    public void setRegNumber(String RegNumber) {
        this.RegNumber = RegNumber;
    }

    /**
     * 
     * @return
     *     The RegInfoText
     */
    public String getRegInfoText() {
        return RegInfoText;
    }

    /**
     * 
     * @param RegInfoText
     *     The RegInfoText
     */
    public void setRegInfoText(String RegInfoText) {
        this.RegInfoText = RegInfoText;
    }

    /**
     * 
     * @return
     *     The Pledgor
     */
    public String getPledgor() {
        return Pledgor;
    }

    /**
     * 
     * @param Pledgor
     *     The Pledgor
     */
    public void setPledgor(String Pledgor) {
        this.Pledgor = Pledgor;
    }

    /**
     * 
     * @return
     *     The Mortgagees
     */
    public String getMortgagees() {
        return Mortgagees;
    }

    /**
     * 
     * @param Mortgagees
     *     The Mortgagees
     */
    public void setMortgagees(String Mortgagees) {
        this.Mortgagees = Mortgagees;
    }

}
