
package ru.balodyarecordz.autoexpert.model.fssp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FsspName {

    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("patronymic")
    @Expose
    private String patronymic;
    @SerializedName("bitrhdate")
    @Expose
    private String bitrhdate;

    /**
     * 
     * @return
     *     The firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * 
     * @param firstname
     *     The firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * 
     * @return
     *     The lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * 
     * @param lastname
     *     The lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * 
     * @return
     *     The patronymic
     */
    public String getPatronymic() {
        return patronymic;
    }

    /**
     * 
     * @param patronymic
     *     The patronymic
     */
    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    /**
     * 
     * @return
     *     The bitrhdate
     */
    public String getBitrhdate() {
        return bitrhdate;
    }

    /**
     * 
     * @param bitrhdate
     *     The bitrhdate
     */
    public void setBitrhdate(String bitrhdate) {
        this.bitrhdate = bitrhdate;
    }

}
