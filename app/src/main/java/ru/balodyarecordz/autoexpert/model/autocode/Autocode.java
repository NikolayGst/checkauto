
package ru.balodyarecordz.autoexpert.model.autocode;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.balodyarecordz.autoexpert.utils.Utils;

public class Autocode implements Parcelable {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("vin")
    @Expose
    private String vin;
    @SerializedName("values")
    @Expose
    private Values values;
    private String captcha;

    public String getCaptcha() {
        return captcha;
    }

    public Bitmap getCaptchaImage() {
        return Utils.base64ToBitmap(captcha);
    }

    /**
     * @return The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return The vin
     */
    public String getVin() {
        return vin;
    }

    /**
     * @param vin The vin
     */
    public void setVin(String vin) {
        this.vin = vin;
    }

    /**
     * @return The values
     */
    public Object getValues() {
        return values;
    }

    /**
     * @param values The values
     */
    public void setValues(Values values) {
        this.values = values;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.code);
        dest.writeString(this.vin);
        dest.writeParcelable(this.values, flags);
    }

    public Autocode() {
    }

    protected Autocode(Parcel in) {
        this.code = (Integer) in.readValue(Integer.class.getClassLoader());
        this.vin = in.readString();
        this.values = in.readParcelable(Values.class.getClassLoader());
    }

    public static final Parcelable.Creator<Autocode> CREATOR = new Parcelable.Creator<Autocode>() {
        @Override
        public Autocode createFromParcel(Parcel source) {
            return new Autocode(source);
        }

        @Override
        public Autocode[] newArray(int size) {
            return new Autocode[size];
        }
    };
}
