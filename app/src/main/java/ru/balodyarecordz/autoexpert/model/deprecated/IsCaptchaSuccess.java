package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ghost Surfer on 03.12.2015.
 */
public class IsCaptchaSuccess {
    @SerializedName("IsOk")
    private boolean isOk;

    public boolean isOk() {
        return isOk;
    }

    public void setOk(boolean ok) {
        isOk = ok;
    }
}
