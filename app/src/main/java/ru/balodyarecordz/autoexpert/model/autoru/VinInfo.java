package ru.balodyarecordz.autoexpert.model.autoru;

import java.util.List;

public class VinInfo {
    private String vin;
    private String pledge;
    private Val values;
    private String code;
    private List<String> marks;

    public String getPledge() {
        return pledge;
    }

    public void setPledge(String pledge) {
        this.pledge = pledge;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<String> getMarks() {
        return marks;
    }

    public void setMarks(List<String> marks) {
        this.marks = marks;
    }

    public Val getValues() {
        return values;
    }

    public void setValues(Val values) {
        this.values = values;
    }
}
