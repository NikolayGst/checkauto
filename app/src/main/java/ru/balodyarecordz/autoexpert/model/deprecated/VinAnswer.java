package ru.balodyarecordz.autoexpert.model.deprecated;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class VinAnswer {
    @SerializedName("Items")
    private ArrayList<VinData> items;
    @SerializedName("NeedCapcha")
    private boolean isNeedCaptcha;
    @SerializedName("TotalCount")
    private int count;

    public ArrayList<VinData> getItems() {
        return items;
    }

    public void setItems(ArrayList<VinData> items) {
        this.items = items;
    }

    public boolean isNeedCaptcha() {
        return isNeedCaptcha;
    }

    public void setNeedCaptcha(boolean needCaptcha) {
        isNeedCaptcha = needCaptcha;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
