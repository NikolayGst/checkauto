package ru.balodyarecordz.autoexpert.model.deprecated;

public class Price {
    private String price;
    private String priceString;

    public Price(String price, String priceString) {
        this.price = price;
        this.priceString = priceString;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceString() {
        return priceString;
    }

    public void setPriceString(String priceString) {
        this.priceString = priceString;
    }
}
