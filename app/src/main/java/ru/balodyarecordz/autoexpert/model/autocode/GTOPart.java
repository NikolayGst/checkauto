
package ru.balodyarecordz.autoexpert.model.autocode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GTOPart implements Parcelable {

    @SerializedName("GTO")
    @Expose
    private List<GTO> GTO = new ArrayList<>();
    @SerializedName("GTOComment")
    @Expose
    private String GTOComment;

    /**
     * 
     * @return
     *     The GTO
     */
    public List<GTO> getGTO() {
        return GTO;
    }

    /**
     * 
     * @param GTO
     *     The GTO
     */
    public void setGTO(List<GTO> GTO) {
        this.GTO = GTO;
    }

    /**
     * 
     * @return
     *     The GTOComment
     */
    public String getGTOComment() {
        return GTOComment;
    }

    /**
     * 
     * @param GTOComment
     *     The GTOComment
     */
    public void setGTOComment(String GTOComment) {
        this.GTOComment = GTOComment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.GTO);
        dest.writeString(this.GTOComment);
    }

    public GTOPart() {
    }

    protected GTOPart(Parcel in) {
        this.GTO = in.createTypedArrayList(ru.balodyarecordz.autoexpert.model.autocode.GTO.CREATOR);
        this.GTOComment = in.readString();
    }

    public static final Creator<GTOPart> CREATOR = new Creator<GTOPart>() {
        @Override
        public GTOPart createFromParcel(Parcel source) {
            return new GTOPart(source);
        }

        @Override
        public GTOPart[] newArray(int size) {
            return new GTOPart[size];
        }
    };
}
