package ru.balodyarecordz.autoexpert.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.Values;
import ru.balodyarecordz.autoexpert.db.VinDBDataSource;
import ru.balodyarecordz.autoexpert.model.autocode.Autocode;
import ru.balodyarecordz.autoexpert.model.autocode.AutocodeCaptcha;
import ru.balodyarecordz.autoexpert.network.API;
import ru.balodyarecordz.autoexpert.utils.GsonUtils;
import ru.balodyarecordz.autoexpert.utils.Utils;

public class RegistrationActivity extends Env implements API.OnConnectionTimeoutListener {

    @Bind(R.id.register_textView_registrationActivity)
    TextView mRegister;
    @Bind(R.id.url_textView_registrationActivity)
    TextView mMosUrl;
    @Bind(R.id.login_editText_registrationActivity)
    EditText mLogin;
    @Bind(R.id.password_editText_registrationActivity)
    EditText mPass;

    private String mCaptcha;
    private AutocodeCaptcha mAutocodeCaptcha;
    API.IServerApi mServerApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        mRegister.setText(Html.fromHtml(getString(R.string.register)));
        mRegister.setOnClickListener(e -> openUrl(getString(R.string.autocode_reg_url)));
        mMosUrl.setText(Html.fromHtml(getString(R.string.autocode_info)));
        mMosUrl.setOnClickListener(e -> openUrl(getString(R.string.mos_url)));

        setAutocode();
    }

    @OnClick(R.id.auth_button_registrationActivity)
    public void authorization() {
        String vin = getIntent().getStringExtra(Values.VIN_TAG);
        String sts = getIntent().getStringExtra(Values.STS_TAG);
        if (mLogin.length() == 0) {
            mLogin.setError(getString(R.string.field_cant_null));
            return;
        }
        if (mPass.length() == 0) {
            mPass.setError(getString(R.string.field_cant_null));
            return;
        }
        if (sts != null) {
            auth(vin, sts);
        } else {
            showStsDialog(vin);
        }


//        serverApi.getAutocodeWithAutorization(mLogin.getText().toString(),
//                mPass.getText().toString(),
//                vin,
//                sts).enqueue(new Callback<Autocode>() {
//                                 @Override
//                                 public void onResponse(Call<Autocode> call, Response<Autocode> response) {
//
//                                 }
//
//                                 @Override
//                                 public void onFailure(Call<Autocode> call, Throwable t) {
//                                    hideDialog();
//                                 }
//                             }
//        );
    }

    private void auth(String vin, String sts) {
        showDialog(getString(R.string.loading_data), this);
        Utils.getGeneralCount(this);
        mServerApi = API.getRetrofit(getString(R.string.api_url), this).create(API.IServerApi.class);
        mServerApi.getAutocodeWithAutorizationResp(mLogin.getText().toString(),
                mPass.getText().toString(),
                vin,
                sts).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideDialog();
                String s = API.bodyToString(response);
                try {
                    Autocode autocode = (GsonUtils.fromJson(s, Autocode.class));
                } catch (Exception e) {
                    e.printStackTrace();
                    mAutocodeCaptcha = (GsonUtils.fromJson(s, AutocodeCaptcha.class));
                    showCaptchaDialog(mAutocodeCaptcha.getCaptchaImage());
                    return;
                }
                Autocode autocode = GsonUtils.fromJson(s, Autocode.class);
                if (autocode == null) {
                    makeInfoDialog(getString(R.string.wrong_pass), false);
                    return;
                }
                if (autocode.getCode() == 300) {
                    makeInfoDialog(getString(R.string.wrong_pass), false);
                    return;
                }
                Utils.incSuccess(RegistrationActivity.this);
                showRateDialog();
                Utils.saveLogin(RegistrationActivity.this, mLogin.getText().toString());
                Utils.savePass(RegistrationActivity.this, mPass.getText().toString());
                Intent intent = new Intent(RegistrationActivity.this, AutocodeActivity.class);
                intent.putExtra(getString(R.string.vin_data_tag), vin);
                intent.putExtra(getString(R.string.sts_data_tag), sts);
                intent.putExtra(Values.AUTOCODE_TAG, API.bodyToString(response));
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideDialog();
            }
        });
    }

    private void getAutocodeWithCaptcha(AutocodeCaptcha autocodeCaptcha) {
        showDialog(getString(R.string.loading_data), this);
        mServerApi.getAutocodeWithAutorizationRespCaptcha(autocodeCaptcha.getVin(),
                autocodeCaptcha.getSts(),
                autocodeCaptcha.getSes(),
                autocodeCaptcha.getRkey(),
                mCaptcha).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String s = API.bodyToString(response);
                Utils.incSuccess(RegistrationActivity.this);
                showRateDialog();
                Utils.saveLogin(RegistrationActivity.this, mLogin.getText().toString());
                Utils.savePass(RegistrationActivity.this, mPass.getText().toString());
                Intent intent = new Intent(RegistrationActivity.this, AutocodeActivity.class);
                intent.putExtra(getString(R.string.vin_data_tag), autocodeCaptcha.getVin());
                intent.putExtra(getString(R.string.sts_data_tag), autocodeCaptcha.getSts());
                intent.putExtra(Values.AUTOCODE_TAG, s);
                startActivity(intent);
                hideDialog();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideDialog();
            }
        });
    }

    private void showCaptchaDialog(Bitmap bmp) {
        if (bmp == null) {
            makeInfoDialogWithoutHeader(getString(R.string.service_down));
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_captcha, null);
        ImageView imageView = (ImageView) myView.findViewById(R.id.captchaImage_imageView);
        imageView.setImageBitmap(bmp);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        EditText captchaText = (EditText) myView.findViewById(R.id.captchaText_editText);
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Button button2 = (Button) myView.findViewById(R.id.cancelButton);
        button2.setOnClickListener(e -> dialog.dismiss());
        button.setOnClickListener(e -> {
            mCaptcha = captchaText.getText().toString();
            if (captchaText.getText().length() == 0) {
                captchaText.setError(getString(R.string.field_cant_null));
                return;
            }
            getAutocodeWithCaptcha(mAutocodeCaptcha);
            dialog.dismiss();
        });
        if (!this.isFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void onConnectionTimeout() {
        try {
            hideDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        makeInfoDialogWithoutHeader(getString(R.string.service_down));
    }

    void showStsDialog(String mVinCode) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_sts, null);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Button button2 = (Button) myView.findViewById(R.id.cancelButton);
        button2.setOnClickListener(e -> dialog.dismiss());
        EditText captchaText = (EditText) myView.findViewById(R.id.stsText_editText);
//        captchaText.setText("5017492064");
        button.setOnClickListener(e -> {
            if (captchaText.getText().toString().length() < 1) {
                captchaText.setError(getString(R.string.field_cant_null));
            } else {
                VinDBDataSource vinDBDataSource = new VinDBDataSource(RegistrationActivity.this);
                vinDBDataSource.open();
                vinDBDataSource.updateSts(captchaText.getText().toString());
                vinDBDataSource.close();
                auth(mVinCode, captchaText.getText().toString());
                dialog.dismiss();
            }
        });
        if (!this.isFinishing()) {
            dialog.show();
        }
    }

    @OnClick(R.id.example_button_registrationActivity)
    public void example() {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("example_autocode.txt"), "UTF-8"));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                builder.append(mLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }
        Intent intent = new Intent(RegistrationActivity.this, AutocodeActivity.class);
        intent.putExtra(Values.AUTOCODE_TAG, builder.toString());
        startActivity(intent);
    }
}
