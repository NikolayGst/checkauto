package ru.balodyarecordz.autoexpert.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.github.pinball83.maskededittext.MaskedEditText;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.fragment.WarningDialogFragment;
import ru.balodyarecordz.autoexpert.model.Imei;
import ru.balodyarecordz.autoexpert.model.Purchase;
import ru.balodyarecordz.autoexpert.network.API;
import ru.balodyarecordz.autoexpert.utils.Utils;
import ru.balodyarecordz.autoexpert.view.helper.OnDialogSkipListener;

public class MainScreenActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnFocusChangeListener, View.OnClickListener, API.OnConnectionTimeoutListener {

    private ListView mDrawerList;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private Purchase mPurchase;
    private int count;
    private boolean mPro;
    private static final long DRAWER_CLOSE_DELAY_MS = 250;
    private static final String NAV_ITEM_ID = "navItemId";

    public static final String TAG = "Purchase";

    private final Handler mDrawerActionHandler = new Handler();

    private int mNavItemId;
    private ActionBarDrawerToggle mDrawerToggle;
    private boolean mBound = false;
    private RecyclerView mRecyclerView;
    private Button mShowAll;
    private static ProgressBar mProgressBar;
    private static TextView mName;
    private static TextView mNumber;
    private static View mProfileLayout;
    private Button mCurrentOrder;
    private BroadcastReceiver mReceiver;
    private NavigationView mNavigationView;
    private String mPhone;
    private String mVin;
    //private TelephonyManager mngr;

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.phone_editText_mainScreenActivity)
    MaskedEditText mPhoneEdit;
    @Bind(R.id.vin_editText_mainScreenActivity)
    EditText mVinEdit;
    @Bind(R.id.vinInfo_text)
    TextView mVinText;
    @Bind(R.id.phoneInfo_text)
    TextView mPhoneText;
    @Bind(R.id.vinInfo_imageButton_mainScreenActivity)
    ImageButton mVinInfo;
    @Bind(R.id.phoneInfo_imageButton_mainScreenActivity)
    ImageButton mPhoneInfo;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.done_button_toolbar)
    Button mDone;
    @Bind(R.id.cancel_button_toolbar)
    Button mCancel;
    @Bind(R.id.myChecks_button_navigationView)
    Button mMyChecks;
    @Bind(R.id.ratingBar)
    RatingBar mRatingBar;
    @Bind(R.id.expertHelp_Button_activityMainScreen)
    Button expertHelp;
    @Bind(R.id.textView11)
    TextView txtView11;
    @BindString(R.string.vin_info)
    String mVinDialog;
    @Bind(R.id.title_textView_mainToolbar)
    TextView mToolbarTitle;
    @Bind(R.id.prefix_textView_mainScreenActivity)
    TextView mPrefix;
    @Bind(R.id.autoExpert_layout_mainToolbar)
    View mAutoExpert;
    private API.IServerApi mServerApi;
    private ProgressDialog mProgressDialog;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        ButterKnife.bind(this);
        Purchase.getInstance().init(this);
        mServerApi = API.getRetrofit(getString(R.string.api_url), this).create(API.IServerApi.class);
        mPurchase = Purchase.getInstance();
      /*  mngr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        Log.d("main", "imei: " + mngr.getDeviceId());*/
        mToolbarTitle.setVisibility(View.GONE);
        mAutoExpert.setVisibility(View.VISIBLE);
        mVinEdit.setOnFocusChangeListener(this);
        mPhoneEdit.setOnFocusChangeListener(this);
        Utils.setupEditText(mDrawerLayout, this);
        initToolbar();
        ViewTarget target = new ViewTarget(R.id.phone_editText_mainScreenActivity, this);
        mMyChecks.setOnClickListener(this);
        initRatingBar();
//        startActivity(new Intent(this, FsspActivity.class));
//        test();
//        playShowcase(target);
    }

   /* private void setTestWatcher() {
        mPhoneEdit.setLongClickable(true);
        mPhoneEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = Utils.removeNonDigits(editable.toString());
                editable.clear();
                editable.append(s);
            }
        });
    }*/

    private void initRatingBar() {
        Utils.setupRatingBar(mRatingBar, this);
    }

    private void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void clear() {
        new Handler().postDelayed(() -> {
            mVinText.setVisibility(View.VISIBLE);
            mVinEdit.setVisibility(View.VISIBLE);
            mVinEdit.clearFocus();
            mVinEdit.setText("");
            mVinInfo.setVisibility(View.VISIBLE);

            mPhoneEdit.setVisibility(View.VISIBLE);
            mPhoneEdit.clearFocus();
            mPhoneInfo.setVisibility(View.VISIBLE);
            mPhoneText.setVisibility(View.VISIBLE);

            txtView11.setVisibility(View.VISIBLE);
            mAutoExpert.setVisibility(View.VISIBLE);
            expertHelp.setVisibility(View.VISIBLE);

            if (mPrefix.getVisibility() == View.VISIBLE) {
                mPrefix.setVisibility(View.GONE);
                mPhoneEdit.setText("");
                mPhoneEdit.setHint(getString(R.string.hint_phone));
            } else {
                mPhoneEdit.setText("");
            }
            mDone.setVisibility(View.GONE);
            mCancel.setVisibility(View.GONE);
            mToolbar.setNavigationIcon(R.drawable.ic_menu);
        }, 2000);
    }

    private void showWarningDialog(String key, int count, OnDialogSkipListener onDialogSkipListener) {
        Bundle bundle = new Bundle();
        WarningDialogFragment warningDialogFragment = new WarningDialogFragment();
        bundle.putInt(Purchase.KEY_COUNT, count);
        bundle.putString(Purchase.KEY_KEY, key);
        warningDialogFragment.setArguments(bundle);
        warningDialogFragment.setOnDialogSkipListener(onDialogSkipListener);
        warningDialogFragment.show(getSupportFragmentManager(), "warning_dialog");
    }


    /*private void checkCountForVin(int count) {
        if (count != -1) {
            if (count <= 2) {
                showWarningDialog(getString(R.string.main_key), count, new OnDialogSkipListener() {
                    @Override
                    public void onSelectedSkip() {
                        if (count != 0) {
                            intent = new Intent(MainScreenActivity.this, VinCheckActivity.class);
                            intent.putExtra(getString(R.string.vin_data_tag), mVinEdit.getText().toString());

                        }
                    }

                    @Override
                    public void onPayPro() {
                        intent = new Intent(MainScreenActivity.this, VinCheckActivity.class);
                        intent.putExtra(getString(R.string.vin_data_tag), mVinEdit.getText().toString());
                    }
                });
            } else {
                if (count != 0) {
                    intent = new Intent(MainScreenActivity.this, VinCheckActivity.class);
                    intent.putExtra(getString(R.string.vin_data_tag), mVinEdit.getText().toString());
                }
            }
        } else {
            intent = new Intent(MainScreenActivity.this, VinCheckActivity.class);
            intent.putExtra(getString(R.string.vin_data_tag), mVinEdit.getText().toString());

        }
        startActivity(intent);
        clear();
    }

    private void checkCountForPhone(int count) {
        if (count != -1) {
            if (count <= 2) {
                showWarningDialog(getString(R.string.main_key), count, new OnDialogSkipListener() {
                    @Override
                    public void onSelectedSkip() {
                        if (count != 0) {
                            Intent intent = new Intent(MainScreenActivity.this, PhoneCheckActivity.class);
                            intent.putExtra(getString(R.string.phone_tag), "7" + mPhoneEdit.getText().toString());
                            startActivity(intent);
                            clear();
                        }
                    }

                    @Override
                    public void onPayPro() {
                        Intent intent = new Intent(MainScreenActivity.this, PhoneCheckActivity.class);
                        intent.putExtra(getString(R.string.phone_tag), "7" + mPhoneEdit.getText().toString());
                        startActivity(intent);
                        cancel();
                    }
                });
            } else {
                if (count != 0) {
                    Intent intent = new Intent(MainScreenActivity.this, PhoneCheckActivity.class);
                    intent.putExtra(getString(R.string.phone_tag), "7" + mPhoneEdit.getText().toString());
                    startActivity(intent);
                    clear();
                }
            }
        } else {
            Intent intent = new Intent(MainScreenActivity.this, PhoneCheckActivity.class);
            intent.putExtra(getString(R.string.phone_tag), "7" + mPhoneEdit.getText().toString());
            startActivity(intent);
            clear();
        }
    }*/

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.abc_action_bar_home_description,
                R.string.abc_action_bar_home_description);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mToolbar.setNavigationOnClickListener(v -> {
            hideSoftKeyboard();
            if (!mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        mDone.setOnClickListener(e -> {
            //TODO Сделать оплату
            if (mVinEdit.hasFocus() || mVinEdit.getVisibility() == View.VISIBLE) {
                int length = mVinEdit.getText().length();
                if (length != 12 && length != 17) {
                    mVinEdit.setError(getString(R.string.fail_vin));
                } else {
                    intent = new Intent(MainScreenActivity.this, VinCheckActivity.class);
                    intent.putExtra(getString(R.string.vin_data_tag), mVinEdit.getText().toString());
                }
            } else if (mPhoneEdit.hasFocus() || mPhoneEdit.getVisibility() == View.VISIBLE) {
                int length = mPhoneEdit.getText().length();
                if (length != 10) {
                    mPhoneEdit.setError(getString(R.string.fail_phone));
                } else {
                    intent = new Intent(MainScreenActivity.this, PhoneCheckActivity.class);
                    intent.putExtra(getString(R.string.phone_tag), "7" + mPhoneEdit.getText().toString());
                }
            }
            startActivity(intent);
            clear();
            hideSoftKeyboard();
        });
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        mNavItemId = menuItem.getItemId();
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(() -> {
//                navigate(menuItem.getItemId());
        }, DRAWER_CLOSE_DELAY_MS);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideSoftKeyboard();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVinEdit.clearFocus();
        mPhoneEdit.clearFocus();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) throws IndexOutOfBoundsException {
        if (hasFocus) {
            mDone.setVisibility(View.VISIBLE);
            mCancel.setVisibility(View.VISIBLE);
            mToolbar.setNavigationIcon(null);
            if (v.getId() == R.id.vin_editText_mainScreenActivity) {
                mVinText.setVisibility(View.INVISIBLE);
                mVinInfo.setVisibility(View.INVISIBLE);

                mPhoneEdit.setVisibility(View.INVISIBLE);
                mPhoneInfo.setVisibility(View.INVISIBLE);
                mPhoneText.setVisibility(View.INVISIBLE);

                txtView11.setVisibility(View.INVISIBLE);
                mAutoExpert.setVisibility(View.INVISIBLE);
                expertHelp.setVisibility(View.INVISIBLE);

            }

            if (v instanceof MaskedEditText) {
                mVinText.setVisibility(View.INVISIBLE);
                mVinEdit.setVisibility(View.INVISIBLE);
                mVinInfo.setVisibility(View.INVISIBLE);

                mPhoneInfo.setVisibility(View.INVISIBLE);
                mPhoneText.setVisibility(View.INVISIBLE);

                mAutoExpert.setVisibility(View.INVISIBLE);
                txtView11.setVisibility(View.INVISIBLE);
                expertHelp.setVisibility(View.INVISIBLE);

                mPrefix.setVisibility(View.VISIBLE);
                mPhoneEdit.setHint("");
//                ((MaskedEditText) v).setText("+7");
//                if (((MaskedEditText) v).length() >=2) {
//                    ((MaskedEditText) v).post(() -> ((MaskedEditText) v).setSelection(2));
//                }
////                ((MaskedEditText) v).setSelection(((MaskedEditText) v).length());
            }
        } /*else {
            if (v.getId() == R.id.vin_editText_mainScreenActivity){
                mVinText.setVisibility(View.VISIBLE);
                mPhoneEdit.setVisibility(View.VISIBLE);
                mVinInfo.setVisibility(View.VISIBLE);
                mAutoExpert.setVisibility(View.VISIBLE);
                mPhoneInfo.setVisibility(View.VISIBLE);
                mPhoneText.setVisibility(View.VISIBLE);
                txtView11.setVisibility(View.VISIBLE);
                expertHelp.setVisibility(View.VISIBLE);
            }
            if (v instanceof MaskedEditText) {
                mVinText.setVisibility(View.VISIBLE);
                mVinEdit.setVisibility(View.VISIBLE);
                mVinInfo.setVisibility(View.VISIBLE);
                mAutoExpert.setVisibility(View.VISIBLE);
                mPhoneInfo.setVisibility(View.VISIBLE);
                mPhoneText.setVisibility(View.VISIBLE);
                txtView11.setVisibility(View.VISIBLE);
                expertHelp.setVisibility(View.VISIBLE);

                ((MaskedEditText) v).setText("");
                ((MaskedEditText) v).setHint(getString(R.string.hint_phone));
            } else {
                ((EditText) v).setText("");
            }
//            hideSoftKeyboard();
            mPrefix.setVisibility(View.GONE);
            mDone.setVisibility(View.GONE);
            mCancel.setVisibility(View.GONE);
            mToolbar.setNavigationIcon(R.drawable.ic_menu);
        }*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myChecks_button_navigationView:
                startActivity(new Intent(MainScreenActivity.this, MyCheckActivity.class));
                break;
        }
    }

    private void playShowcase(ViewTarget target) {
//        Target target = new ViewTarget(v);
        new ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(target)
                .setContentText(getString(R.string.phone_showcase_hint))
//                .setOnClickListener(e -> Toast.makeText(MainScreenActivity.this, "msg", Toast.LENGTH_SHORT).show())
                .hideOnTouchOutside()
                .setStyle(R.style.AppTheme_AppBarOverlay)
                .build();
    }

    @OnClick(R.id.phoneInfo_imageButton_mainScreenActivity)
    public void showPhoneInfo() {
        Utils.showDialog(getString(R.string.phone_info), this);
    }

    @OnClick(R.id.cancel_button_toolbar)
    public void cancel() {
        mVinText.setVisibility(View.VISIBLE);
        mVinEdit.setVisibility(View.VISIBLE);
        mVinEdit.clearFocus();
        mVinEdit.setText("");
        mVinInfo.setVisibility(View.VISIBLE);

        mPhoneEdit.setVisibility(View.VISIBLE);
        mPhoneEdit.clearFocus();
        mPhoneInfo.setVisibility(View.VISIBLE);
        mPhoneText.setVisibility(View.VISIBLE);

        txtView11.setVisibility(View.VISIBLE);
        mAutoExpert.setVisibility(View.VISIBLE);
        expertHelp.setVisibility(View.VISIBLE);

        if (mPrefix.getVisibility() == View.VISIBLE) {
            mPrefix.setVisibility(View.GONE);
            mPhoneEdit.setText("");
            mPhoneEdit.setHint(getString(R.string.hint_phone));
        } else {
            mPhoneEdit.setText("");
        }
        mDone.setVisibility(View.GONE);
        mCancel.setVisibility(View.GONE);
        mToolbar.setNavigationIcon(R.drawable.ic_menu);
    }

    @OnClick(R.id.vinInfo_imageButton_mainScreenActivity)
    public void showVinInfo() {
        Utils.showDialog(getString(R.string.vin_info), this);
    }

    @OnClick(R.id.expertHelp_Button_activityMainScreen)
    public void openPodborActivity() {
        startActivity(new Intent(this, RosPodborActivity.class));
    }

    @OnClick(R.id.vk_imageButton_navigationView)
    public void openVk() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/club114943562"));
        startActivity(intent);
    }

    @OnClick(R.id.pro_button_navigationView)
    public void openProDialog() {
        //TODO Доделать
        Imei imei = new Imei();
        imei.setTries(1);
        String key = imei.getTries() == 0
                ? getString(R.string.navigation_key_buy)
                : getString(R.string.navigation_key);
        showWarningDialog(key, 0, null);
    }

    @OnClick(R.id.share_button_navigationView)
    public void share() {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "АвтоЭксперт - бесплатное приложение, для проверки vin номера авто, номера телефона продавца и полной диагностики авто.\n" +
                "Скачай в App Store https://goo.gl/xOcIEW\n" +
                "Google Play https://goo.gl/dvQqse\n" +
                "Группа в ВК https://vk.com/club114943562");
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_with)));
    }

    @OnClick(R.id.mail_imageButton_navigationView)
    public void sendMail() {
        Utils.sendMail(this);
    }


    @Override
    public void onConnectionTimeout() {
        try {
            hideDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        makeInfoDialogWithoutHeader(getString(R.string.service_down));
    }

    public void makeInfoDialogWithoutHeader(String message) {
        if (!(this.isFinishing())) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater li = LayoutInflater.from(this);
            final View myView = li.inflate(R.layout.dialog_info, null);
            Button button = (Button) myView.findViewById(R.id.doneButton);
            TextView tv = (TextView) myView.findViewById(R.id.info_textView_infoDialog);
            View header = myView.findViewById(R.id.headerColor);
            header.setVisibility(View.GONE);
            tv.setMovementMethod(LinkMovementMethod.getInstance());
            tv.setText(message);
            builder.setView(myView);
            AlertDialog dialog = builder.create();
            button.setOnClickListener(e -> dialog.dismiss());
            dialog.show();
        }
    }

    void showDialog(String message, Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null)
            mProgressDialog.hide();
        if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog.cancel();
        }
    }
}
