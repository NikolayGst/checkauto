package ru.balodyarecordz.autoexpert.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pinball83.maskededittext.MaskedEditText;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.net.SocketTimeoutException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.balodyarecordz.autoexpert.CheckAutoApp;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.network.API;
import ru.balodyarecordz.autoexpert.view.ScrollViewWithListener;

public class RosPodborActivity extends Env implements ScrollViewWithListener.ScrollViewListener, API.OnConnectionTimeoutListener {
    //
    @Bind(R.id.vk_imageButton_rospodborActivity)
    ImageButton mVk;
    @Bind(R.id.fb_imageButton_rospodborActivity)
    ImageButton mFb;
    @Bind(R.id.youtube_imageButton_rospodborActivity)
    ImageButton mYoutube;
    @Bind(R.id.ok_imageButton_rospodborActivity)
    ImageButton mOk;
    @Bind(R.id.call_layout_rosPodbor)
    View mCall;
    @Bind(R.id.scrollView)
    ScrollViewWithListener mScrollView;
    @Bind(R.id.www_textView_rospodborActivity)
    TextView mWww;

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ros_podbor);
        ButterKnife.bind(this);
        setToolbarExpert();
        mVk.setOnClickListener(e -> startUrl(getString(R.string.vk_podbor_url)));
        mFb.setOnClickListener(e -> startUrl(getString(R.string.fb_podbor_url)));
        mYoutube.setOnClickListener(e -> startUrl(getString(R.string.youtube_podbor_url)));
        mOk.setOnClickListener(e -> startUrl(getString(R.string.ok_podbor_url)));
        mScrollView.setScrollViewListener(this);
        mWww.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        mCall.setTranslationY(250);
//        mCall.animate().translationY(250).setInterpolator(new AccelerateInterpolator());
        showRateDialog();
    }

    private void startUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick({R.id.call_Button_posPodbor, R.id.call_imageView_posPodbor, R.id.call_layout_rosPodbor})
    public void openDialog() {
        makeDialog();
    }

    private void makeDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        View snackView = getLayoutInflater().inflate(R.layout.dialog_rospodbor, null);
        EditText name = (EditText) snackView.findViewById(R.id.name_editText_rosPodbor);
        MaskedEditText phone = (MaskedEditText) snackView.findViewById(R.id.phone_editText_rosPodbor);
        Button send = (Button) snackView.findViewById(R.id.send_Button_rosPodbor);
        ImageView close = (ImageView) snackView.findViewById(R.id.red_cross);
        builder.setView(snackView);
        AlertDialog dialog = builder.create();
        send.setOnClickListener(e -> {
            if (name.getText().length() == 0) {
                name.setError(getString(R.string.field_cant_null));
                return;
            }
            if (phone.getText().length() == 0) {
                phone.setError(getString(R.string.field_cant_null));
                return;
            }
            if (phone.getText().length() < 10) {
                phone.setError(getString(R.string.fail_phone));
                return;
            }
            makeWarningDialog(name.getText().toString(),phone.getText().toString(),dialog);
          //  hideSoftKeyboard();
          /*  API.IServerApi serverApi = API.getRetrofit(getString(R.string.api_url), this).create(API.IServerApi.class);
            String phoneText = phone.getText().toString();
            serverApi.sendTradeRequest(name.getText().toString(), phoneText).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    hideDialog();
                    Toast.makeText(RosPodborActivity.this, R.string.success_request, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    hideDialog();
                    if (t instanceof SocketTimeoutException) {
                        makeInfoDialog(getString(R.string.service_down), false);
                    }
                }
            });*/
//            Intent i = new Intent(Intent.ACTION_SENDTO);
//            i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.mail_subject));
//            i.putExtra(Intent.EXTRA_TEXT, String.format(getString(R.string.rospodbor_mail_format), name.getText().toString(), phone.getText().toString()));
//            i.setData(Uri.parse("mailto:" + getString(R.string.rospodbor_mail)));
//            try {
//                startActivity(Intent.createChooser(i, getString(R.string.choose_mail_client)));
//            } catch (android.content.ActivityNotFoundException ex) {
//                Toast.makeText(RosPodborActivity.this, getString(R.string.mail_client_not_installed), Toast.LENGTH_SHORT).show();
//            }
        });
        dialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
        close.setOnClickListener(e -> dialog.dismiss());
    }

    private void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void makeWarningDialog(String name, String phone, AlertDialog alertDialog) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_podbor, null);
        TextView info = (TextView) view.findViewById(R.id.info);
        TextView text = (TextView) view.findViewById(R.id.text);
        info.setText(R.string.podbor_warning);
        text.setText("Имя: " + name + ", Телефон: " + phone);
        Button yes = (Button) view.findViewById(R.id.yesButton);
        Button no = (Button) view.findViewById(R.id.noButton);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        yes.setOnClickListener((v) -> {
            showDialog(getString(R.string.loading_data), this);
            API.IServerApi serverApi = API.getRetrofit(getString(R.string.api_url), this).create(API.IServerApi.class);
            serverApi.sendTradeRequest(name, phone).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dialog.dismiss();
                    alertDialog.dismiss();
                    hideDialog();
                    Toast.makeText(RosPodborActivity.this, R.string.success_request, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    hideDialog();
                    dialog.dismiss();
                    alertDialog.dismiss();
                    if (t instanceof SocketTimeoutException) {
                        makeInfoDialog(getString(R.string.service_down), false);
                    }
                }
            });
        });
        dialog.show();
        no.setOnClickListener(e -> dialog.dismiss());
    }
    @Override
    public void onScrollChanged(ScrollViewWithListener scrollView, int x, int y, int oldx, int oldy) {

    }

    @Override
    public void onEndScroll() {
//        mCall.setVisibility(View.VISIBLE);
//        mCall.animate().translationY(0).setInterpolator(new AccelerateInterpolator());
    }

    @Override
    public void onScroll() {
        mCall.setVisibility(View.VISIBLE);
        mCall.animate().translationY(0).setInterpolator(new AccelerateInterpolator());
    }

    @OnClick(R.id.callPhone_imageView_posPodbor)
    public void callClick() {
        requireCall();

    }

    private void call() {
        Tracker t = ((CheckAutoApp) getApplication()).getDefaultTracker();
        t.send(new HitBuilders.EventBuilder()
                .setCategory("РосПодбор")
                .setAction("Нажатие на звонок")
                .build());
        Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
        phoneIntent.setData(Uri.parse(getString(R.string.tel_podbor)));
        startActivity(phoneIntent);
    }

    private void requireCall() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},
                    REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            call();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call();
                } else {
                    makeSnackBar(R.string.permission_denied_call);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @OnClick(R.id.www_textView_rospodborActivity)
    public void openRosPodbor() {
        openUrl(getString(R.string.rospodbor_url));
    }

    @Override
    public void onConnectionTimeout() {
        try {
            hideDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        makeInfoDialogWithoutHeader(getString(R.string.service_down));
    }
}
