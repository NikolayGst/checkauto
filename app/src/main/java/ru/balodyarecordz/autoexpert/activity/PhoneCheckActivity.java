package ru.balodyarecordz.autoexpert.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.balodyarecordz.autoexpert.model.deprecated.Datum;
import ru.balodyarecordz.autoexpert.network.API;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.RecyclerItemClickListener;
import ru.balodyarecordz.autoexpert.adapter.AutoInfoAdapter;
import ru.balodyarecordz.autoexpert.db.PhoneDBDataSource;
import ru.balodyarecordz.autoexpert.model.deprecated.PerekupData;
import ru.balodyarecordz.autoexpert.utils.GsonUtils;
import ru.balodyarecordz.autoexpert.utils.Utils;

public class PhoneCheckActivity extends Env implements API.OnConnectionTimeoutListener{

    @Bind(R.id.cars_recyclerView_phoneCheckActivity)
    RecyclerView mRecyclerView;
    @Bind(R.id.webView_phoneCheckActivity)
    WebView mWebView;
    @Bind(R.id.progressBar_phoneCheckActivity)
    ProgressBar mProgressBar;
    @Bind(R.id.noData_textView_activityPhoneCheck)
    TextView mNoData;
    private PerekupData mPerekup;
    private PhoneDBDataSource mDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_check);
        ButterKnife.bind(this);
        setToolbarTitle(getString(R.string.phone_title));
        initDataFromIntent();
        if (!Utils.isPhoneMessageShowed(this)) {
            makeInfoDialogUrlWithEmail(getString(R.string.phone_correct_message), getString(R.string.phone_message), "https://perekupclub.ru");
            Utils.setPhoneMessageShowed(this);
        }
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                openUrl(mPerekup.getBody().getData().get(position).getSourceUrl());
//                mWebView.loadUrl();
            }

            @Override
            public void onItemLongClick(View view, int position) {
            }
        }));
    }

    private void initDataFromIntent() {
        String phone = getIntent().getStringExtra(getString(R.string.phone_tag));
        String data = getIntent().getStringExtra(getString(R.string.phone_data_tag));
        if (data != null) {
            PerekupData perekupData = GsonUtils.fromJson(data, PerekupData.class);
            initRecyclerView(perekupData);
        } else {
            if (isConnected())
            loadData(phone);
        }
    }

    private void loadData(final String phone) {
        showDialog(R.string.loading_data, this);
        Utils.getGeneralCount(this);
        API.IPerekup iPerekup = API.getRetrofit(getString(R.string.perekup_url), this).create(API.IPerekup.class);
        iPerekup.getPhoneInfo(getString(R.string.perekup_token), phone).enqueue(new Callback<PerekupData>() {
            @Override
            public void onResponse(Call<PerekupData> call, Response<PerekupData> response) {
                Utils.incSuccess(PhoneCheckActivity.this);
                showRateDialog();
                mDataSource = new PhoneDBDataSource(PhoneCheckActivity.this);
                mDataSource.open();
                mDataSource.addPhone(phone, GsonUtils.toJson(response.body()));
                mDataSource.close();
                initRecyclerView(response.body());
                hideDialog();
            }

            @Override
            public void onFailure(Call<PerekupData> call, Throwable t) {
                hideDialog();
                mProgressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private void initRecyclerView(PerekupData perekupData) {
        mProgressBar.setVisibility(View.GONE);
        if (perekupData.getBody().getData().size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mPerekup = perekupData;
            ArrayList<Datum> arrayList = (ArrayList) perekupData.getBody().getData();
            Collections.reverse(arrayList);
            RecyclerView.LayoutManager manager = new LinearLayoutManager(PhoneCheckActivity.this);
            mRecyclerView.setLayoutManager(manager);
            mRecyclerView.setAdapter(new AutoInfoAdapter(arrayList));
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onConnectionTimeout() {
        try {
            hideDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        makeInfoDialogWithoutHeader(getString(R.string.service_down));
    }

}
