package ru.balodyarecordz.autoexpert.activity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory.GibddHistory;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory.OwnershipPeriod;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory.Vehicle;
import ru.balodyarecordz.autoexpert.utils.FormatType;

public class HistoryGibddActivity extends Env {

    @Bind(R.id.txtModel)
    TextView txtModel;
    @Bind(R.id.txtYear)
    TextView txtYear;
    @Bind(R.id.txtVin)
    TextView txtVin;
    @Bind(R.id.txtKuzov)
    TextView txtKuzov;
    @Bind(R.id.txtColor)
    TextView txtColor;
    @Bind(R.id.txtVolume)
    TextView txtVolume;
    @Bind(R.id.txtPower)
    TextView txtPower;
    @Bind(R.id.txtTypeCar)
    TextView txtTypeCar;
    @Bind(R.id.txtHistory)
    TextView txtHistory;

    private String from;
    private String to;
    private String message;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_gibdd);
        setToolbarTitle(getString(R.string.history_car_gibdd));
        ButterKnife.bind(this);
        showData();
    }

    private void showData() {
        GibddHistory gibddHistory = (GibddHistory) getIntent().getSerializableExtra("gibdd");
        if (gibddHistory != null) {
            Vehicle vehicle = gibddHistory.getRequestResult().getVehicle();
            txtModel.setText(vehicle.getModel());
            txtYear.setText(String.format("%s г.", vehicle.getYear()));
            txtVin.setText(vehicle.getVin());
            txtKuzov.setText(vehicle.getBodyNumber());
            txtColor.setText(vehicle.getColor());
            txtVolume.setText(vehicle.getEngineVolume());
            txtPower.setText(String.format("%s/%s", vehicle.getPowerKwt(), vehicle.getPowerHp()));
            txtTypeCar.setText(FormatType.formatCar(Integer.parseInt(vehicle.getType())));
            List<OwnershipPeriod> ownershipPeriods = gibddHistory.getRequestResult()
                    .getOwnershipPeriods().getOwnershipPeriod();
            message = "";
            for (OwnershipPeriod ownershipPeriod : ownershipPeriods) {
                String formatOwner;
                String personType = ownershipPeriod.getSimplePersonType();
                String person = personType.equals("Natural") ? "Физическое лицо" : "Юридическое лицо";
                from = ownershipPeriod.getFrom();
                to = ownershipPeriod.getTo();
                if (to != null) {
                    formatOwner = getString(R.string.gibdd_owner_from_to_format);
                    message += String.format(formatOwner, from, to, person);
                } else {
                    formatOwner = getString(R.string.gibdd_owner_from_format);
                    message += String.format(formatOwner, from, person);
                }
                message += "\n";
            }
            txtHistory.setText(message);
        }
    }
}
