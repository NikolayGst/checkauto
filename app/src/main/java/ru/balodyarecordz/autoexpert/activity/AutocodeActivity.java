package ru.balodyarecordz.autoexpert.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.adapter.CommercialAdapter;
import ru.balodyarecordz.autoexpert.adapter.RestrictionsAdapter;
import ru.balodyarecordz.autoexpert.model.autocode.Autocode;
import ru.balodyarecordz.autoexpert.model.autocode.AutocodeCaptcha;
import ru.balodyarecordz.autoexpert.model.autocode.Restriction;
import ru.balodyarecordz.autoexpert.model.autocode.Values;
import ru.balodyarecordz.autoexpert.network.API;
import ru.balodyarecordz.autoexpert.utils.GsonUtils;
import ru.balodyarecordz.autoexpert.utils.Utils;
import ru.balodyarecordz.autoexpert.view.AutocodeView;

public class AutocodeActivity extends Env implements API.OnConnectionTimeoutListener {

    @Bind(R.id.owners_autocodeView)
    AutocodeView mOwners;
    @Bind(R.id.dtp_autocodeView)
    AutocodeView mDtp;
    @Bind(R.id.mileage_autocodeView)
    AutocodeView mMileage;
    @Bind(R.id.insurance_autocodeView)
    AutocodeView mInsurance;
    @Bind(R.id.tech_autocodeView)
    AutocodeView mTech;
    @Bind(R.id.commercial_recyclerView_autocodeActivity)
    RecyclerView mCommercial;
    @Bind(R.id.restriction_recyclerView_autocodeActivity)
    RecyclerView mRestriction;
    @Bind(R.id.ogr_textView_autocodeActivity)
    TextView mOgr;
    @Bind(R.id.comm_textView_autocodeActivity)
    TextView mComm;
    @Bind(R.id.data_layout_autocodeActivity)
    View mData;
    @Bind(R.id.noResults_textView_autocodeActivity)
    TextView mNoResults;

    private String mCaptcha;
    private String mVin;
    private String mSts;
    private AutocodeCaptcha mAutocodeCaptcha;
    private API.IServerApi mServerApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_autocode);
        setAutocode();
        ButterKnife.bind(this);
        setToolbarTitle(getString(R.string.autocode_title));
        mVin = getIntent().getExtras().getString(getString(R.string.vin_data_tag));
        mSts = getIntent().getExtras().getString(getString(R.string.sts_data_tag));
        String autocode = getIntent().getExtras().getString(ru.balodyarecordz.autoexpert.Values.AUTOCODE_TAG);
        if (autocode != null) {
            try {
                showData(GsonUtils.fromJson(autocode, Autocode.class));
            } catch (Exception e) {
                mAutocodeCaptcha = GsonUtils.fromJson(autocode, AutocodeCaptcha.class);
                if (mAutocodeCaptcha == null) {
                    showNotFound();
                    return;
                }
                if (mAutocodeCaptcha.getCode() == 201) {
                    showNotFound();
                    return;
                } else {
                    showCaptchaDialog(mAutocodeCaptcha.getCaptchaImage());
                }
            }
        } else {
            if (isConnected())
                getAutocodeData(mVin, mSts);
        }
    }


    private void getAutocodeData(String vin, String sts) {
        Utils.getGeneralCount(this);
        mServerApi = API.getRetrofit(getString(R.string.api_url), this).create(API.IServerApi.class);
        showDialog(getString(R.string.loading_data), this);
        mServerApi.getAutocodeWithAutorizationResp(Utils.getAutocodeLogin(this), Utils.getAutocodePass(this), vin, sts).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String s = API.bodyToString(response);
                try {
                    Autocode autocode = (GsonUtils.fromJson(s, Autocode.class));
                    showData(autocode);
                    Utils.incSuccess(AutocodeActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                    mAutocodeCaptcha = (GsonUtils.fromJson(s, AutocodeCaptcha.class));
                    if (mAutocodeCaptcha.getCode() == 201) {
                        showNotFound();
                        return;
                    } else {
                        showCaptchaDialog(mAutocodeCaptcha.getCaptchaImage());
                    }
                    return;
                }
                hideDialog();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideDialog();
                if (t instanceof SocketTimeoutException) {
                    makeInfoDialogWithoutHeader(getString(R.string.service_down));
                }
            }
        });
//        mServerApi.getAutocodeWithAutorization(Utils.getAutocodeLogin(this), Utils.getAutocodePass(this), vin, sts).enqueue(new Callback<Autocode>() {
//            @Override
//            public void onResponse(Call<Autocode> call, Response<Autocode> response) {
//                Utils.incSuccess(AutocodeActivity.this);
//                showData(response.body());
//                hideDialog();
//            }
//
//            @Override
//            public void onFailure(Call<Autocode> call, Throwable t) {
//                hideDialog();
//                if (t instanceof SocketTimeoutException) {
//                    makeInfoDialog(getString(R.string.service_down), false);
//                }
//            }
//        });
    }

    private void getAutocodeWithCaptcha(AutocodeCaptcha autocodeCaptcha) {
        mServerApi.getAutocodeWithAutorizationRespCaptcha(autocodeCaptcha.getVin(),
                autocodeCaptcha.getSts(),
                autocodeCaptcha.getSes(),
                autocodeCaptcha.getRkey(),
                mCaptcha).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideDialog();
                String s = API.bodyToString(response);
                try {
                    Autocode autocode = (GsonUtils.fromJson(s, Autocode.class));
                    showData(autocode);
                    Utils.incSuccess(AutocodeActivity.this);
                } catch (Exception e) {
                    mAutocodeCaptcha = GsonUtils.fromJson(s, AutocodeCaptcha.class);
                    if (mAutocodeCaptcha == null) {
                        showNotFound();
                        return;
                    }
                    if (mAutocodeCaptcha.getCode() == 201) {
                        showNotFound();
                        return;
                    } else {
                        showCaptchaDialog(mAutocodeCaptcha.getCaptchaImage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideDialog();
            }
        });
    }

    private void showCaptchaDialog(Bitmap bmp) {
        if (bmp == null) {
            makeInfoDialogWithoutHeader(getString(R.string.service_down));
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_captcha, null);
        ImageView imageView = (ImageView) myView.findViewById(R.id.captchaImage_imageView);
        imageView.setImageBitmap(bmp);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        EditText captchaText = (EditText) myView.findViewById(R.id.captchaText_editText);
        captchaText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Button button2 = (Button) myView.findViewById(R.id.cancelButton);
        button2.setOnClickListener(e -> dialog.dismiss());
        button.setOnClickListener(e -> {
            mCaptcha = captchaText.getText().toString();
            if (captchaText.getText().length() == 0) {
                captchaText.setError(getString(R.string.field_cant_null));
                return;
            }
            getAutocodeWithCaptcha(mAutocodeCaptcha);
            dialog.dismiss();
        });
        if (!this.isFinishing()) {
            dialog.show();
        }
    }

    private void showNoResults() {
        mData.setVisibility(View.GONE);
        mNoResults.setVisibility(View.VISIBLE);
        mNoResults.setText(getString(R.string.no_results));
        return;
    }

    private void showNotFound() {
        mData.setVisibility(View.GONE);
        mNoResults.setVisibility(View.VISIBLE);
        mNoResults.setText(getString(R.string.auto_not_found));
        return;
    }

    private void showData(Autocode autocode) {
        mOgr.setVisibility(View.VISIBLE);
        mComm.setVisibility(View.VISIBLE);
        if (autocode == null) {
            showNoResults();
        }
        if (autocode.getValues() instanceof Values) {
            Values values = (Values) autocode.getValues();
            mData.setVisibility(View.VISIBLE);
            mNoResults.setVisibility(View.GONE);
            mOwners.setData(getString(R.string.owner_count), getString(R.string.count), values.getData().getVladHist().getVladHistTable().size());
            mOwners.setDataOwner(getString(R.string.owner_count), values.getData().getVladHist().getVladHistTable());
            if (values.getData().getGTOPart().getGTO().size() > 0) {
                if (values.getData().gettOList().size() > 0) {
                    mTech.setDataGTO(getString(R.string.tech), values.getData().getGTOPart().getGTO(), values.getData().gettOList());
                } else {
                    mTech.setDataGTO(getString(R.string.tech), values.getData().getGTOPart().getGTO());
                }
            } else {
                if (values.getData().gettOList().size() > 0) {
                    mTech.setDataTO(getString(R.string.tech), values.getData().gettOList());
                } else {
                    mTech.setData(getString(R.string.tech), getString(R.string.count), getString(R.string.no_count));
                }
            }
            if (values.getData().getDTPPart().getDTP().size() > 0) {
                mDtp.setDataDTP(getString(R.string.dtp), values.getData().getDTPPart().getDTP());
            } else {
                mDtp.setData(getString(R.string.dtp), getString(R.string.dtp_count), getString(R.string.no_count));
            }
            if (values.getData().getInsurancePart().getInsurance().size() > 0) {
                mInsurance.setData(getString(R.string.sk), getString(R.string.sk_count), values.getData().getInsurancePart().getInsurance().size());
            } else {
                mInsurance.setData(getString(R.string.sk), getString(R.string.sk_count), getString(R.string.no_count));
            }
            if (values.getData().getMileagePart().getMileage().size() > 0) {
                mMileage.setData(getString(R.string.mileage), getString(R.string.count), values.getData().getMileagePart().getMileage().size());
            } else {
                mMileage.setData(getString(R.string.mileage), getString(R.string.count), getString(R.string.no_count));
            }
            int yes = 0;
            for (Restriction r : values.getData().getRestrictionsPart().getRestrictions()) {
                if (!r.getOgrRes().equals(getString(R.string.no))) {
                    yes++;
                }
            }
            mCommercial.setLayoutManager(new LinearLayoutManager(AutocodeActivity.this));
            mCommercial.setAdapter(new CommercialAdapter((ArrayList) values.getData().getKommercialPart().getKommercial()));
            mRestriction.setLayoutManager(new LinearLayoutManager(AutocodeActivity.this));
            mRestriction.setAdapter(new RestrictionsAdapter((ArrayList) values.getData().getRestrictionsPart().getRestrictions()));
        } else if (autocode.getValues() instanceof String) {
            mData.setVisibility(View.GONE);
            mNoResults.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionTimeout() {
        try {
            hideDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        makeInfoDialogWithoutHeader(getString(R.string.service_down));
    }
}
