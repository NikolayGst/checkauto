package ru.balodyarecordz.autoexpert.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import ru.balodyarecordz.autoexpert.CheckAutoApp;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.Values;
import ru.balodyarecordz.autoexpert.db.VinDBDataSource;
import ru.balodyarecordz.autoexpert.utils.Utils;

public class Env extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ActionBarDrawerToggle mDrawerToggle;
    private CoordinatorLayout mCoordinatorLayout;
    protected DrawerLayout mDrawerLayout;
    private ProgressDialog mProgressDialog;
    private Toolbar mToolbar;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_env);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        CheckAutoApp application = (CheckAutoApp) getApplication();
        mTracker = application.getDefaultTracker();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name = getLocalClassName();
        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void makeSnackBar(String message, View.OnClickListener onClickListener, String action) {

        Snackbar snackbar = Snackbar
                .make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG)
                .setAction(action, onClickListener);
        snackbar.show();
    }

    public void makeSnackBar(String message) {
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        Snackbar snackbar = Snackbar
                .make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    void showDialog(String message, Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null)
            mProgressDialog.hide();
        if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog.cancel();
        }
    }

    void showDialog(int resId, Context context) {
        showDialog(getString(resId), context);
    }

    public void makeSnackBar(int res) {
        makeSnackBar(getString(res));
    }

    public void makePodborDialog() {
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        final Snackbar snackbar = Snackbar.make(mCoordinatorLayout, "", Snackbar.LENGTH_INDEFINITE);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        View snackView = getLayoutInflater().inflate(R.layout.dialog_rospodbor, null);
        EditText name = (EditText) snackView.findViewById(R.id.name_editText_rosPodbor);
        EditText phone = (EditText) snackView.findViewById(R.id.phone_editText_rosPodbor);
        Button send = (Button) snackView.findViewById(R.id.send_Button_rosPodbor);
        ImageView close = (ImageView) snackView.findViewById(R.id.red_cross);
        send.setOnClickListener(e -> {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.rospodbor_mail)});
            i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.mail_subject));
            i.putExtra(Intent.EXTRA_TEXT, String.format(getString(R.string.rospodbor_mail_format), name.getText().toString(), phone.getText().toString()));
            try {
                startActivity(Intent.createChooser(i, getString(R.string.choose_mail_client)));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(Env.this, getString(R.string.mail_client_not_installed), Toast.LENGTH_SHORT).show();
            }
        });
        close.setOnClickListener(e -> snackbar.dismiss());
        layout.addView(snackView, 0);
        snackbar.show();
    }

    public void makeDialogSnackBar(int type, final Intent intent) {
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        final Snackbar snackbar = Snackbar.make(mCoordinatorLayout, "", Snackbar.LENGTH_INDEFINITE);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        View snackView = getLayoutInflater().inflate(R.layout.template_dialog, null);
        TextView textViewTop = (TextView) snackView.findViewById(R.id.textViewTop);
        TextView textViewMiddle = (TextView) snackView.findViewById(R.id.textViewMiddle);
        TextView textViewBottom = (TextView) snackView.findViewById(R.id.textViewBottom);
        Button button = (Button) snackView.findViewById(R.id.button_dialogTemplate);
        switch (type) {
            case 0:
                textViewTop.setText(getString(R.string.warning));
                textViewMiddle.setText(getString(R.string.this_order));
                textViewBottom.setText(getString(R.string.price_func));
                button.setText(getString(R.string.begin));
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                        startActivity(intent);
                    }
                });
                break;
            case 1:
                break;
            case 2:
                break;
        }
        layout.addView(snackView, 0);
        snackbar.show();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        mDrawerLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_env, null);
        setContentView(mDrawerLayout);
        FrameLayout actContent = (FrameLayout) mDrawerLayout.findViewById(R.id.frame_container);
        // add layout of BaseActivities inside framelayout.i.e. frame_container
        getLayoutInflater().inflate(layoutResID, actContent, true);

        initNavigationView();
    }

    void setToolbarTitle(String title) {
        TextView mTitle = (TextView) findViewById(R.id.title_textView_mainToolbar);
        mTitle.setText(title);
    }

    void setToolbarExpert() {
        TextView mTitle = (TextView) findViewById(R.id.title_textView_mainToolbar);
        View mAutoExpert = findViewById(R.id.autoExpert_layout_mainToolbar);
        mAutoExpert.setVisibility(View.VISIBLE);
    }

    void setAutocode() {
        findViewById(R.id.title_layout_mainToolbar).setVisibility(View.GONE);
        findViewById(R.id.autocode_layout_mainToolbar).setVisibility(View.VISIBLE);
        mToolbar.setBackgroundColor(Color.parseColor("#005e94"));
    }

    void initNavigationView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.additional,
                R.string.close);
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
//                    mDrawerLayout.openDrawer(GravityCompat.START);
//                } else {
                onBackPressed();
//                }
            }

        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void makeInfoDialog(String message, boolean isGreen) {
        if (!(this.isFinishing())) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater li = LayoutInflater.from(this);
            final View myView = li.inflate(R.layout.dialog_info, null);
            Button button = (Button) myView.findViewById(R.id.doneButton);
            TextView tv = (TextView) myView.findViewById(R.id.info_textView_infoDialog);
            View header = myView.findViewById(R.id.headerColor);
            if (isGreen) header.setBackgroundColor(getResources().getColor(R.color.green));
            else header.setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
            tv.setMovementMethod(LinkMovementMethod.getInstance());
            tv.setText(message);
            builder.setView(myView);
            AlertDialog dialog = builder.create();
            button.setOnClickListener(e -> {
                dialog.dismiss();
            });
            dialog.show();
        }
    }

    public void makeInfoDialogWithoutHeader(String message) {
        if (!(this.isFinishing())) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater li = LayoutInflater.from(this);
            final View myView = li.inflate(R.layout.dialog_info, null);
            Button button = (Button) myView.findViewById(R.id.doneButton);
            TextView tv = (TextView) myView.findViewById(R.id.info_textView_infoDialog);
            View header = myView.findViewById(R.id.headerColor);
            header.setVisibility(View.GONE);
            tv.setMovementMethod(LinkMovementMethod.getInstance());
            tv.setText(message);
            builder.setView(myView);
            AlertDialog dialog = builder.create();
            button.setOnClickListener(e -> dialog.dismiss());
            dialog.show();
        }
    }

    public void openUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean b = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!b) {
            makeInfoDialog(getString(R.string.check_internet), false);
        }
        return b;
    }

    void showRateDialog() {
        boolean b = Utils.isGoingToMarket(this);
        int success = Utils.getSuccessCount(this);
        int general = Utils.getGeneralCount(this);
        if (!Utils.isGoingToMarket(this)) {
            if (Utils.getSuccessCount(this) == 4 || (Utils.getGeneralCount(this) % 10 == 0 && Utils.getGeneralCount(this) != 0 && Utils.getGeneralCount(this) <= 40)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(true);
                LayoutInflater li = LayoutInflater.from(this);
                final View myView = li.inflate(R.layout.dialog_rate, null);
                Button button = (Button) myView.findViewById(R.id.doneButton);
                RatingBar ratingBar = (RatingBar) myView.findViewById(R.id.ratingBar);
                Utils.setupRatingBar(ratingBar, this);
                builder.setView(myView);
                AlertDialog dialog = builder.create();
                button.setOnClickListener(e -> dialog.dismiss());
                ratingBar.setOnRatingBarChangeListener((ratingBar1, v, b1) -> {
                    dialog.cancel();
                    if (v <= 3) {
                        Utils.showTechDialog(Env.this);
                    } else {
                        Utils.setGoingToMarket(Env.this);
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(getString(R.string.play_market_url) + getPackageName()));
                        startActivity(i);
                    }
                });
                dialog.show();
            }
        }
    }

    public void makeInfoDialogUrl(String message, String url) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_info_url, null);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        TextView tv = (TextView) myView.findViewById(R.id.info_textView_infoDialog);
        tv.setText(Html.fromHtml(message));
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        tv.setOnClickListener(e -> {
            openUrl(url);
            dialog.dismiss();
        });
        button.setOnClickListener(e -> {
            dialog.dismiss();
        });
        dialog.show();
    }

    public void makeInfoDialogUrlWithEmail(String messageMail, String messageUrl, String url) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_info_url_mail, null);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        TextView mail = (TextView) myView.findViewById(R.id.mail_textView_infoDialog);
        TextView urlTv = (TextView) myView.findViewById(R.id.url_textView_infoDialog);
        mail.setText(Html.fromHtml(messageMail));
        urlTv.setText(Html.fromHtml(messageUrl));
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        mail.setOnClickListener(e -> {
            Utils.showTechDialog(this);
            dialog.dismiss();
        });
        urlTv.setOnClickListener(e -> {
            openUrl(url);
            dialog.dismiss();
        });
        button.setOnClickListener(e -> {
            dialog.dismiss();
        });
        dialog.show();
    }

    void showStsDialog(String mVinCode) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_sts, null);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Button button2 = (Button) myView.findViewById(R.id.cancelButton);
        button2.setOnClickListener(e -> dialog.dismiss());
        EditText captchaText = (EditText) myView.findViewById(R.id.stsText_editText);
//        if (Utils.getAutocodeSts(this) != null) {
//            captchaText.setText(Utils.getAutocodeSts(this));
//        }
        if (getIntent().getStringExtra(Values.STS_SP_TAG) != null) {
            captchaText.setText(getIntent().getStringExtra(Values.STS_SP_TAG));
        }
//        captchaText.setText("5017492064");
        button.setOnClickListener(e -> {
            if (captchaText.getText().toString().length() < 1) {
                captchaText.setError(getString(R.string.field_cant_null));
            } else {
                Intent intent;
//                if (Utils.isAutocodeDataExist(this)) {
                VinDBDataSource vinDBDataSource = new VinDBDataSource(this);
                vinDBDataSource.open();
                vinDBDataSource.updateSts(captchaText.getText().toString());
                vinDBDataSource.close();
                intent = new Intent(this, AutocodeActivity.class);
                intent.putExtra(getString(R.string.vin_data_tag), mVinCode);
                intent.putExtra(getString(R.string.sts_data_tag), captchaText.getText().toString());
                dialog.dismiss();
                startActivity(intent);
//                } else {
//                    intent = new Intent(this, RegistrationActivity.class);
//                    intent.putExtra(ru.balodyarecordz.autoexpert.Values.VIN_TAG, mVinCode);
//                    intent.putExtra(ru.balodyarecordz.autoexpert.Values.STS_TAG, captchaText.getText().toString());
//                    dialog.dismiss();
//                    startActivity(intent);
//                }
            }
        });
        if (!this.isFinishing()) {
            dialog.show();
        }
    }
}