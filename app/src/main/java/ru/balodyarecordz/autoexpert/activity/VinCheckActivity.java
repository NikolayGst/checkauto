package ru.balodyarecordz.autoexpert.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.RecyclerItemClickListener;
import ru.balodyarecordz.autoexpert.adapter.SimpleAdapter;
import ru.balodyarecordz.autoexpert.db.VinDBDataSource;
import ru.balodyarecordz.autoexpert.model.CaptchaResponse;
import ru.balodyarecordz.autoexpert.model.autoru.Bank;
import ru.balodyarecordz.autoexpert.model.autoru.Values;
import ru.balodyarecordz.autoexpert.model.autoru.VinInfo;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddDtp.Dtp;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory.GibddHistory;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddRestritions.Restritions;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddRestritions.RestritionsRecord;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddWanted.Wanted;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddWanted.WantedRecord;
import ru.balodyarecordz.autoexpert.model.reestr.Item;
import ru.balodyarecordz.autoexpert.model.reestr.Reestr;
import ru.balodyarecordz.autoexpert.network.API;
import ru.balodyarecordz.autoexpert.utils.FormatType;
import ru.balodyarecordz.autoexpert.utils.Utils;

public class VinCheckActivity extends Env implements API.OnConnectionTimeoutListener {

    @Bind(R.id.modelList_recyclerView_vinCheck)
    RecyclerView mModels;
    @Bind(R.id.vin_progressBar_vinCheckActivity)
    View mProgressBar;
    @Bind(R.id.vinInfo_layout_vinCheckActivity)
    View mVinData;
    @Bind(R.id.vin_textView_vinCheck)
    TextView mVin;
    @Bind(R.id.mark_textView_vinCheck)
    TextView mMark;
    @Bind(R.id.model_textView_vinCheck)
    TextView mModel;
    @Bind(R.id.modify_textView_vinCheck)
    TextView mModify;
    @Bind(R.id.kuzov_textView_vinCheck)
    TextView mKuzov;
    @Bind(R.id.doorsCount_textView_vinCheck)
    TextView mDoors;
    @Bind(R.id.type_textView_vinCheck)
    TextView mType;
    @Bind(R.id.year_textView_vinCheck)
    TextView mYear;
    @Bind(R.id.factory_textView_vinCheck)
    TextView mFactory;
    @Bind(R.id.country_textView_vinCheck)
    TextView mCountry;
    @Bind(R.id.countryMain_textView_vinCheck)
    TextView mCountryMain;
    @Bind(R.id.serial_textView_vinCheck)
    TextView mSerial;
    @Bind(R.id.noData_textView_vinCheckActivity)
    TextView mNoData;
    @Bind(R.id.mark_layout_vinCheckActivity)
    View mMarkLayout;
    @Bind(R.id.model_layout_vinCheckActivity)
    View mModelLayout;
    @Bind(R.id.year_layout_vinCheckActivity)
    View mYearLayout;


    private String mCaptcha;
    private API.IServerApi mServerApi;
    private CaptchaResponse mResponse;
    private String mVinCode;
    private String mSts;
    private int mVinLength;
    private String from;
    private String to;

    private VinDBDataSource mDataSource;

    private final int TYPE_AUTOCODE = 0;
    private final int TYPE_GIBDD = 1;
    private final int TYPE_GIBDD_HISTORY = 11;
    private final int TYPE_GIBDD_DTP = 12;
    private final int TYPE_GIBDD_CAR_WANTED = 13;
    private final int TYPE_GIBDD_RESTRICTIONS = 14;
    private final int TYPE_REESTR = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vin_check);
        mVinCode = getIntent().getExtras().getString(getString(R.string.vin_data_tag));
        ButterKnife.bind(this);
        mVin.setText(mVinCode);
        mVinLength = mVin.getText().length();
//        mServerApi = API.getRetrofit(getString(R.string.api_url)).create(API.IServerApi.class);
        mServerApi = API.getRetrofit(getString(R.string.api_url), this).create(API.IServerApi.class);
        if (isConnected() && mVinLength != 12) {
            getVinData();
        }
        setToolbarTitle(getString(R.string.vin_title));
    }


    @OnClick(R.id.fssp_button_vinCheckActivity)
    public void startFssp() {
        startActivity(new Intent(VinCheckActivity.this, FsspActivity.class));
    }

    @OnClick(R.id.gibdd_history_button_vinCheckActivity)
    public void getGibddHistoryCaptcha() {
        if (isConnected()) {
            showDialog(getString(R.string.loading_data), this);
            mServerApi.getGibddCaptcha().enqueue(new Callback<CaptchaResponse>() {
                @Override
                public void onResponse(Call<CaptchaResponse> call, Response<CaptchaResponse> response) {
                    hideDialog();
                    mResponse = response.body();
                    if (response.body() != null) {
                        if (response.body().getCaptcha() != null) {
                            showCaptchaDialog(response.body().getCaptchaImage(), TYPE_GIBDD_HISTORY);
                        } else {
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                        }
                    } else {
                        makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                    }
                }

                @Override
                public void onFailure(Call<CaptchaResponse> call, Throwable t) {
                    hideDialog();
                    if (t instanceof SocketTimeoutException) {
                        makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                    }
                }
            });
        }
    }

    @OnClick(R.id.dtp_button_vinCheckActivity)
    public void getDtpCaptcha() {
        if (isConnected()) {
            showDialog(getString(R.string.loading_data), this);
            mServerApi.getGibddCaptcha().enqueue(new Callback<CaptchaResponse>() {
                @Override
                public void onResponse(Call<CaptchaResponse> call, Response<CaptchaResponse> response) {
                    hideDialog();
                    mResponse = response.body();
                    if (response.body() != null) {
                        if (response.body().getCaptcha() != null) {
                            showCaptchaDialog(response.body().getCaptchaImage(), TYPE_GIBDD_DTP);
                        } else {
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                        }
                    } else {
                        makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                    }
                }

                @Override
                public void onFailure(Call<CaptchaResponse> call, Throwable t) {
                    hideDialog();
                    if (t instanceof SocketTimeoutException) {
                        makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                    }
                }
            });
        }
    }

    @OnClick(R.id.gibdd_car_wanted_button_vinCheckActivity)
    public void getCarWantedCaptcha() {
        if (isConnected()) {
            showDialog(getString(R.string.loading_data), this);
            mServerApi.getGibddCaptcha().enqueue(new Callback<CaptchaResponse>() {
                @Override
                public void onResponse(Call<CaptchaResponse> call, Response<CaptchaResponse> response) {
                    hideDialog();
                    mResponse = response.body();
                    if (response.body() != null) {
                        if (response.body().getCaptcha() != null) {
                            showCaptchaDialog(response.body().getCaptchaImage(), TYPE_GIBDD_CAR_WANTED);
                        } else {
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                        }
                    } else {
                        makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                    }
                }

                @Override
                public void onFailure(Call<CaptchaResponse> call, Throwable t) {
                    hideDialog();
                    if (t instanceof SocketTimeoutException) {
                        makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                    }
                }
            });
        }
    }

    @OnClick(R.id.gibdd_restrictions_button_vinCheckActivity)
    public void getRestrictionCaptcha() {
        if (isConnected()) {
            showDialog(getString(R.string.loading_data), this);
            mServerApi.getGibddCaptcha().enqueue(new Callback<CaptchaResponse>() {
                @Override
                public void onResponse(Call<CaptchaResponse> call, Response<CaptchaResponse> response) {
                    hideDialog();
                    mResponse = response.body();
                    if (response.body() != null) {
                        if (response.body().getCaptcha() != null) {
                            showCaptchaDialog(response.body().getCaptchaImage(), TYPE_GIBDD_RESTRICTIONS);
                        } else {
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                        }
                    } else {
                        makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                    }
                }

                @Override
                public void onFailure(Call<CaptchaResponse> call, Throwable t) {
                    hideDialog();
                    if (t instanceof SocketTimeoutException) {
                        makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                    }
                }
            });
        }
    }


    @OnClick(R.id.notarius_button_vinCheckActivity)
    public void getReestrCaptcha() {
        if (isConnected() && mVinLength != 12) {
            showDialog(getString(R.string.loading_data), this);
            mServerApi.getReestrCaptcha(mVinCode).enqueue(new Callback<CaptchaResponse>() {
                @Override
                public void onResponse(Call<CaptchaResponse> call, Response<CaptchaResponse> response) {
                    hideDialog();
                    mResponse = response.body();
                    if (response.body().getCaptcha() != null) {
                        showCaptchaDialog(response.body().getCaptchaImage(), TYPE_REESTR);
                    } else {
                        makeInfoDialog(getString(R.string.service_down), false);
                    }
                }

                @Override
                public void onFailure(Call<CaptchaResponse> call, Throwable t) {
                    hideDialog();
                    if (t instanceof SocketTimeoutException) {
                        makeInfoDialogWithoutHeader(getString(R.string.service_down));
                    }
                }
            });
        } else {
            // Данный источник не поддерживает проверку vin-номера японского автомобиля.
            showVinDialogError();
        }
    }

    private void showVinDialogError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        View myView = li.inflate(R.layout.dialog_vin_error, null);
        Button buttonOk = (Button) myView.findViewById(R.id.okButton);
        builder.setView(myView);
        AlertDialog alertDialog = builder.create();
        buttonOk.setOnClickListener(v -> {
            if (alertDialog.isShowing()) alertDialog.dismiss();
        });
        alertDialog.show();
    }

    private void showCaptchaDialog(Bitmap bmp, int type) {
        mCaptcha = "";
        if (bmp == null) {
            makeInfoDialogWithoutHeader(getString(R.string.service_down));
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_captcha, null);
        ImageView imageView = (ImageView) myView.findViewById(R.id.captchaImage_imageView);
        imageView.setImageBitmap(bmp);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        EditText captchaText = (EditText) myView.findViewById(R.id.captchaText_editText);
        if (type == TYPE_REESTR) {
            captchaText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        } else {
            captchaText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            captchaText.setRawInputType(InputType.TYPE_CLASS_PHONE);
        }
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Button button2 = (Button) myView.findViewById(R.id.cancelButton);
        button2.setOnClickListener(e -> dialog.dismiss());
        button.setOnClickListener(e -> {
            mCaptcha = captchaText.getText().toString();
            if (captchaText.getText().length() == 0) {
                captchaText.setError(getString(R.string.field_cant_null));
                return;
            }
            switch (type) {
                case TYPE_AUTOCODE:
                    break;
                case TYPE_GIBDD_HISTORY:
                    if (isConnected())
                        getGibddHistoryData(mVinCode, mResponse.getSes(), mCaptcha);
                    break;
                case TYPE_GIBDD_DTP:
                    if (isConnected())
                        getGibddDtpData(mVinCode, mResponse.getSes(), mCaptcha);
                    break;
                case TYPE_GIBDD_CAR_WANTED:
                    if (isConnected())
                        getCarWantedData(mVinCode, mResponse.getSes(), mCaptcha);
                    break;
                case TYPE_GIBDD_RESTRICTIONS:
                    if (isConnected())
                        getGibddRestritionsData(mVinCode, mResponse.getSes(), mCaptcha);
                    break;
                case TYPE_REESTR:
                    if (isConnected())
                        getReestrData(mVinCode, mResponse.getToken(), mResponse.getSes(), mCaptcha);
                    break;
            }
            dialog.dismiss();
        });
        if (!this.isFinishing()) {
            dialog.show();
        }
    }


    private void getGibddHistoryData(String vinCode, String ses, String captcha) {
        showDialog(getString(R.string.loading_data), this);
        Utils.incGeneral(this);
        mServerApi.getGibddHistoryData(ses, vinCode, captcha).enqueue(new Callback<GibddHistory>() {
            @Override
            public void onResponse(Call<GibddHistory> call, Response<GibddHistory> response) {
                hideDialog();
                String message = "";
                if (response.body() != null) {
                    GibddHistory gibddHistory = response.body();
                    switch (gibddHistory.getStatus()) {
                        case 200:
                            Intent intent = new Intent(VinCheckActivity.this, HistoryGibddActivity.class);
                            GibddHistory gibdd = response.body();
                            intent.putExtra("gibdd", gibdd);
                            startActivity(intent);
                            break;
                        case 201:
                            makeInfoDialogWithType(getString(R.string.need_recaptcha), TYPE_GIBDD_HISTORY);
                            break;
                        case 404:
                            makeInfoDialogWithoutHeader(getString(R.string.vin_404));
                            break;
                        case 500:
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                            break;
                        case 502:
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                            break;
                    }
                } else {
                    makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                }
            }

            @Override
            public void onFailure(Call<GibddHistory> call, Throwable t) {
                hideDialog();
            //    Log.d("Gibdd", "onFailure: " + t.getLocalizedMessage());
                t.printStackTrace();
                if (t instanceof SocketTimeoutException) {
                    makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                }
            }
        });
    }

    private void getGibddDtpData(String vinCode, String ses, String captcha) {
        showDialog(getString(R.string.loading_data), this);
        Utils.incGeneral(this);
        mServerApi.getGibddDtpData(ses, vinCode, captcha).enqueue(new Callback<Dtp>() {
            @Override
            public void onResponse(Call<Dtp> call, Response<Dtp> response) {
                hideDialog();
                if (response.body() != null) {
                    Dtp dtp = response.body();
                    switch (dtp.getStatus()) {
                        case 200:
                            makeInfoDialogWithoutHeader(getString(R.string.error_dtp));
                            break;
                        case 201:
                            makeInfoDialogWithType(getString(R.string.need_recaptcha), TYPE_GIBDD_DTP);
                            break;
                        case 500:
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                            break;
                        case 502:
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                            break;
                    }
                } else {
                    makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                }
            }

            @Override
            public void onFailure(Call<Dtp> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                }
            }
        });
    }

    private void getCarWantedData(String vinCode, String ses, String captcha) {
        showDialog(getString(R.string.loading_data), this);
        Utils.incGeneral(this);
        mServerApi.getGibddCarWantedData(ses, vinCode, captcha).enqueue(new Callback<Wanted>() {
            @Override
            public void onResponse(Call<Wanted> call, Response<Wanted> response) {
                hideDialog();
                String message = "";
                if (response.body() != null) {
                    Wanted wanted = response.body();
                    switch (wanted.getStatus()) {
                        case 200:
                            if (wanted.getRequestResult().getWantedRecords().size() > 0) {
                                List<WantedRecord> wantedRecord = wanted.getRequestResult().getWantedRecords();
                                for (WantedRecord record : wantedRecord) {
                                    String format = getString(R.string.gibdd_wanted_format);
                                    message += String.format(format,
                                            record.getWModel(),
                                            record.getWGodVyp() + " г.",
                                            record.getWDataPu() + " г.",
                                            record.getWRegInic());
                                    message += "\n";
                                }
                                makeInfoDialog(message, false);
                            } else {
                                message += getString(R.string.gibdd_no_wanted);
                                makeInfoDialog(message, true);
                            }
                            break;
                        case 201:
                            makeInfoDialogWithType(getString(R.string.need_recaptcha), TYPE_GIBDD_CAR_WANTED);
                            break;
                        case 500:
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                            break;
                        case 502:
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                            break;
                    }
                } else {
                    makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                }
            }

            @Override
            public void onFailure(Call<Wanted> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                }
            }
        });
    }

    private void getGibddRestritionsData(String vinCode, String ses, String captcha) {
        showDialog(getString(R.string.loading_data), this);
        Utils.incGeneral(this);
        mServerApi.getGibddRestrictData(ses, vinCode, captcha).enqueue(new Callback<Restritions>() {
            @Override
            public void onResponse(Call<Restritions> call, Response<Restritions> response) {
                hideDialog();
                String message = "";
                if (response.body() != null) {
                    Restritions restritions = response.body();
                    switch (restritions.getStatus()) {
                        case 200:
                            if (restritions.getRequestResult().getRestritionsRecords().size() > 0) {
                                List<RestritionsRecord> restritionsRecord = restritions.getRequestResult().getRestritionsRecords();
                                for (RestritionsRecord record : restritionsRecord) {
                                    String format = getString(R.string.gibdd_restricted_format);
                                    String tsyear = record.getTsyear() != null ? record.getTsyear() + " г" : "-";
                                    message += String.format(format,
                                            record.getTsmodel(),
                                            tsyear,
                                            record.getDateogr() + " г.",
                                            record.getRegname(),
                                            record.getRegname(),
                                            FormatType.formatOrg(record.getOgrkod()));
                                    message += "\n\n";
                                }
                                makeInfoDialog(message, false);
                            } else {
                                message += getString(R.string.gibdd_no_restrictions);
                                makeInfoDialog(message, true);
                            }
                            break;
                        case 201:
                            makeInfoDialogWithType(getString(R.string.need_recaptcha), TYPE_GIBDD_RESTRICTIONS);
                            break;
                        case 500:
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                            break;
                        case 502:
                            makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                            break;
                    }
                } else {
                    makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                }
            }

            @Override
            public void onFailure(Call<Restritions> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    makeInfoDialogWithoutHeader(getString(R.string.gibdd_server_error));
                }
            }
        });
    }

    private void getReestrData(String vin, String token, String ses, String captcha) {
        showDialog(getString(R.string.loading_data), this);
        Utils.incGeneral(this);
        mServerApi.getReestrData(vin, token, ses, captcha).enqueue(new Callback<Reestr>() {
            @Override
            public void onResponse(Call<Reestr> call, Response<Reestr> response) {
                hideDialog();
                String message = "";
                if (response.body().getError() != null)
                    if (response.body().getError() != 0) {
                        makeInfoDialogWithoutHeader(getString(R.string.need_recaptcha));
                        return;
                    }
                Utils.incSuccess(VinCheckActivity.this);
                showRateDialog();
                if (response.body().getItems() != null) {
                    if (response.body().getItems().size() > 0) {
                        Item item = response.body().getItems().get(0);
                        message += String.format(getString(R.string.reestr_format), item.getPledgor(), item.getMortgagees());
                    } else {
                        message += getString(R.string.reestr_no);
                        makeInfoDialog(message, true);
                        return;
                    }
                } else {
                    message += getString(R.string.reestr_no);
                    makeInfoDialog(message, true);
                    return;
                }
                makeInfoDialog(message, false);
            }

            @Override
            public void onFailure(Call<Reestr> call, Throwable t) {
                hideDialog();
                if (t instanceof SocketTimeoutException) {
                    makeInfoDialog(getString(R.string.service_down), false);
                }
            }
        });
    }

    @OnClick(R.id.autocode_button_vinCheckActivity)
    public void getAutocode() {
        if (mVinLength != 12) {
            if (Utils.isAutocodeDataExist(this)) {
                showStsDialog(mVinCode);
            } else {
                Intent intent = new Intent(this, RegistrationActivity.class);
                intent.putExtra(ru.balodyarecordz.autoexpert.Values.VIN_TAG, mVinCode);
                startActivity(intent);
            }
//        makeInfoDialog(getString(R.string.autocode_error), false);
        } else {
            // Данный источник не поддерживает проверку vin-номера японского автомобиля.
            showVinDialogError();
        }
    }

    private void getVinModel(String model) {
        mProgressBar.setVisibility(View.VISIBLE);
        mModels.setVisibility(View.GONE);
        mServerApi.getVinData(mVinCode, model).enqueue(new Callback<VinInfo>() {
            @Override
            public void onResponse(Call<VinInfo> call, Response<VinInfo> response) {
                mProgressBar.setVisibility(View.GONE);
                setVinData(response.body().getValues().getValues());
            }

            @Override
            public void onFailure(Call<VinInfo> call, Throwable t) {
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getVinData() {
        mProgressBar.setVisibility(View.VISIBLE);
        mServerApi.getVinData(mVinCode).enqueue(new Callback<VinInfo>() {
            @Override
            public void onResponse(Call<VinInfo> call, Response<VinInfo> response) {
                mProgressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getMarks() != null) {
                        if (response.body().getMarks().size() > 0) {
                            mModels.setVisibility(View.VISIBLE);
                            mModels.setLayoutManager(new LinearLayoutManager(VinCheckActivity.this));
                            mModels.setAdapter(new SimpleAdapter((ArrayList) response.body().getMarks()));
                            mModels.addOnItemTouchListener(new RecyclerItemClickListener(VinCheckActivity.this, mModels, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    if (isConnected())
                                        getVinModel(response.body().getMarks().get(position));
                                }

                                @Override
                                public void onItemLongClick(View view, int position) {
                                }
                            }));
                        } else {
                            Values values = response.body().getValues().getValues();
                            setVinData(values);
                        }
                    } else {
                        Values values = response.body().getValues().getValues();
                        setVinData(values);
                    }
                } else {
                    mProgressBar.setVisibility(View.GONE);
                    mNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<VinInfo> call, Throwable t) {
                mProgressBar.setVisibility(View.GONE);
                mNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    @OnClick(R.id.bank_button_vinCheckActivity)
    public void getPledgeData() {
        if (isConnected() && mVinLength != 12) {
            showDialog(getString(R.string.loading_data), this);
            mServerApi.getVinData(mVinCode).enqueue(new Callback<VinInfo>() {
                @Override
                public void onResponse(Call<VinInfo> call, Response<VinInfo> response) {
                    if (response.body() != null)
                        if (response.body().getMarks() != null) {
                            if (response.body().getMarks().size() > 0) {
                                showModelsDialog(response.body().getMarks());
                                hideDialog();
                            }
                        } else {
                            if (response.body().getPledge().equals("1")) {
                                getBankInfo();
                            } else {
                                hideDialog();
                                makeInfoDialog(getString(R.string.no_bank), true);
                            }
                        }
                }

                @Override
                public void onFailure(Call<VinInfo> call, Throwable t) {
                    hideDialog();
                    makeInfoDialog(getString(R.string.no_info), true);
                }
            });
        } else {
            // Данный источник не поддерживает проверку vin-номера японского автомобиля.
            showVinDialogError();
        }
    }

    private void getBankInfo() {
        mServerApi.getBankData(mVinCode).enqueue(new Callback<Bank>() {
            @Override
            public void onResponse(Call<Bank> call, Response<Bank> response) {
                makeInfoDialog(response.body().getPledge().getZalog() + "\n" + response.body().getPledge().getValues().getBank(), false);
                hideDialog();
            }

            @Override
            public void onFailure(Call<Bank> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    makeInfoDialogWithoutHeader(getString(R.string.service_down));
                }
            }
        });
    }

    private void getPledgeInfo(String model) {
        Utils.incGeneral(this);
        showDialog(getString(R.string.loading_data), this);
        mServerApi.getVinData(mVinCode, model).enqueue(new Callback<VinInfo>() {
            @Override
            public void onResponse(Call<VinInfo> call, Response<VinInfo> response) {
                if (response.body().getPledge() != null) {
                    if (response.body().getPledge().equals("1")) {
                        Utils.incSuccess(VinCheckActivity.this);
                        showRateDialog();
                        if (isConnected())
                            getBankInfo();
                    } else {
                        hideDialog();
                        makeInfoDialog(getString(R.string.no_bank), true);
                    }
                }
            }

            @Override
            public void onFailure(Call<VinInfo> call, Throwable t) {
                hideDialog();
                if (t instanceof SocketTimeoutException) {
                    makeInfoDialog(getString(R.string.service_down), false);
                }
            }
        });
    }

    private void showModelsDialog(List<String> models) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_models, null);
        RecyclerView modelsList = (RecyclerView) myView.findViewById(R.id.models_recyclerView);
        modelsList.setLayoutManager(new LinearLayoutManager(VinCheckActivity.this));
        modelsList.setAdapter(new SimpleAdapter((ArrayList) models));
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        modelsList.addOnItemTouchListener(new RecyclerItemClickListener(VinCheckActivity.this, mModels, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (isConnected())
                    getPledgeInfo(models.get(position));
                dialog.dismiss();
            }

            @Override
            public void onItemLongClick(View view, int position) {
            }
        }));

        Button button2 = (Button) myView.findViewById(R.id.cancelButton);
        button2.setOnClickListener(e -> dialog.dismiss());
        if (!this.isFinishing()) {
            dialog.show();
        }
    }

    private void setVinData(Values values) {
        if (values != null) {
            mVinData.setVisibility(View.VISIBLE);
            mDataSource = new VinDBDataSource(VinCheckActivity.this);
            mDataSource.open();
            if (Utils.isNotEmpty(values.getMark())) {
                ru.balodyarecordz.autoexpert.Values.LAST_ID = mDataSource.addVin(mVinCode, values.getMark());
                mMark.setText(values.getMark());
            } else {
                mMarkLayout.setVisibility(View.GONE);
            }
            if (Utils.isNotEmpty(values.getModel())) {
                mModel.setText(values.getModel());
            } else {
                mModelLayout.setVisibility(View.GONE);
            }
            if (Utils.isNotEmpty(values.getYear())) {
                mYear.setText(values.getYear());
            } else {
                mYearLayout.setVisibility(View.GONE);
            }
            mModify.setText(values.getModify());
            mKuzov.setText(values.getKuzov());
            mDoors.setText(values.getDoorsCount());
            mType.setText(values.getType());
            mFactory.setText(values.getFactory());
            mCountry.setText(values.getCountry());
            mCountryMain.setText(values.getCountryMain());
            mSerial.setText(values.getSerialNumber());
            mDataSource.close();
        }
    }

    public void makeInfoDialogWithType(String message, int type) {
        if (!(this.isFinishing())) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater li = LayoutInflater.from(this);
            final View myView = li.inflate(R.layout.dialog_info, null);
            Button button = (Button) myView.findViewById(R.id.doneButton);
            TextView tv = (TextView) myView.findViewById(R.id.info_textView_infoDialog);
            View header = myView.findViewById(R.id.headerColor);
            header.setVisibility(View.GONE);
            tv.setMovementMethod(LinkMovementMethod.getInstance());
            tv.setText(message);
            builder.setView(myView);
            AlertDialog dialog = builder.create();
            button.setOnClickListener(e -> {
                switch (type) {
                    case TYPE_REESTR:
                        if (isConnected())
                            getReestrCaptcha();
                        break;
                    case TYPE_GIBDD_HISTORY:
                        if (isConnected())
                            getGibddHistoryCaptcha();
                        break;
                    case TYPE_GIBDD_DTP:
                        if (isConnected())
                            getDtpCaptcha();
                        break;
                    case TYPE_GIBDD_CAR_WANTED:
                        if (isConnected())
                            getCarWantedCaptcha();
                        break;
                    case TYPE_GIBDD_RESTRICTIONS:
                        if (isConnected())
                            getRestrictionCaptcha();
                        break;
                }
                dialog.dismiss();
            });
            dialog.show();

        }
    }

    @Override
    public void onConnectionTimeout() {
        try {
            hideDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        makeInfoDialogWithoutHeader(getString(R.string.service_down));
    }

}
