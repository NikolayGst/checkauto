package ru.balodyarecordz.autoexpert.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.RecyclerItemClickListener;
import ru.balodyarecordz.autoexpert.Values;
import ru.balodyarecordz.autoexpert.adapter.PhoneAdapter;
import ru.balodyarecordz.autoexpert.adapter.VinSwipeAdapter;
import ru.balodyarecordz.autoexpert.db.PhoneDBDataSource;
import ru.balodyarecordz.autoexpert.db.VinDBDataSource;
import ru.balodyarecordz.autoexpert.model.db.PhoneDb;
import ru.balodyarecordz.autoexpert.model.db.VinDb;
import ru.balodyarecordz.autoexpert.view.helper.OnStartDragListener;
import ru.balodyarecordz.autoexpert.view.helper.SimpleItemTouchHelperCallback;

public class MyCheckActivity extends Env implements View.OnClickListener, OnStartDragListener {

    @Bind(R.id.vin_button_myCheckActivity)
    Button mVin;
    @Bind(R.id.phone_button_myCheckActivity)
    Button mPhone;
    @Bind(R.id.phone_recyclerView_myCheckActivity)
    RecyclerView mPhoneRecyclerView;
    @Bind(R.id.vin_recyclerView_myCheckActivity)
    RecyclerView mVinRecyclerView;
    @Bind(R.id.noData_textView_activityMyCheck)
    TextView mNoData;

    private PhoneDBDataSource mPhonesDataSource;
    private VinDBDataSource mVinDataSource;
    private ItemTouchHelper mItemTouchHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_check);
        ButterKnife.bind(this);
        setToolbarTitle(getString(R.string.checks_title));
        mVin.setOnClickListener(this);
        mVin.callOnClick();
        mPhone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(MyCheckActivity.this);
        mPhoneRecyclerView.setLayoutManager(manager);
        switch (v.getId()) {
            case R.id.vin_button_myCheckActivity:
                mVin.setBackgroundResource(R.drawable.left_rounder_corner_black);
                mPhone.setBackgroundResource(R.drawable.right_rounder_corner);
                mVin.setTextColor(Color.WHITE);
                mPhone.setTextColor(Color.BLACK);
                mPhoneRecyclerView.setVisibility(View.GONE);
                mVinRecyclerView.setVisibility(View.VISIBLE);
                mNoData.setVisibility(View.VISIBLE);
                mVinDataSource = new VinDBDataSource((MyCheckActivity.this));
                mVinDataSource.open();
                final ArrayList<VinDb> vinData = mVinDataSource.getAllVins();
                Collections.reverse(vinData);
                mVinRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(MyCheckActivity.this, mPhoneRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(MyCheckActivity.this, VinCheckActivity.class);
                        intent.putExtra(getString(R.string.vin_data_tag), vinData.get(position).getVin());
                        intent.putExtra(Values.STS_SP_TAG, vinData.get(position).getSts());
                        startActivity(intent);
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {

                    }
                }));
                mVinDataSource.close();
                if (vinData.size() > 0) {
                    mVinRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                    VinSwipeAdapter adapter = new VinSwipeAdapter(vinData, this);
                    mVinRecyclerView.setAdapter(adapter);
                    mNoData.setVisibility(View.GONE);
                    mPhoneRecyclerView.setVisibility(View.GONE);
                    mVinRecyclerView.setVisibility(View.VISIBLE);
                    ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
                    mItemTouchHelper = new ItemTouchHelper(callback);
                    mItemTouchHelper.attachToRecyclerView(mVinRecyclerView);
                } else {
                    mVinRecyclerView.setVisibility(View.GONE);
                    mNoData.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.phone_button_myCheckActivity:
                mVinRecyclerView.setVisibility(View.GONE);
                mPhonesDataSource = new PhoneDBDataSource(MyCheckActivity.this);
                mPhonesDataSource.open();
                final ArrayList<PhoneDb> data = mPhonesDataSource.getAllPhones();
                Collections.reverse(data);
                mPhoneRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(MyCheckActivity.this, mPhoneRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(MyCheckActivity.this, PhoneCheckActivity.class);
                        intent.putExtra(getString(R.string.phone_tag), data.get(position).getPhone());
                        intent.putExtra(getString(R.string.phone_data_tag), data.get(position).getData());
                        startActivity(intent);
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {

                    }
                }));
                mPhonesDataSource.close();
                if (data.size() > 0) {
                    PhoneAdapter adapter = new PhoneAdapter(data, this);
                    mPhoneRecyclerView.setAdapter(adapter);
                    mNoData.setVisibility(View.GONE);
                    mVinRecyclerView.setVisibility(View.GONE);
                    mPhoneRecyclerView.setVisibility(View.VISIBLE);
                    ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
                    mItemTouchHelper = new ItemTouchHelper(callback);
                    mItemTouchHelper.attachToRecyclerView(mPhoneRecyclerView);
                } else {
                    mPhoneRecyclerView.setVisibility(View.GONE);
                    mNoData.setVisibility(View.VISIBLE);
                }
                mVin.setTextColor(Color.BLACK);
                mPhone.setTextColor(Color.WHITE);
                mVin.setBackgroundResource(R.drawable.left_rounder_corner);
                mPhone.setBackgroundResource(R.drawable.right_rounder_corner_black);
                break;
        }
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
}
