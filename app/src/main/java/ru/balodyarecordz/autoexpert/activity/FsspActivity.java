package ru.balodyarecordz.autoexpert.activity;

import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.balodyarecordz.autoexpert.network.API;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.adapter.SimpleAdapter;
import ru.balodyarecordz.autoexpert.model.CaptchaResponse;
import ru.balodyarecordz.autoexpert.model.fssp.Fssp;
import ru.balodyarecordz.autoexpert.utils.Utils;

public class FsspActivity extends Env implements API.OnConnectionTimeoutListener {

    @Bind(R.id.progressBar_fsspActivity)
    ProgressBar mProgressBar;
    //    @Bind(R.id.captchaImage_imageView_fsspActivity)
//    ImageView mCaptcha;
    @Bind(R.id.captchaText_editText_fsspActivity)
    EditText mCaptchaText;
    @Bind(R.id.name_editText_fsspActivity)
    EditText mName;
    @Bind(R.id.fam_editText_fsspActivity)
    EditText mFam;
    @Bind(R.id.patr_editText_fsspActivity)
    EditText mPatr;
    //    @Bind(R.id.done_button_fsspActivity)
//    Button mDone;
    @Bind(R.id.date_button_fsspActivity)
    TextView mDate;
    @Bind(R.id.done_button_toolbar)
    Button mDone;

    private String mDateString;
    private CaptchaResponse mResponse;
    private API.IServerApi mServerApi;
    private String mCaptcha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fssp);
        setToolbarTitle(getString(R.string.fssp_title));
        mServerApi = API.getRetrofit(getString(R.string.api_url), this).create(API.IServerApi.class);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mDone.setVisibility(View.VISIBLE);
//        getFsspCaptcha();
    }

    private void showCaptchaDialog(Bitmap bmp) {
        if (bmp == null) {
            makeInfoDialogWithoutHeader(getString(R.string.service_down));
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_captcha, null);
        ImageView imageView = (ImageView) myView.findViewById(R.id.captchaImage_imageView);
        imageView.setImageBitmap(bmp);
        EditText captchaText = (EditText) myView.findViewById(R.id.captchaText_editText);
        captchaText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Button button2 = (Button) myView.findViewById(R.id.cancelButton);
        button2.setOnClickListener(e -> dialog.dismiss());
        button.setOnClickListener(e -> {
            if (captchaText.getText().length() == 0) {
                captchaText.setError(getString(R.string.field_cant_null));
                return;
            }
            if (isConnected())
                getFsspData(mResponse.getSes(),
                        captchaText.getText().toString(),
                        mFam.getText().toString(),
                        mName.getText().toString(),
                        mPatr.getText().toString(),
                        mDateString);
            dialog.dismiss();
        });
        dialog.show();
    }

    private void getFsspData(String ses, String cap, String second, String first, String pat, String date) {
        showDialog(getString(R.string.loading_data), this);
        Utils.getGeneralCount(this);
//        mServerApi.getFsspDataResp(ses, cap, second, first, pat, date).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                String s = API.bodyToString(response);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                t.printStackTrace();
//            }
//        });
        mServerApi.getFsspData(ses, cap, second, first, pat, date).enqueue(new Callback<Fssp>() {
            @Override
            public void onResponse(Call<Fssp> call, Response<Fssp> response) {
                hideDialog();
                if (response.body().getCode() == 201) {
                    makeInfoDialog(getString(R.string.no_fssp), true);
                    return;
                } else if (response.body().getCode() == 202) {
                    makeInfoDialogWithoutHeader(getString(R.string.need_recaptcha));
                    return;
                }
                if (response.body().getCode() == 200) {
                    Utils.incSuccess(FsspActivity.this);
                    showRateDialog();
                    if (response.body().getValues() instanceof List) {
                        List<List<String>> values = (List<List<String>>) response.body().getValues();
                        if (values.size() > 0) {
                            String message = "";
                            int count = 0;
                            for (List<String> list : values) {
                                if (list.size() > 0) {
                                    count++;
                                    for (String s : list) {
                                        message += "\n\n" + s;
                                    }
                                }
                            }
//                        makeResponseDialog(values);
                            makeInfoDialogUrl(String.format(getString(R.string.fssp_format), count), false);
                        } else {
                            makeInfoDialog(getString(R.string.no_fssp), true);
                        }
                    } else {
                        makeInfoDialogWithoutHeader(getString(R.string.need_recaptcha));
                    }
                }
//                if (response.body().getValues() != null) {
//                    makeInfoDialog(String.format(getString(R.string.fssp_format), response.body().getValues().size()));
//                } else if (response.body().getCode() == 202) {
//
//                    } else {
//                    makeInfoDialog(getString(R.string.no_fssp));
//                }
            }

            @Override
            public void onFailure(Call<Fssp> call, Throwable t) {
                hideDialog();
                if (t instanceof SocketTimeoutException) {
                    makeInfoDialogWithoutHeader(getString(R.string.service_down));
                } else {
                    getFsspCaptcha();
                }
            }
        });
    }

    public void makeInfoDialogUrl(String message, boolean isGreen) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_info, null);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        TextView tv = (TextView) myView.findViewById(R.id.info_textView_infoDialog);
        View header = myView.findViewById(R.id.headerColor);
        if (isGreen) header.setBackgroundColor(getResources().getColor(R.color.green));
        else
            header.setBackgroundColor(getResources().getColor(android.R.color.holo_red_dark));
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(Html.fromHtml(message));
        tv.setOnClickListener(e -> openUrl("http://fssprus.ru/iss/ip"));
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        button.setOnClickListener(e -> {
            dialog.dismiss();
        });
        if (!this.isFinishing()) {
            dialog.show();
        }
    }

    public void getFsspCaptcha() {
        if (isConnected()) {
//            mProgressBar.setVisibility(View.VISIBLE);
            mServerApi.getFsspCaptcha().enqueue(new Callback<CaptchaResponse>() {
                @Override
                public void onResponse(Call<CaptchaResponse> call, Response<CaptchaResponse> response) {
                    mProgressBar.setVisibility(View.GONE);
//                mCaptcha.setImageBitmap(response.body().getCaptchaImage());
                    if (response.body().getCaptcha() != null) {
                        showCaptchaDialog(response.body().getCaptchaImage());
                    } else {
                        makeInfoDialogWithoutHeader(getString(R.string.service_down));
                    }
                    mResponse = response.body();
                }

                @Override
                public void onFailure(Call<CaptchaResponse> call, Throwable t) {
                    if (t instanceof SocketTimeoutException) {
                        makeInfoDialog(getString(R.string.service_down), false);
                    } else {
                        getFsspCaptcha();
                    }
                }
            });
        }
    }

    @OnClick(R.id.date_button_fsspActivity)
    public void opedDateDialog() {
        DatePickerDialog tpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                mDateString = Utils.int2String(i2) + "." + Utils.int2String(i1 + 1) + "." + Utils.int2String(i);
                mDate.setText(getString(R.string.date_hint) + ": " + mDateString);
            }
        }, 1990, 0, 1);
        tpd.show();
    }

    @OnClick(R.id.done_button_toolbar)
    public void doneClick() {
//        getFsspData(mResponse.getSes(),
//                mCaptchaText.getText().toString(),
//                mFam.getText().toString(),
//                mName.getText().toString(),
//                mPatr.getText().toString(),
//                mDateString);
        if (mFam.getText().length() == 0) {
            mFam.setError(getString(R.string.field_cant_null));
            return;
        }
        if (mName.getText().length() == 0) {
            mName.setError(getString(R.string.field_cant_null));
            return;
        }
        if (mPatr.getText().length() == 0) {
            mPatr.setError(getString(R.string.field_cant_null));
            return;
        }
        getFsspCaptcha();
    }

    private void makeResponseDialog(List<List<String>> values) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = LayoutInflater.from(this);
        final View myView = li.inflate(R.layout.dialog_fssp_response, null);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        RecyclerView recyclerView = (RecyclerView) myView.findViewById(R.id.response_recyclerView_fsspDialog);
        TextView header = (TextView) myView.findViewById(R.id.header_textView_fsspDialog);
        ArrayList<String> arrayList = new ArrayList<>();
        for (List<String> s : values) {
            if (s.size() > 0) {
                arrayList.add(s.get(0) + "\n" + s.get(1));
            }
        }
        if (arrayList.size() > 0) {
            header.setText(String.format(getString(R.string.fssp_format), arrayList.size()));
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(new SimpleAdapter(arrayList));
        } else {
            header.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            TextView nodata = (TextView) myView.findViewById(R.id.noData_textView_fsspDialog);
            nodata.setVisibility(View.VISIBLE);
        }
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        button.setOnClickListener(e -> dialog.dismiss());
        dialog.show();
    }

    @Override
    public void onConnectionTimeout() {
        try {
            hideDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        makeInfoDialogWithoutHeader(getString(R.string.service_down));
    }
}
