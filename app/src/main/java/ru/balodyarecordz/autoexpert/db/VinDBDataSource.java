package ru.balodyarecordz.autoexpert.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import ru.balodyarecordz.autoexpert.Values;
import ru.balodyarecordz.autoexpert.model.db.VinDb;

public class VinDBDataSource {
    private SQLiteDatabase mDatabase;
    private final VinDBHelper mDbHelper;
    private String[] mAllColumns = {VinDBHelper.COLUMN_ID, VinDBHelper.COLUMN_VIN, VinDBHelper.COLUMN_MARK, VinDBHelper.COLUMN_STS};//, ContractsHelper.COLUMN_SEND };

    public VinDBDataSource(Context context) {
        mDbHelper = VinDBHelper.getInstance(context);
    }

    public void open() throws SQLException {
        mDatabase = mDbHelper.getWritableDatabase();
    }

    public void close() {
        mDbHelper.close();
    }

    public void createVindata(String vin) {
        ContentValues values = new ContentValues();
        values.put(VinDBHelper.COLUMN_VIN, vin);
        mDatabase.insert(VinDBHelper.TABLE_VINS, null,
                values);
        Cursor cursor = mDatabase.query(VinDBHelper.TABLE_VINS, null,
                null, null, null, null, null);
        cursor.moveToFirst();
        cursor.close();
    }

    public ArrayList<VinDb> getAllVins() {
        ArrayList<VinDb> vins = new ArrayList<>();
        Cursor cursor = mDatabase.query(VinDBHelper.TABLE_VINS,
                mAllColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            vins.add(cursorToVin(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return vins;
    }

    public long addVin(String vin) {
        ContentValues values = new ContentValues();
        values.put(VinDBHelper.COLUMN_VIN, vin);
        long id = mDatabase.insert(VinDBHelper.TABLE_VINS, null,
                values);
        Cursor cursor = mDatabase.query(VinDBHelper.TABLE_VINS, null,
                null, null, null, null, null);
        cursor.moveToFirst();
        cursor.close();
        return id;
    }

    public long addVin(String vin, String mark) {
        ContentValues values = new ContentValues();
        values.put(VinDBHelper.COLUMN_VIN, vin);
        values.put(VinDBHelper.COLUMN_MARK, mark);
        values.put(VinDBHelper.COLUMN_STS, "");
        long id = mDatabase.insert(VinDBHelper.TABLE_VINS, null,
                values);
        Cursor cursor = mDatabase.query(VinDBHelper.TABLE_VINS, null,
                null, null, null, null, null);
        cursor.moveToFirst();
        cursor.close();
        return id;
    }

    public void deleteFromDB(ArrayList<String> models) {
        for (String model : models) {
            mDatabase.delete(VinDBHelper.TABLE_VINS, "" +
                    "vin = " + model, null);
        }
    }

    public void deleteFromDB(String id) {
        mDatabase.delete(VinDBHelper.TABLE_VINS, "" +
                "_id = " + id, null);
    }

    private VinDb cursorToVin(Cursor cursor) {
        VinDb vinDb = new VinDb();
        vinDb.setId(cursor.getString(0));
        vinDb.setVin(cursor.getString(1));
        vinDb.setMark(cursor.getString(2));
        vinDb.setSts(cursor.getString(3));
        return vinDb;
    }

    public void updateSts(String sts) {
        ContentValues values = new ContentValues();
        values.put(VinDBHelper.COLUMN_STS, sts);
        mDatabase.update(VinDBHelper.TABLE_VINS, values, VinDBHelper.COLUMN_ID + " = ?", new String[] { String.valueOf(Values.LAST_ID) });
    }
}