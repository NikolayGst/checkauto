package ru.balodyarecordz.autoexpert.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PhoneDBHelper extends SQLiteOpenHelper {

    private static PhoneDBHelper sInstance;

    public static final String DB_NAME = "phone.sqlite";
    public static final int DB_VERSION = 1;
    public static final String TABLE_PHONES = "phones";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_DATA = "data";
    public static final String COLUMN_ID = "_id";

    private final String DATABASE_CREATE = "create table " + TABLE_PHONES
            + " (" + COLUMN_ID + " integer primary key autoincrement, " + COLUMN_PHONE
            + " text not null, " + COLUMN_DATA
            + " text not null);";

    public static synchronized PhoneDBHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PhoneDBHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public PhoneDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS phones");
        onCreate(db);
    }
}