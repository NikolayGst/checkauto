package ru.balodyarecordz.autoexpert.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class VinDBHelper extends SQLiteOpenHelper {

    private static VinDBHelper sInstance;

    public static final String DB_NAME = "vins.sqlite";
    public static final int DB_VERSION = 4;
    public static final String TABLE_VINS = "vins";
    public static final String COLUMN_VIN = "vin";
    public static final String COLUMN_MARK = "mark";
    public static final String COLUMN_STS = "sts";
    public static final String COLUMN_ID = "_id";

    private final String DATABASE_CREATE = "create table " + TABLE_VINS
            + " (" + COLUMN_ID + " integer primary key autoincrement, " + COLUMN_VIN
            + " text not null, "+ COLUMN_MARK
            + " text, " + COLUMN_STS  + " text);";

    public static synchronized VinDBHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new VinDBHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public VinDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS vins");
        onCreate(db);
    }
}