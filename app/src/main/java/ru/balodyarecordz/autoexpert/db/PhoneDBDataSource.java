package ru.balodyarecordz.autoexpert.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import ru.balodyarecordz.autoexpert.model.db.PhoneDb;

public class PhoneDBDataSource {
    private SQLiteDatabase mDatabase;
    private final PhoneDBHelper mDbHelper;
    private String[] mAllColumns = {PhoneDBHelper.COLUMN_ID, PhoneDBHelper.COLUMN_PHONE, PhoneDBHelper.COLUMN_DATA};//, ContractsHelper.COLUMN_SEND };

    public PhoneDBDataSource(Context context) { mDbHelper = PhoneDBHelper.getInstance(context);
    }

    public void open() throws SQLException {
        mDatabase = mDbHelper.getWritableDatabase();
    }

    public void close() {
        mDbHelper.close();
    }

    public void createPhonedata(String phone, String data) {
        ContentValues values = new ContentValues();
        values.put(PhoneDBHelper.COLUMN_PHONE, phone);
        values.put(PhoneDBHelper.COLUMN_DATA, data);
        mDatabase.insert(PhoneDBHelper.TABLE_PHONES, null,
                values);
        Cursor cursor = mDatabase.query(PhoneDBHelper.TABLE_PHONES, null,
                null, null, null, null, null);
        cursor.moveToFirst();
//        String contractModel = cursorToContract(cursor);
        cursor.close();
//        return contractModel;
    }

    public ArrayList<PhoneDb> getAllPhones() {
        ArrayList<PhoneDb> phones = new ArrayList<>();
        Cursor cursor = mDatabase.query(PhoneDBHelper.TABLE_PHONES,
                mAllColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            phones.add(cursorToPhone(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return phones;
    }

    public void addPhone(String phone, String data) {
        ContentValues values = new ContentValues();
        values.put(PhoneDBHelper.COLUMN_PHONE, phone);
        values.put(PhoneDBHelper.COLUMN_DATA, data);
        mDatabase.insert(PhoneDBHelper.TABLE_PHONES, null,
                values);
        Cursor cursor = mDatabase.query(PhoneDBHelper.TABLE_PHONES, null,
                null, null, null, null, null);
        cursor.moveToFirst();
//        String contractModel = cursorToContract(cursor);
        cursor.close();
    }

    public void deleteFromDB(ArrayList<String> models) {
        for (String model:models) {
            mDatabase.delete(PhoneDBHelper.TABLE_PHONES, "" +
                    "phone = " + model, null);
        }
    }

    public void deleteFromDB(String id) {
            mDatabase.delete(PhoneDBHelper.TABLE_PHONES, "" +
                    "_id = " + id, null);
    }

    private PhoneDb cursorToPhone(Cursor cursor) {
        PhoneDb data = new PhoneDb();
        data.setId(cursor.getString(0));
        data.setPhone(cursor.getString(1));
        data.setData(cursor.getString(2));
        return data;
    }
}