package ru.balodyarecordz.autoexpert.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;

import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.model.Purchase;
import ru.balodyarecordz.autoexpert.network.API;
import ru.balodyarecordz.autoexpert.view.helper.OnDialogSkipListener;


public class WarningDialogFragment extends DialogFragment implements View.OnClickListener, BillingProcessor.IBillingHandler, API.OnConnectionTimeoutListener {


    private static final String PRODUCT_ID = "ru.balodyarecordz.autoexpert.pro";
    private static final String TAG = "Tag";

    private Bundle mBundle;
    private int count;
    private TextView mTxtMsg;
    private Purchase mPurchase;
    private TextView mButtonSkip;
    private TextView mButtonPay;
    private String mLicenseId = "";
    private BillingProcessor bp;
    private boolean readyToPurchase = false;
    private String mKey;

    private OnDialogSkipListener mOnDialogSkipListener;
    private API.IServerApi mServerApi;
    private ProgressDialog mProgressDialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();

        mBundle = getArguments();

        mServerApi = API.getRetrofit(getString(R.string.api_url), this).create(API.IServerApi.class);

        count = mBundle.getInt(Purchase.KEY_COUNT);
        mKey = mBundle.getString(Purchase.KEY_KEY);

        bp = new BillingProcessor(context, mLicenseId, this);

        if (!BillingProcessor.isIabServiceAvailable(context)) {
            showToast("In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16");
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        View view = getActivity().getLayoutInflater().inflate(R.layout.warning_dialog_layout, null);
        mTxtMsg = (TextView) view.findViewById(R.id.txtMsg);

        mButtonSkip = (TextView) view.findViewById(R.id.btnSkip);
        mButtonSkip.setOnClickListener(this);
        mButtonPay = (TextView) view.findViewById(R.id.btnPay);
        mButtonPay.setOnClickListener(this);

        if (mBundle != null)
            switch (mKey) {
                case "main":
                    Spannable text = new SpannableString("У Вас осталось " + count + " из 7 бесплатных запросов. " +
                            "Получите версию PRO и совершайте проверки без ограничений!");
                    if (count == 0) text.setSpan(new ForegroundColorSpan(Color.RED), 15, 16,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mTxtMsg.setText(text);
                    break;
                case "buy":
                    mTxtMsg.setText("У вас уже есть вирсия PRO");
                    mButtonPay.setVisibility(View.GONE);
                    break;
                default:
                    mTxtMsg.setText(R.string.get_pro);
                    break;
            }
        alert.setView(view);

        return alert.create();
    }

    private void showToast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSkip:
                if (mOnDialogSkipListener != null) mOnDialogSkipListener.onSelectedSkip();
                dismiss();
                break;
            case R.id.btnPay:
                if (!readyToPurchase) {
                    showToast("Billing not initialized.");
                    return;
                }else {
                    bp.purchase(getActivity(),PRODUCT_ID);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        if (mOnDialogSkipListener != null) mOnDialogSkipListener.onPayPro();
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        showToast("onBillingError: " + Integer.toString(errorCode));
    }

    @Override
    public void onBillingInitialized() {
        //   showToast("onBillingInitialized");
        readyToPurchase = true;
    }

    @Override
    public void onDestroy() {
        if (bp != null) bp.release();
        super.onDestroy();
    }

    public void setOnDialogSkipListener(OnDialogSkipListener onDialogSkipListener) {
        if (mOnDialogSkipListener == null)
            mOnDialogSkipListener = onDialogSkipListener;
    }

    void showDialog(String message, Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
    }

    void hideDialog() {
        if (mProgressDialog != null)
            mProgressDialog.hide();
        if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog.cancel();
        }
    }

    @Override
    public void onConnectionTimeout() {
        try {
            hideDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
