package ru.balodyarecordz.autoexpert.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.activity.deprecated.MyChecksActivity;
import ru.balodyarecordz.autoexpert.activity.deprecated.MyContractsActivity;

public class ListFragment extends EnvFragment implements View.OnClickListener{

    private Activity mActivity;
    private CardView mShare;
    private CardView mMyChecks;
    private CardView mMyContracts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        mActivity = getActivity();
        View view = inflater.inflate(R.layout.fragment_list,
                container, false);
        initFragment(view);
        return view;
    }

    private void initFragment(View v) {
        mShare = (CardView) v.findViewById(R.id.share);
        mMyChecks = (CardView) v.findViewById(R.id.my_checks);
        mMyContracts = (CardView) v.findViewById(R.id.my_contracts);
        mShare.setOnClickListener(this);
        mShare.setOnClickListener(this);
        mMyContracts.setOnClickListener(this);
        mMyChecks.setOnClickListener(this);

//        String font = "fonts/font_light.otf";
//        if(font == null) {
//            return;
//        }
//        Typeface tf = FontCache.get(font, mActivity);
//        applyFontRecursively((ViewGroup)v, tf);
    }


    void applyFontRecursively(ViewGroup parent, Typeface font) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            if (child instanceof ViewGroup) {
                applyFontRecursively((ViewGroup) child, font);
            } else if (child != null) {
                if (child.getClass() == AppCompatTextView.class) {
                    ((TextView) child).setTypeface(font);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.share:
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Здесь будет информация о приложении " +
                        "Скачать с Play Market https://play.google.com/store/");
                startActivity(Intent.createChooser(shareIntent, "Поделиться с помощью..."));
                break;
            case R.id.my_checks:
                startActivity(new Intent(getActivity(), MyChecksActivity.class));
                break;
            case R.id.my_contracts:
                startActivity(new Intent(getActivity(), MyContractsActivity.class));
                break;
        }
    }
}
