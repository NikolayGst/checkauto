package ru.balodyarecordz.autoexpert.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.activity.deprecated.CheckAutoActivity;
import ru.balodyarecordz.autoexpert.activity.deprecated.ContractActivity;
import ru.balodyarecordz.autoexpert.activity.Env;
import ru.balodyarecordz.autoexpert.utils.Utils;

public class MainFragment extends EnvFragment implements View.OnClickListener{

    private Activity mActivity;
    private CardView mHowToSell;
    private CardView mCheck;
    private CardView mMakeOrder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        mActivity = getActivity();
        View view = inflater.inflate(R.layout.fragment_main,
                container, false);
        initFragment(view);
        return view;
    }

    private void initFragment(View v) {
        Utils.clearSP(mActivity);
        mHowToSell = (CardView) v.findViewById(R.id.main_how_to);
        mCheck = (CardView) v.findViewById(R.id.main_check);
        mMakeOrder = (CardView) v.findViewById(R.id.main_make);
        mCheck.setOnClickListener(this);
        mMakeOrder.setOnClickListener(this);
        mHowToSell.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_check:
                startActivity(new Intent(mActivity, CheckAutoActivity.class));
                break;
            case R.id.main_how_to:
                break;
            case R.id.main_make:
                ((Env) getActivity()).makeDialogSnackBar(0, new Intent(mActivity, ContractActivity.class));
//                startActivity();
                break;
        }
    }

}
