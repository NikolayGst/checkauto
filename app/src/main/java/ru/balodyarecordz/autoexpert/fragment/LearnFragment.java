package ru.balodyarecordz.autoexpert.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ru.balodyarecordz.autoexpert.R;

public class LearnFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_learn,
                container, false);
        initFragment(view);
        return view;
    }

    private void initFragment(View parent) {
        ImageView imageView = (ImageView) parent.findViewById(R.id.imageView);
        TextView textView = (TextView) parent.findViewById(R.id.text_learnFragment);

        Bundle bundle = getArguments();
        imageView.setImageResource(bundle.getInt(getString(R.string.image_key)));
        textView.setText(bundle.getString(getString(R.string.text_key)));
    }

}
