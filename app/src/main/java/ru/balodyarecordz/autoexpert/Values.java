package ru.balodyarecordz.autoexpert;

public class Values {
    public static boolean isStarsPressed = false;
    public static final String SUCCESS_COUNT_TAG = "SUCCESS_COUNT_TAG";
    public static final String GENERAL_COUNT_TAG = "GENERAL_COUNT_TAG";
    public static final String IS_GOING = "IS_GOING";
    public static final String IS_PHONE_MESSAGE_SHOWED = "IS_PHONE_MESSAGE_SHOWED";
    public static final String LOGIN_AUTOCODE = "LOGIN_AUTOCODE";
    public static final String PASS_AUTOCODE = "PASS_AUTOCODE";
    public static final String VIN_TAG = "VIN_TAG";
    public static final String STS_TAG = "STS_TAG";
    public static final String STS_SP_TAG = "STS_SP_TAG";
    public static final String AUTOCODE_TAG = "AUOCODE_TAG";
    public static long LAST_ID = 0;
}
