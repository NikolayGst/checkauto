package ru.balodyarecordz.autoexpert.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.model.autocode.DTP;
import ru.balodyarecordz.autoexpert.model.autocode.GTO;
import ru.balodyarecordz.autoexpert.model.autocode.TOList;
import ru.balodyarecordz.autoexpert.model.autocode.VladHistTable;

public class AutocodeView extends LinearLayout {

    private TextView mTitle;
    private TextView mName;
    private TextView mDescription;

    public AutocodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeView(context);
    }

    public AutocodeView(Context context) {
        super(context);
        initializeView(context);
    }

    public AutocodeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeView(context);
    }

    private void initializeView(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_autocode, this, true);
        mTitle = (TextView) view.findViewById(R.id.title_viewAutocode);
        mName = (TextView) view.findViewById(R.id.name_viewAutocode);
        mDescription = (TextView) view.findViewById(R.id.description_viewAutocode);
    }

    public void setData(String title, String name, String desc) {
        mTitle.setText(title);
        mName.setText(name);
        mDescription.setText(desc);
    }

    public void setData(String title, String name, int desc) {
        mTitle.setText(title);
        mName.setText(name);
        mDescription.setText(String.valueOf(desc));
    }

    public void setDataDTP(String title, List<DTP> data) {
        mTitle.setText(title);
        String left = "";
        String right = "";
        for (DTP d: data) {
            left += d.getDTPDate() + "\n\n";
            right += d.getDTPType() + "\n\n";
        }
        mName.setText(left);
        mDescription.setText(right);
    }

    public void setDataGTO(String title, List<GTO> data) {
        mTitle.setText(title);
        String left = "";
        String right = "";
        for (GTO d: data) {
            left += d.getGTODate() + "\n\n";
            right += d.getGTORes() + "\n\n";
        }
        mName.setText(left);
        mDescription.setText(right);
    }

    public void setDataTO(String title, List<TOList> data) {
        mTitle.setText(title);
        String left = "";
        String right = "";
        for (TOList d: data) {
            left += d.getTODate() + "\n\n";
            right += d.getTORes() + "\n\n";
        }
        mName.setText(left);
        mDescription.setText(right);
    }

    public void setDataGTO(String title, List<GTO> data, List<TOList> toData) {
        mTitle.setText(title);
        String left = "";
        String right = "";
        for (TOList d: toData) {
            left += d.getTODate() + "\n\n";
            right += d.getTORes() + "\n\n";
        }
        for (GTO d: data) {
            left += d.getGTODate() + "\n\n";
            right += d.getGTORes() + "\n\n";
        }
        mName.setText(left);
        mDescription.setText(right);
    }

    public void setDataOwner(String title, List<VladHistTable> data) {
        mTitle.setText(title);
        String left = "";
        String right = "";
        for (VladHistTable d: data) {
            left += d.getVladPeriod() + "\n\n";
            right += d.getVladType() + "\n\n";
        }
        mName.setText(left);
        mDescription.setText(right);
    }
}
