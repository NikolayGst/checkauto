package ru.balodyarecordz.autoexpert.view.helper;

/**
 * Created by Николай on 18.06.2016.
 */
public interface OnDialogSkipListener {
    void onSelectedSkip();
    void onPayPro();
}
