package ru.balodyarecordz.autoexpert.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.balodyarecordz.autoexpert.model.CaptchaResponse;
import ru.balodyarecordz.autoexpert.model.Token;
import ru.balodyarecordz.autoexpert.model.autocode.Autocode;
import ru.balodyarecordz.autoexpert.model.autoru.Bank;
import ru.balodyarecordz.autoexpert.model.autoru.VinInfo;
import ru.balodyarecordz.autoexpert.model.deprecated.PerekupData;
import ru.balodyarecordz.autoexpert.model.fssp.Fssp;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddDtp.Dtp;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddHistory.GibddHistory;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddRestritions.Restritions;
import ru.balodyarecordz.autoexpert.model.gibdd.GibddWanted.Wanted;
import ru.balodyarecordz.autoexpert.model.reestr.Reestr;

public class API {
//
//    private static final OkHttpClient client = new OkHttpClient();

    private static final String GIBDD_URL = "http://www.gibdd.ru";
    private static final String REESTR_URL = "https://www.reestr-zalogov.ru/";
    //    private static RestAdapter restAdapter;
    private static Executor executor = Executors.newSingleThreadExecutor();
    private static Picasso picasso;


    //    public interface IReestr {
//        @POST("/api/pledgesearch/ByVIN")
//        void checkVin(@Body Vin vin, Callback<VinAnswer> callback);
//
//        @Headers("X-Requested-With: XMLHttpRequest")
//        @POST("/Captcha/Init")
//        void captchaInit(Callback<CaptchaInit> callback);
//
//        @FormUrlEncoded
//        @POST("/Captcha/Verify")
//        void verify(@Field("CaptchaDeText") String token,
//                    @Field("CaptchaInputText") String captcha,
//                    Callback<IsCaptchaSuccess> callback);
//    }
//
    public interface IPerekup {
        @GET("/api/offer")
        Call<PerekupData> getPhoneInfo(@Query("mobileToken") String token, @Query("phone") String phone);
    }
//
//    public interface IGibdd {
//
//        @GET("/check/fines/")
//        void getSession(Callback<Response> callback);
//
//        @FormUrlEncoded
//        @Headers("Referer:http://www.gibdd.ru/check/auto/")
//        @POST("/proxy/check/fines/2.0/client.php")
//        void getFines(@Field("regnum") String regnum,
//                      @Field("regreg") String regreg,
//                      @Field("stsnum") String stsnum,
//                      @Field("req") String reqfines,
//                      @Field("captchaWord") String captchaWord,
//                      Callback<FinesAnswer> callback);
//
//        @FormUrlEncoded
//        @Headers("Referer:http://www.gibdd.ru/check/fines/")
//        @POST("/proxy/check/auto/2.0/client.php")
//        void getWanted(@Field("vin") String vin,
//                       @Field("captchaWord") String captchaWord,
//                       Callback<WantedAnswer> callback);
//
//    }

//    public static class SaveCookiesInterceptor implements Interceptor {
//        private Context context;
//
//        public SaveCookiesInterceptor(Context context) {
//            this.context = context;
//        }
//
//        @Override
//        public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
//            com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
//            if (!originalResponse.headers("Set-Cookie").isEmpty()) {
//                HashSet<String> cookies = new HashSet<>();
//                String cookie = "";
//                for (String header : originalResponse.headers("Set-Cookie")) {
//                    cookies.add(header);
//                    if (header.contains("PHP")) {
//                        cookie = header.replace("; path=/", "").replace("PHPSESSID=", "");
//                    }
//                }
//                if (!cookie.equals("")) {
//                    Utils.saveToSharedPreferences("cook", cookie, context);
//                }
//            }
//            return originalResponse;
//        }
//    }

//    public static Picasso getPicasso(Context context, final int type) {
//        OkHttpClient client = new OkHttpClient();
//        client.networkInterceptors().add(new Interceptor() {
//
//            @Override
//            public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
//                Request newRequest = null;
////                                        chain.request().newBuilder()
////                                .addHeader("Referer", "http://www.gibdd.ru/check/auto/")
////                                .build();
////                        return chain.proceed(newRequest);
//                switch (type) {
//                    case 0:
//                        newRequest = chain.request().newBuilder()
//                                .addHeader("Referer", "http://www.gibdd.ru/check/fines/")
//                                .build();
//                        return chain.proceed(newRequest);
//                    case 1:
//                        newRequest = chain.request().newBuilder()
//                                .addHeader("Referer", "http://www.gibdd.ru/check/auto/")
//                                .build();
//                        return chain.proceed(newRequest);
//                    default:
//                        newRequest = chain.request().newBuilder()
//                                .build();
//                        return chain.proceed(newRequest);
//                }
//
//            }
//        });
//        client.setCookieHandler(CookieHandler.getDefault());
//        picasso = new Picasso.Builder(context)
//                .downloader(new OkHttpDownloader(client))
//                .build();
//        return picasso;
//    }

//    public static RestAdapter getRestAdapter(Context context, String url) {
//        client.setCookieHandler(CookieHandler.getDefault());
//        client.interceptors().add(new SaveCookiesInterceptor(context));
//        restAdapter = new RestAdapter.Builder()
//                .setEndpoint(url)
//                .setClient(new OkClient(client))
//                .build();
//        return restAdapter;
//    }

    public static Retrofit getRetrofit(String url, OnConnectionTimeoutListener listener) {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        return onOnIntercept(chain, listener);
                    }
                })
                .build();
        Gson gson = new GsonBuilder().serializeNulls().create();
        GsonConverterFactory factory = GsonConverterFactory.create(gson);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(factory)
//                .client(okHttpClient)
                .baseUrl(url)
                .build();
        return retrofit;
    }

    private static okhttp3.Response onOnIntercept(Interceptor.Chain chain, OnConnectionTimeoutListener listener) throws IOException {
        try {
            okhttp3.Response response = chain.proceed(chain.request());
            return response.newBuilder().body(ResponseBody.create(response.body().contentType(), response.body().string())).build();
        } catch (SocketTimeoutException exception) {
            exception.printStackTrace();
            if (listener != null)
                listener.onConnectionTimeout();
        }

        return chain.proceed(chain.request());
    }

    public interface IServerApi {

        /* @GET("/user/check/@imei/@vin")
         Call
         http://tkachenkodevelop.ru/user/check/358044056732758/XTA21099043623914
 */

        @GET("/autoexpert/get-token")
        Call<Token> getToken();

        @GET("/vin/fssp")
        Call<CaptchaResponse> getFsspCaptcha();

        @GET("/vin/fssp/detail/{session}/{captcha}/{lastname}/{firstname}/{patronymic}/{birthdate}")
        Call<Fssp> getFsspData(@Path("session") String session,
                               @Path("captcha") String captcha,
                               @Path("lastname") String lastname,
                               @Path("firstname") String firstname,
                               @Path("patronymic") String patronomic,
                               @Path("birthdate") String birthdate);

        @GET("/vin/fssp/detail/{session}/{captcha}/{lastname}/{firstname}/{patronymic}/{birthdate}")
        Call<ResponseBody> getFsspDataResp(@Path("session") String session,
                                           @Path("captcha") String captcha,
                                           @Path("lastname") String lastname,
                                           @Path("firstname") String firstname,
                                           @Path("patronymic") String patronomic,
                                           @Path("birthdate") String birthdate);

        @GET("/vin/reestr-zalogov/{vin}")
        Call<CaptchaResponse> getReestrCaptcha(@Path("vin") String vin);


        @GET("/vin/reestr-zalogov/verify/{vin}/{token}/{session}/{captcha}")
        Call<Reestr> getReestrData(@Path("vin") String vin,
                                   @Path("token") String token,
                                   @Path("session") String session,
                                   @Path("captcha") String captcha);

        @GET("/vin/reestr-zalogov/verify/{vin}/{token}/{session}/{captcha}")
        Call<ResponseBody> getReestrDataResp(@Path("vin") String vin,
                                             @Path("token") String token,
                                             @Path("session") String session,
                                             @Path("captcha") String captcha);

        @GET("/vin/api/gibdd/v3/")
        Call<CaptchaResponse> getGibddCaptcha();


        //ГИБДД

        // /vin/api/gibdd/v3/getWanted/@ses/@vin/@cap
        // /vin/api/gibdd/v3/getDtp/@ses/@vin/@cap
        // /vin/api/gibdd/v3/getRestrict/@ses/@vin/@cap

       /* @GET("/vin/api/gibdd/getInfo/{session}/{vin}/{captcha}")
        Call<Gibdd> getGibddData(@Path("vin") String vin,
                                 @Path("session") String session,
                                 @Path("captcha") String captcha);*/

        @GET("/vin/api/gibdd/v3/getHistory/{session}/{vin}/{captcha}")
        Call<GibddHistory> getGibddHistoryData(@Path("session") String session,
                                               @Path("vin") String vin,
                                               @Path("captcha") String captcha);

        @GET("/vin/api/gibdd/v3/getDtp/{session}/{vin}/{captcha}")
        Call<Dtp> getGibddDtpData(@Path("session") String session,
                                  @Path("vin") String vin,
                                  @Path("captcha") String captcha);

        @GET("/vin/api/gibdd/v3/getWanted/{session}/{vin}/{captcha}")
        Call<Wanted> getGibddCarWantedData(@Path("session") String session,
                                           @Path("vin") String vin,
                                           @Path("captcha") String captcha);


        @GET("/vin/api/gibdd/v3/getRestrict/{session}/{vin}/{captcha}")
        Call<Restritions> getGibddRestrictData(@Path("session") String session,
                                               @Path("vin") String vin,
                                               @Path("captcha") String captcha);


        //ГИБДД
        @GET("/vin/api/gibdd/getInfo/{session}/{vin}/{captcha}")
        Call<ResponseBody> getGibddDataResp(@Path("vin") String vin,
                                            @Path("session") String session,
                                            @Path("captcha") String captcha);

        @GET("/vin/autoru/{vin}")
        Call<VinInfo> getVinData(@Path("vin") String vin);

        @GET("/vin/autoru/pledge/{vin}")
        Call<Bank> getBankData(@Path("vin") String vin);

        @GET("/vin/autoru/{vin}")
        Call<ResponseBody> getVinDataR(@Path("vin") String vin);


        @GET("/vin/autoru/{vin}/{mark}")
        Call<VinInfo> getVinData(@Path("vin") String vin,
                                 @Path("mark") String mark);

        @GET("/vin/autoru/{vin}/{mark}")
        Call<ResponseBody> getVinDataResp(@Path("vin") String vin,
                                          @Path("mark") String mark);

        @GET("/vin/avtokod/{vin}/{sts}")
        Call<Autocode> getAutocodeData(@Path("vin") String vin,
                                       @Path("sts") String sts);

        @GET("/vin/avtokod/{vin}/{sts}")
        Call<ResponseBody> getAutocodeDataResp(@Path("vin") String vin,
                                               @Path("sts") String sts);

        @GET("/vin/tradeinplus/{name}/{phone}")
        Call<ResponseBody> sendTradeRequest(@Path("name") String name,
                                            @Path("phone") String phone);

        @GET("/vin/avtokod/{user}/{pwd}/{vin}/{sts}")
        Call<Autocode> getAutocodeWithAutorization(@Path("user") String user,
                                                   @Path("pwd") String pwd,
                                                   @Path("vin") String vin,
                                                   @Path("sts") String sts);

        @GET("/vin/avtokod/{user}/{pwd}/{vin}/{sts}")
        Call<ResponseBody> getAutocodeWithAutorizationResp(@Path("user") String user,
                                                           @Path("pwd") String pwd,
                                                           @Path("vin") String vin,
                                                           @Path("sts") String sts);

        @GET("/vin/avtokod/{vin}/{sts}/{ses}/{rkey}/{captcha}")
        Call<ResponseBody> getAutocodeWithAutorizationRespCaptcha(@Path("vin") String vin,
                                                                  @Path("sts") String sts,
                                                                  @Path("ses") String ses,
                                                                  @Path("rkey") String rkey,
                                                                  @Path("captcha") String captcha);

    }

    public static String bodyToString(Response<ResponseBody> response) {
        ResponseBody body = response.body();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(body.byteStream()));
            StringBuilder out = new StringBuilder();
            String newLine = System.getProperty("line.separator");
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }
            return out.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public interface OnConnectionTimeoutListener {
        void onConnectionTimeout();
    }
}
