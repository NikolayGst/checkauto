package ru.balodyarecordz.autoexpert.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import ru.balodyarecordz.autoexpert.BuildConfig;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.Values;

public class Utils {

    public static void saveToSharedPreferences(String key, String value,
                                               Context context) {
        SharedPreferences pref = getPrefs(context);
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String getAutocodeLogin(Context context) {
        return getPrefs(context).getString(Values.LOGIN_AUTOCODE, null);
    }

    public static String getAutocodePass(Context context) {
        return getPrefs(context).getString(Values.PASS_AUTOCODE, null);
    }

    public static String getAutocodeSts(Context context) {
        return getPrefs(context).getString(Values.STS_SP_TAG, null);
    }

    public static void saveLogin(Context context, String login) {
        SharedPreferences pref = getPrefs(context);
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Values.LOGIN_AUTOCODE, login);
        edit.commit();
    }

    public static void savePass(Context context, String pass) {
        SharedPreferences pref = getPrefs(context);
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Values.PASS_AUTOCODE, pass);
        edit.commit();
    }

    public static void saveSts(Context context, String sts) {
        SharedPreferences pref = getPrefs(context);
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(Values.STS_SP_TAG, sts);
        edit.commit();
    }

    public static boolean isAutocodeDataExist(Context context) {
        return (getAutocodeLogin(context) != null && getAutocodePass(context) != null);
    }

    public static boolean isPhoneMessageShowed(Context context) {
        return getPrefs(context).getBoolean(Values.IS_PHONE_MESSAGE_SHOWED, false);
    }

    public static boolean isGoingToMarket(Context context) {
        return getPrefs(context).getBoolean(Values.IS_GOING, false);
    }

    public static void setPhoneMessageShowed(Context context) {
        SharedPreferences pref = getPrefs(context);
        SharedPreferences.Editor edit = pref.edit();
        edit.putBoolean(Values.IS_PHONE_MESSAGE_SHOWED, true);
        edit.commit();
    }

    public static void setGoingToMarket(Context context) {
        SharedPreferences pref = getPrefs(context);
        SharedPreferences.Editor edit = pref.edit();
        edit.putBoolean(Values.IS_GOING, true);
        edit.commit();
    }

    public static int getSuccessCount(Context context) {
        return getPrefs(context).getInt(Values.SUCCESS_COUNT_TAG, 0);
    }

    public static int getGeneralCount(Context context) {
        return getPrefs(context).getInt(Values.GENERAL_COUNT_TAG, 0);
    }

    public static void incSuccess(Context context) {
        SharedPreferences pref = getPrefs(context);
        SharedPreferences.Editor edit = pref.edit();
        edit.putInt(Values.SUCCESS_COUNT_TAG, getSuccessCount(context) + 1);
        edit.commit();
    }

    public static void incGeneral(Context context) {
        SharedPreferences pref = getPrefs(context);
        SharedPreferences.Editor edit = pref.edit();
        edit.putInt(Values.GENERAL_COUNT_TAG, getGeneralCount(context) + 1);
        edit.commit();
    }

    public static String parseDate(String date, String format)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            return formatter.parse(date).toString();
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void setupRatingBar(RatingBar mRatingBar, Context context) {
        mRatingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Animation anim = AnimationUtils.loadAnimation(context, R.anim.fadein);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        mRatingBar.setAlpha(1);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mRatingBar.setAlpha(0);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                mRatingBar.startAnimation(anim);
                return false;
            }
        });
        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (!Values.isStarsPressed) {
                    Values.isStarsPressed = true;
                    if (v <= 3) {
                        showTechDialog(context);
                    } else {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(context.getString(R.string.play_market_url) + context.getPackageName()));
                        context.startActivity(i);
                    }
                }
            }
        });
    }

    public static void sendMail(Context context) {
        String message = "Хочу рассказать о …\n\n";
        message += "Version: " + BuildConfig.VERSION_NAME + "\n";
        message += "Device: " + Build.MODEL + "\n";
        message += "OS: " + Build.VERSION.CODENAME + "\n" + "\n";
        message += "Отправлено с Android";
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.tech_supp_subject));
        i.putExtra(Intent.EXTRA_TEXT, message);
        i.setData(Uri.parse("mailto:" + context.getString(R.string.autoexpert_mail)));
        try {
            context.startActivity(Intent.createChooser(i, context.getString(R.string.choose_mail_client)));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, context.getString(R.string.mail_client_not_installed), Toast.LENGTH_SHORT).show();
        }
    }

    public static void showTechDialog(Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater li = LayoutInflater.from(context);
        final View myView = li.inflate(R.layout.dialog_tech, null);
        Button button = (Button) myView.findViewById(R.id.doneButton);
        builder.setView(myView);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Button button2 = (Button) myView.findViewById(R.id.cancelButton);
        button2.setOnClickListener(e -> dialog.dismiss());
        button.setOnClickListener(e -> {
            sendMail(context);
            dialog.dismiss();
        });
        dialog.show();
    }

    public static void showDialog(String message, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public static String getMonthName(int i) {
        switch (i) {
            case 0:
                return "января";
            case 1:
                return "февраля";
            case 2:
                return "марта";
            case 3:
                return "апреля";
            case 4:
                return "мая";
            case 5:
                return "июня";
            case 6:
                return "июля";
            case 7:
                return "августа";
            case 8:
                return "сентября";
            case 9:
                return "октября";
            case 10:
                return "ноября";
            case 11:
                return "декабря";
            default:
                return "";
        }
    }

    public static String loadFromSharedPreferences(String key,
                                                   Context context) {
        SharedPreferences pref = getPrefs(context);
        String s = pref.getString(key, "");
        return s;
    }

    public static String loadCookies(Context context) {
        SharedPreferences pref = getPrefs(context);
        String s = pref.getString("cook", "");
        return s;
    }

    public static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
    }

    public static void clearSP(Context context) {
        getPrefs(context).edit().clear().commit();
    }


    public static void saveStarsState(Context context, boolean b) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("stars", b);
        editor.commit();
    }

    public static boolean isNotEmpty(String s) {
        if (s != null) {
            if (!s.trim().equals("") && !s.equals("значение не определено") && !s.equals("данные отсутствуют"))
                return true;
            else return false;
        } else
            return false;
    }

    public static boolean isStarsPressed(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getBoolean("stars", false);
    }
//    public static String responseToString(Response result) {
//        BufferedReader reader = null;
//        StringBuilder sb = new StringBuilder();
//        try {
//
//            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
//
//            String line;
//
//            try {
//                while ((line = reader.readLine()) != null) {
//                    sb.append(line);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        return sb.toString();
//    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void setupEditText(final View view, final Activity activity) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    view.clearFocus();
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupEditText(innerView, activity);
            }
        }
    }

    public static Bitmap base64ToBitmap(String strBase64) {
        if (strBase64 == null) {
            return null;
        }
        strBase64 = strBase64.replace("data:image/jpeg;base64,", "");
        strBase64 = strBase64.replace("data:image/gif;base64,", "");
        strBase64 = strBase64.replace("data:image/png;base64,", "");
        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static String int2String(int value) {
        if (value < 10) {
            return "0" + String.valueOf(value);
        } else {
            return String.valueOf(value);
        }
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public static String removeNonDigits(final String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        return str.replaceAll("\\D+", "");
    }

}
