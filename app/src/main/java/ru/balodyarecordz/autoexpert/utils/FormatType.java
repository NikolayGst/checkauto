package ru.balodyarecordz.autoexpert.utils;

public class FormatType {

    public static String formatCar(int type) {
        String typeStr = "";
        switch (type) {
            case 1:
                typeStr = "Грузовые автомобили бортовые";
                break;
            case 2:
                typeStr = "Грузовые автомобили шасси";
                break;
            case 3:
                typeStr = "Грузовые автомобили фургоны";
                break;
            case 4:
                typeStr = "Грузовые автомобили тягачи седельные";
                break;
            case 5:
                typeStr = "Грузовые автомобили самосвалы";
                break;
            case 6:
                typeStr = "Грузовые автомобили рефрижераторы";
                break;
            case 7:
                typeStr = "Грузовые автомобили цистерны";
                break;
            case 8:
                typeStr = "Грузовые автомобили с гидроманипулятором";
                break;
            case 9:
                typeStr = "Грузовые автомобили прочие";
                break;
            case 21:
                typeStr = "Легковые автомобили универсал";
                break;
            case 22:
                typeStr = "Легковые автомобили комби (хэтчбек)";
                break;
            case 23:
                typeStr = "Легковые автомобили седан";
                break;
            case 24:
                typeStr = "Легковые автомобили лимузин";
                break;
            case 25:
                typeStr = "Легковые автомобили купе";
                break;
            case 26:
                typeStr = "Легковые автомобили кабриолет";
                break;
            case 27:
                typeStr = "Легковые автомобили фаэтон";
                break;
            case 28:
                typeStr = "Легковые автомобили пикап";
                break;
            case 29:
                typeStr = "Легковые автомобили прочие";
                break;
            case 41:
                typeStr = "Автобусы длиной не более 5 м";
                break;
            case 42:
                typeStr = "Автобусы длиной более 5 м, но не более 8 м";
                break;
            case 43:
                typeStr = "Автобусы длиной более 8 м, но не более 12 м";
                break;
            case 44:
                typeStr = "Автобусы сочлененные длиной более 12 м";
                break;
            case 49:
                typeStr = "Автобусы прочие";
                break;
            case 51:
                typeStr = "Специализированные автомобили автоцистерны ";
                break;
            case 52:
                typeStr = "Специализированные автомобили санитарные ";
                break;
            case 53:
                typeStr = "Специализированные автомобили автокраны ";
                break;
            case 54:
                typeStr = "Специализированные автомобили заправщики ";
                break;
            case 55:
                typeStr = "Специализированные автомобили мастерские ";
                break;
            case 56:
                typeStr = "Специализированные автомобили автопогрузчики ";
                break;
            case 57:
                typeStr = "Специализированные автомобили эвакуаторы ";
                break;
            case 58:
                typeStr = "Специализированные пассажирские транспортные средства ";
                break;
            case 59:
                typeStr = "Специализированные автомобили прочие ";
                break;
            case 71:
                typeStr = "Мотоциклы ";
                break;
            case 72:
                typeStr = "Мотороллеры и мотоколяски";
                break;
            case 73:
                typeStr = "Мотовелосипеды и мопеды";
                break;
            case 74:
                typeStr = "Мотонарты";
                break;
            case 80:
                typeStr = "Прицепы самосвалы";
                break;
            case 81:
                typeStr = "Прицепы к легковым автомобилям";
                break;
            case 82:
                typeStr = "Прицепы общего назначения к грузовым автомобилям";
                break;
            case 83:
                typeStr = "Прицепы цистерны";
                break;
            case 84:
                typeStr = "Прицепы тракторные";
                break;
            case 85:
                typeStr = "Прицепы вагоны-дома передвижные";
                break;
            case 86:
                typeStr = "Прицепы со специализированными кузовами";
                break;
            case 87:
                typeStr = "Прицепы трейлеры";
                break;
            case 88:
                typeStr = "Прицепы автобуса";
                break;
            case 89:
                typeStr = "Прицепы прочие";
                break;
            case 91:
                typeStr = "Полуприцепы с бортовой платформой";
                break;
            case 92:
                typeStr = "Полуприцепы самосвалы";
                break;
            case 93:
                typeStr = "Полуприцепы фургоны";
                break;
            case 95:
                typeStr = "Полуприцепы цистерны";
                break;
            case 99:
                typeStr = "Полуприцепы прочие";
                break;
            case 31:
                typeStr = "Трактора";
                break;
            case 32:
                typeStr = "Самоходные машины и механизмы";
                break;
            case 33:
                typeStr = "Трамваи";
                break;
            case 34:
                typeStr = "Троллейбусы";
                break;
            case 35:
                typeStr = "Велосипеды";
                break;
            case 36:
                typeStr = "Гужевой транспорт";
                break;
            case 38:
                typeStr = "Подвижной состав железных дорог";
                break;
            case 39:
                typeStr = "Иной";
                break;
        }
        return typeStr;
    }

    public static String formatOrg(int type) {
        String typeStr = "";
        switch (type) {
            case 0:
                typeStr = "Запрет на регистрационные действия";
                break;
            case 1:
                typeStr = "Запрет на снятие с учета";
                break;
            case 2:
                typeStr = "Запрет на регистрационные действия и прохождение ГТО";
                break;
            case 3:
                typeStr = "Утилизация (для транспорта не старше 5 лет)";
                break;
            case 4:
                typeStr = "Аннулирование";
                break;
        }
        return typeStr;
    }
}
