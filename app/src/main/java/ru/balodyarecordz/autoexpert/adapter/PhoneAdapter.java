package ru.balodyarecordz.autoexpert.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.db.PhoneDBDataSource;
import ru.balodyarecordz.autoexpert.model.db.PhoneDb;
import ru.balodyarecordz.autoexpert.view.helper.ItemTouchHelperAdapter;

public class PhoneAdapter extends RecyclerView.Adapter<PhoneAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    private ArrayList<PhoneDb> mData;
    private Context mContext;

    public PhoneAdapter(ArrayList<PhoneDb> mData, Context context) {
        this.mData = mData;
        this.mContext = context;
    }

//    public void add(String s, int position) {
//        position = position == -1 ? getItemCount()  : position;
//        mData.add(position,s);
//        notifyItemInserted(position);
//    }

    public void remove(int position){
        PhoneDBDataSource mPhonesDataSource = new PhoneDBDataSource(mContext);
        mPhonesDataSource.open();
        if (position < getItemCount()) {
            mPhonesDataSource.deleteFromDB(mData.get(position).getId());
            mData.remove(position);
            notifyItemRemoved(position);
            mPhonesDataSource.close();
        }
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        return false;
    }

    @Override
    public void onItemDismiss(int position) {
        remove(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.title)
        TextView title;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_simple, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.title.setText(mData.get(position).getPhone());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}