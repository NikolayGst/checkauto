package ru.balodyarecordz.autoexpert.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.model.deprecated.Datum;

public class AutoInfoAdapter extends RecyclerView.Adapter<AutoInfoAdapter.ViewHolder> {

    private ArrayList<Datum> mArrayList;

    public AutoInfoAdapter(ArrayList<Datum> mArrayList) {
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_auto_info, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.carName.setText(mArrayList.get(position).getCarName());
        holder.price.setText(mArrayList.get(position).getPrice() + " руб.");
        holder.date.setText(mArrayList.get(position).getCreatedAt());
        holder.mileage.setText(mArrayList.get(position).getMileage() + " км.");
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.carName_textView_itemAutoInfo)
        TextView carName;
        @Bind(R.id.price_textView_itemAutoInfo)
        TextView price;
        @Bind(R.id.date_textView_itemAutoInfo)
        TextView date;
        @Bind(R.id.mileage_textView_itemAutoInfo)
        TextView mileage;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}