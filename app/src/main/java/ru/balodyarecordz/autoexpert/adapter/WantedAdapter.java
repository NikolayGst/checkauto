package ru.balodyarecordz.autoexpert.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.model.deprecated.Restricted;
import ru.balodyarecordz.autoexpert.model.deprecated.WantedAnswer;

public class WantedAdapter extends RecyclerView.Adapter<WantedAdapter.ViewHolder> {

    private ArrayList<String> mRestrictedArray;
    private ArrayList<String> mWantedArray;
    private WantedAnswer mAnswer;

    public WantedAdapter(ArrayList<String> mRestrictedArray, ArrayList<String> mWantedArray) {
        this.mRestrictedArray = mRestrictedArray;
        this.mWantedArray = mWantedArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vin, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Restricted restricted = mAnswer.getRestricted();
        Restricted wanted = mAnswer.getWanted();
        if (position > restricted.getRecords().size()) {
            holder.restrict.setText("Ограничение: " + restricted.getRecords().get(position));
        } else {
            holder.restrict.setText("Ограничение: " + wanted.getRecords().get(position - restricted.getRecords().size()));
        }
    }

    @Override
    public int getItemCount() {
        return mRestrictedArray.size() + mWantedArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView restrict;

        public ViewHolder(View v) {
            super(v);
            restrict = (TextView) v.findViewById(R.id.wanted_textView);
        }
    }


}
