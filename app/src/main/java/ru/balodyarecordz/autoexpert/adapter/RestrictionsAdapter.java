package ru.balodyarecordz.autoexpert.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.model.autocode.Restriction;

public class RestrictionsAdapter extends RecyclerView.Adapter<RestrictionsAdapter.ViewHolder> {

    private ArrayList<Restriction> mData;

    public RestrictionsAdapter(ArrayList<Restriction> mData) {
        this.mData = mData;
    }

//    public void add(String s, int position) {
//        position = position == -1 ? getItemCount()  : position;
//        mData.add(position,s );
//        notifyItemInserted(position);
//    }

    public void remove(int position){
        if (position < getItemCount()  ) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.left)
        TextView left;
        @Bind(R.id.right)
        TextView right;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dual, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.left.setText(mData.get(position).getOgrName());
        holder.right.setText(mData.get(position).getOgrRes());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}