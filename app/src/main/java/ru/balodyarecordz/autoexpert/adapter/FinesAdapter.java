package ru.balodyarecordz.autoexpert.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.model.deprecated.Fine;

public class FinesAdapter extends RecyclerView.Adapter<FinesAdapter.ViewHolder> {

    private ArrayList<Fine> mArrayList;

    public FinesAdapter(ArrayList<Fine> mArrayList) {
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_fine, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Fine fine = mArrayList.get(position);
        holder.fio.setText(fine.getName() + " " + fine.getSname());
        holder.price.setText(fine.getSumma());
        holder.date.setText(fine.getDate());
        holder.koapCode.setText(fine.getCode());
        holder.koapText.setText(fine.getFine());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView fio;
        TextView koapText;
        TextView koapCode;
        TextView date;
        TextView price;

        public ViewHolder(View v) {
            super(v);
            fio = (TextView) v.findViewById(R.id.fine_fio);
            koapText = (TextView) v.findViewById(R.id.fine_reason);
            koapCode = (TextView) v.findViewById(R.id.fine_date);
            date = (TextView) v.findViewById(R.id.fine_date);
            price = (TextView) v.findViewById(R.id.fine_price);
        }
    }
}
