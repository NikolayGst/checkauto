package ru.balodyarecordz.autoexpert.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.balodyarecordz.autoexpert.R;
import ru.balodyarecordz.autoexpert.db.VinDBDataSource;
import ru.balodyarecordz.autoexpert.model.db.VinDb;
import ru.balodyarecordz.autoexpert.utils.Utils;
import ru.balodyarecordz.autoexpert.view.helper.ItemTouchHelperAdapter;

public class VinSwipeAdapter extends RecyclerView.Adapter<VinSwipeAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    private ArrayList<VinDb> mData;
    private Context mContext;

    public VinSwipeAdapter(ArrayList<VinDb> mData, Context context) {
        this.mData = mData;
        this.mContext = context;
    }

//    public void add(String s, int position) {
//        position = position == -1 ? getItemCount()  : position;
//        mData.add(position, s);
//        notifyItemInserted(position);
//    }

    public void remove(int position){
        VinDBDataSource mVinDataSource = new VinDBDataSource(mContext);
        mVinDataSource.open();
        if (position < getItemCount()) {
            mVinDataSource.deleteFromDB(mData.get(position).getId());
            mData.remove(position);
            notifyItemRemoved(position);
            mVinDataSource.close();
        }
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        return false;
    }

    @Override
    public void onItemDismiss(int position) {
        remove(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.mark)
        TextView mark;
        @Bind(R.id.sts)
        TextView sts;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vin, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.title.setText(mData.get(position).getVin());
        if (mData.get(position).getMark() != null) {
            if (Utils.isNotEmpty(mData.get(position).getMark())) {
                holder.mark.setText(mData.get(position).getMark());
            }
        }
        if (mData.get(position).getSts() != null) {
            if (Utils.isNotEmpty(mData.get(position).getSts())) {
                holder.sts.setText(mData.get(position).getSts());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}